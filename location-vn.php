<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
        <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
        
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        <link href="css/portfolio.css" rel="stylesheet">
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
        <link href="msd_assets/css/msd_styles_location.css" rel="stylesheet">
        
        <style>
            .nav{font-family: 'TeXGyreTermes'}
        </style>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MDVCFTH');</script>
		<!-- End Google Tag Manager -->
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P959B4G');</script>
		<!-- End Google Tag Manager -->
        
	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P959B4G"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-MDVCFTH""
		height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar " >
            <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        
                        <div class="main-menu" id="navbarNavDropdown">
                            <ul>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Design concept</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop1">
                                        <a class="dropdown-item" href="Introduction-vn.php">Introduction (project concept/ Somerset services</a>
                                        <a class="dropdown-item" href="#">Photo mock up unit</a>
                                        <a class="dropdown-item" href="#">Photo facilities</a>
                                        <a class="dropdown-item" href="#">Photo view 360 degree</a>
                                    </div>
                                </li>
                                 <li class="nav-item ">
                                    <a class="nav-link " href="location-vn.php" id="menu_drop2">Location</a>
                                    <!-- <div class="dropdown-menu" aria-labelledby="menu_drop2">
                                        <a class="dropdown-item" href="#">Location map</a>
                                        <a class="dropdown-item" href="#">Photo facilities near by</a>
                                    </div> -->
                                </li>
                               <!--  <li class="nav-item dropdown">
                                   <a class="nav-link dropdown-toggle" href="#" id="menu_drop2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Location</a>
                                   <div class="dropdown-menu" aria-labelledby="menu_drop2">
                                       <a class="dropdown-item" href="#">Location map</a>
                                       <a class="dropdown-item" href="#">Photo facilities near by</a>
                                   </div>
                               </li> -->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Unit layout</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop3">
                                        <a class="dropdown-item" href="#">Facilities Plan</a>
                                        <a class="dropdown-item" href="#">Floor plan</a>
                                        <a class="dropdown-item" href="#">Unit plan</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Media</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop4">
                                        <a class="dropdown-item" href="#">Mews</a>
                                        <a class="dropdown-item" href="#">E-brochure</a>
                                        <a class="dropdown-item" href="#">Project update</a>
                                        <a class="dropdown-item" href="#">Video events</a>
                                        <a class="dropdown-item" href="#">Video lifestyle/location/teaser</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Promotion</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop5">
                                        <a class="dropdown-item" href="#">New sales program/promotion</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Developer/ contact </a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop6">
                                        <a class="dropdown-item" href="#">Developers CL/ The Ascott</a>
                                        <a class="dropdown-item" href="#">introduction</a>
                                        <a class="dropdown-item" href="#">Letter CEO/ The Ascott</a>
                                        <a class="dropdown-item" href="#">Registration</a>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="introduction-vn.php" data-toggle="elementscroll">Design concept/ inspiration</a></li>
                                <li><a href="location-vn.php" data-toggle="elementscroll">Location</a></li>
                                <li><a href="residences-vn.php" data-toggle="elementscroll">Unit layout</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Media</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Developer/ Contact</a></li>
                                <li id="mnuReg" class="clsGoldbg" data-toggle="elementscroll"><a href="register-vn.php">Register</a></li>
                            </ul>

                        </div>
                    </div>
                    <div id="divLangToggle" > <a href="index.php">EN</a> / <a href="index-vn.php"> VN</a> </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <div class="texture-overlay"></div>
            
			
		</header>
        
        <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2 hidden-xs" data-parallax="scroll" data-image-src="img/locationhero.jpg"> 

                    <div class="clsArtImpression2">
                        <!--<span class="artimp2">Artist's impression  </span>-->
                    </div>
                    
                </div>
                
                <img id="imgLocKeyVis-mobile" src="img/location-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                <div  id="divPgKeyOuterTxtWrap">
                
                    <div id="divLocPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center locHeaderVNAlign" >

                            <div id="divLocPgKeyVisHeadTxt1" >
                               
                                
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet text-left">Kiến tạo</span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderNormalFontViet text-left text-uppercase">Chuẩn Mực Mới</span> 
                            </div>

                            <div id="divLocPgKeyVisHeadTxt2" style="padding-left: 10%;">
                                
                                
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFontViet"> tại&nbsp;</span> 
                                                <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">CHÂU Á</span>
                                
                                
                            </div>
                        </div>
                        


                    </div>

                </div>
            </div>    
           

        </section>

        
        
        <section id="sec" class="parllaxpadding"> 
                
            
            <div class="container-fluid">
	
				<div class="msd_maincontainer">
                
                    
                    <!-- header -->
                    <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                            
                            <div class="groupheader">
                                <!--
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Sophistication</span> 
                                </div>

                                <div id="divIntroSlideHeadTxt2">
                                	<span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inside</span>
                                    <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">and&nbsp;</span> 
                                    <span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">out</span>
                                </div>
                                -->
                            </div>
                            
                        </div>  
                    <!-- end header -->
                    
                    <style>
                      

                       
                        
                    </style>
                    
                    
                    
                    <div class="loc_map1"><img src="img/LocationMap4.jpg" alt="location_map1" style="margin-bottom:20px;" /></div>
                    
                    <div id="IntroBarInner" class="clsBarInner zoomLocationMap1">
                        <div class=" col-xs-12 text-center clsNormalFont text-uppercase">
                            <a>Click để xem thêm 
                            <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
     
                    
   
                    
                <div class="loc2_map1_legend">
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon1">ĐỊA ĐIỂM</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">1</span> Bitexco</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">2</span> Khách sạn Pullman Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">3</span> Khách sạn Park Hyatt Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">4</span> Khách sạn InterContinental Asiana Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">5</span> Khách sạn Le Méridien Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">6</span> Khách sạn The Reverie Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">7</span> Khách sạn Nikko Sài Gòn</p>
                                    
                                    
                                </div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">8</span> Khách sạn Equatorial TP HCM</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">9</span> Chợ Bến Thành</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">10</span> Nhà thờ Đức bà</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">11</span> Nhà hát thành phố</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">12</span> Chợ Lớn</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">13</span> Nhà khách chính phủ</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">14</span> Bưu điện thành phố</p>
                                    
                                </div>
                                <div class="clear"></div>
                            </div>
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon4">TRUNG TÂM MUA SẮM VÀ GIẢI TRÍ</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">15</span> Trung tâm thương mại Vincom</span></p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">16</span> Trung tâm sự kiện Gem</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">17</span> Chill Skybar</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">18</span> Riverside Palace</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">19</span> Trung tâm thương mại Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">20</span> Khu người Hoa</p>
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">21</span> Now Zone Plaza</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">22</span> Chợ An Đông</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">23</span> Công viên 23/9</p>
                                     <p class="lgnd_1"><span  class="a2 amenitiesCircle">24</span> Công viên Tao Đàn</p>
                                      <p class="lgnd_1"><span  class="a2 amenitiesCircle">25</span> Bảo tàng nghệ thuật</p>
                                      <p class="lgnd_1"><span  class="a2 amenitiesCircle">26</span> Sân vân động Thống Nhất</p>
                                     
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                            
                
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon6">TRUNG TÂM HÀNH CHÍNH </div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">27</span> Ủy ban nhân dân TP Hồ Chí Minh</p>
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">28</span> Ngân hàng nhà nước Việt Nam</p>
                                    
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">29</span> Sở cảnh sát thành phố</p>
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">30</span> Cục cảnh sát phòng cháy chữa cháy</p>
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                
                
                            
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon2">BỆNH VIỆN</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">31</span> Bệnh viện đa khoa Sài Gòn</p>
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">32</span> Bệnh viện quốc tế Columbia Asia Saigon</p>
                                    
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">33</span> Bệnh viện FV Saigon</p>
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">34</span> Bệnh Viện Chấn Thương Chỉnh Hình</p>
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                            
                           
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon5">GIÁO DỤC</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a5 amenitiesCircle">35</span> Trường quốc tế The American School</p>
                                    <p class="lgnd_1"><span  class="a5 amenitiesCircle">36</span> Đại học Khoa học TP HCM</p>
                                    
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a5 amenitiesCircle">37</span> Trường Lê Hồng Phong</p>
                                     <p class="lgnd_1"><span  class="a5 amenitiesCircle">38</span> Đại học Sài Gòn</p>
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                            
                            
                           
                            
                            
                        </div>

                    
                    <div style="width:100%; height:50px;"></div>
            

                    <!-- /* OSR  Begin - KEY PLACES */ -->
            
                        <style>
                            #divLocImgs {width:97% !important;margin:0 auto;}
                            
                            .recent-work-wrap .overlay .preview {
                                bottom: 0;
                                display: inline-block;
                                height: 35px;
                                line-height: 35px;
                                border-radius: 0;
                                background: transparent;
                                text-align: center;
                                color: #fff;
                                z-index: 1;
                                position: absolute;
                                
                                width: 100%;
                                height: 100%;

                                top: 50%;
                                left: 0;
                                padding: 0;

                            }
                            
                            @media only screen and (max-width : 740px) {#divLocImgs {width:90% !important;}}
                        </style>

                    <div id="divLocImgs">
                        
                       
                        <div class="row">
                            <div class="portfolio-items">
                                <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <a href="" ><img class="img-responsive" src="img/keyplaces/thumbs/Ben-Thanh-Market.jpg" alt=""></a>
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item joomla bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Bitexco-Financial-Tower.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item bootstrap wordpress col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Cho-Lon-Market.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>        
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item joomla wordpress apps col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/City-People-Committee.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>           
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item joomla html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/City-Post-Office.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>      
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item wordpress html apps col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Columbia-Asia-Hospital.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>         
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item wordpress html col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Gem-Center.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Hotel-Inter-Continental.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Hotel-Park-Hyatt.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Le-Meridien.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Museum-of-Ho-Chi-Minh-City.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Opera-House.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Saigon-Center.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Saigon-Notre-Dame-Basilica.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/State-Bank-of-Vietnam.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Sunwah-Tower.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Tao-Dan-Park.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/The-Reverie.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Thien-Hau-Temple.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Vincom-Center.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                            </div>
                        </div>
                        
                        
                    </div>
                    <!-- /* OSR  End - KEY PLACES */ -->
            
            
                
                </div><!-- END msd_maincontainer -->
                
                

				
                


            </div>

            
            

        </section>
        
       
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                            Chúng tôi đã đặc biệt thận trọng trong việc chuẩn bị nội dung tài liệu này cũng như các bản mẫu. Tuy nhiên vui lòng lưu ý rằng bản thân tập đoàn, cũng như các đại lý bán hàng, sẽ không chịu trách nhiệm cho những thông tin chưa chính xác trong tài liệu này. Tuy bản thân chúng tôi thực sự tin tưởng rằng những nội dung dưới đây là hoàn toàn chính xác, nhưng tài liệu này sẽ không mang tính đại diện hay trở thành một tuyên ngôn riêng. Tất cả thông tin, những thông số đều có thể sẽ được thay đổi bởi chủ đầu tư hoặc bởi các nhà chức trách có thẩm quyền, bởi vậy mà đây không thể trở thành một phần của các hợp đồng hay đề nghị. Các bản vẽ, thiết kế hay hình ảnh đồ họa sẽ không đại diện chính xác cho điều kiện xây dựng thực tế. Các khu vực sàn được đo đạc kĩ lưỡng, có căn cứ từ các bản báo cáo. Trong khi đó, các hình ảnh đồ họa có thể không phản ánh hoàn toàn điều kiện thực tế.
                            
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/js/msd_functions.js"></script>
        
        <!-- MSD JS INCLUDES -->
        
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>
        <script src="msd_assets/js/msd_functions_location.js"></script>

        
        <script src="js/jquery.isotope.min.js"></script>
        <script src="js/portfolio.js"></script> 
        
        <div class="blackbox_shade"></div>
        <div class="blackbox">
        <div class="blackbox_control"><span>CLOSE - X</span></div>
        <div class="blackbox_container">hihi</div>
        </div>
           
        <div class="brightbox"></div>
	</body>
    
    <script>
    var w=$(window), bg = $('.bg');

w.scroll(function(e){
   bg.css('top','-'+w.scrollTop()*.5+'px');
});
    
    
    </script>
    <?php include_once('google_tracking.php') ?>
</html>
