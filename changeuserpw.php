<?php require_once("library/check_session.php.inc"); ?>
<?php require_once("library/db_connect.php.inc"); ?>
<?php require_once("library/functions.php.inc"); ?>
<?php include_once("library/ps_pagination_simple.php") ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
        <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
        
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <link href="css/regform.css" rel="stylesheet"> 
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
        <style>
            body, tbody{color:black;}
            a  {color:black;}
            a:hover {color:blue;}
        </style>
        
	</head>
	<body>
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar " >
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="index.html"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        <span class="breadcrumbx">Page name goes here</span>
                        
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked">
                                
                                <li><a href="index.html">HOME  </a></li>
                                <li><a href="introduction.html">INTRODUCTION</a></li>
                                <li><a href="location.html" >LOCATION </a></li>
                                <li><a href="residences.html" >RESIDENCES</a></li>
                                <li><a href="developer.html" >DEVELOPER</a></li>
                                <li id="mnuReg" class="clsGoldbg active"  ><a href="register.html">REGISTER  </a></li>

                            </ul>



                        </div>
                    </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <div class="texture-overlay"></div>
            
			
		</header>
        
       
       
        <!-- OSR - Begin Body -->
        
        
                
            <?php

							
                if ( $sessionUser != 1) { redirect("adminlogin.php?return_url=" . getPageId() . ".php&event=Authorization Failure"); exit; } 

               

            ?>
            
        
            <section  id="secLoginForm" >
                <div class="container" id="secLoginFormContainer" >
                    <div class="row">
                    
                        <?php

                            if ( $sessionUser != 1) { redirect("adminlogin.php?return_url=" . getPageId() . ".php&event=Authorization Failure"); exit; } 
                        ?>
                        
                        
                        <div id="regDataTableWrap">
                        <div class="table-responsive  regtable" >
                           
                            
                           

                            <div class="col-lg-12  ">
                                
                                <?php

                                $varUserId = intval($_GET['ue']);


                                $query_user_details = "";

                                $query_user_details = $query_user_details."SELECT * FROM tblusers WHERE usid=". $varUserId  ;

                                $result_user_details = mysql_query($query_user_details) or die('MySql Error' . mysql_error());


                                $num_rows = mysql_num_rows($result_user_details);

                                //echo $num_rows;

                                
                                if ( $_SESSION['usertype'] != 10) { 
                                    
                                    
                                        echo "<span class='clsRed'><center><br />You are not authorized to access User Management Area</center></span> <br / > <br /> <a href='admincp.php'>Home</a>"; 

                                    }  
                                else if( $num_rows < 1) { 
                                    
                                    
                                        echo "<span class='clsRed'><center><br />User details not found</center></span> <br / > <br /> <a href='admincp.php'>Home</a>"; 

                                    } 
                                else

                                    {
                                        

                                        $row = mysql_fetch_array($result_user_details);
                                    

                                ?>
                                
                                
                              
                                
                                <a href="admincp.php"><span class="clsGoldLight" >Home</a> |</span> <a href="logout.php"><span class="clsGoldLight" >Logout</a></span>
                                
                                <form class="well form-horizontal" action="savepw.php" method="post"  id="changepass_form"  accept-charset="UTF-8">
                                    
                               
                                    <input type="hidden" name="hdnUsID" id="hdnUsID" value="<?php echo intval($row['usid']); ?>">
                            
                                    <?php If ($_SESSION['errMsg'] <> "") { ?>

                                        <div class="clsRed text-center">
                                            <?php
                                                echo  $_SESSION['errMsg'];

                                                ?>
                                            <br /><br />

                                        </div>

                                    <?php
                                        }

                                    ?>
                                    <div class="clsGold headerTxt text-center"><strong>CHANGE PASSWORD</strong> <hr></div>
                                    <div id="frmWrap">
                                        
                                        <fieldset>
                                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" > <!-- OSR - FROM END -->

                                                
                                                 <div class="form-group">

                                                    <div id="divUserID" class="col-md-6 nopadding clsRegTop text-right" >
                                                        <label  class=" text-uppercase-  clsGold " > User &nbsp;&nbsp;&nbsp; </label>

                                                    </div>

                                                    <span id="spnBr-" class="visible-xs"></span>
                                                     <div class="col-md-6 inputGroupContainer nopadding " >
                                                        <?php echo $row['userid'] ?>
                                                    </div>

                                                </div>
                                                

                                                <div class="form-group">

                                                    <div id="divUserID" class="col-md-6 nopadding clsRegTop text-right" >
                                                        <label  class=" text-uppercase-  clsGold " > Password &nbsp;&nbsp;&nbsp; </label>

                                                    </div>

                                                    <span id="spnBr-" class="visible-xs"></span>
                                                     <div class="col-md-6 inputGroupContainer nopadding " >
                                                        <input  name="password"  placeholder="" class="form-control  element-transparent clsGold"  type="password">

                                                    </div>

                                                </div>

                                                 <div class="form-group">

                                                    <div id="divPass2" class="col-md-6 nopadding clsRegTop text-right" >
                                                        <label  class=" text-uppercase-  clsGold " >Confirm Password &nbsp;&nbsp;&nbsp; </label>

                                                    </div>

                                                    <span id="spnBr-" class="visible-xs"></span>
                                                     <div class="col-md-6 inputGroupContainer nopadding " >
                                                        <input type="password" name="confpassword"  placeholder="" class="form-control  element-transparent clsGold"  type="text">

                                                    </div>

                                                </div>


                                                <br />


                                                <div class="form-group text-center">

                                                    <div class="col-md-12 clsSubmitVerySmall">
                                                        <button type="submit" class="btn  btn-custom" >SUBMIT <span class="glyphicon glyphicon-play"></span></button>

                                                    </div>
                                                </div>






                                            </div> <!-- OSR - FROM END -->

                                        </fieldset>
                                    </div>
                                </form>
                                
                                
                                
                                <?php
                                    
                                    }
                                ?>
                            
                            <br /><br />
                            </div>
                            
                            
                            <br />

                            
                            
                           
                        </div>
                    </div>

                         
                    </div>
 
                    
                </div>
                
                
            </section>
        
        
        <!-- OSR - End Body -->
         
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                            We have used reasonable care in preparing this website and in constructing the model(s) and showflats. Please note, however, that neither our agents nor we will be held responsible for any inaccuracy in the contents of this website. 
                            Whilst we believe the contents of this website to be correct and accurate and correct at the time of publication, they are not to be regarded as statements or representations of fact. All information, 
                            specifications and plans herein contained may be subjected to change from time to time by us and/or the competent authorities as may be required and do not form part of an offer or contract. Renderings, depictions and illustrations are artistic impressions. 
                            Photographs do not necessarily represent as-built standard specifications. Floor areas are approximate measurements and are subject to final survey. Likewise the model(s) and showflats are artistic impressions only and should not be considered as representation of fact.
                            
                        
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
        <script type="text/javascript" src="js/bootstrapValidator.min.js"> </script>
        <script src="js/register.js"></script>
        
        
        
        <!-- MSD JS INCLUDES -->
        <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>
        <script src="msd_assets/js/msd_functions.js"></script>


        <div class="brightbox"></div>
        
        <script>

          $(document).ready(function() {



            $('#changepass_form').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    password: {
                        validators: {
                                stringLength: {
                                min: 3,
                            },
                                notEmpty: {
                                message: 'Please key-in Password'
                            }
                        }
                    },
                    confpassword: {
                        validators: {
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                            
                        }
                    }
                    }
                })
                .on('success.form.bv', function(e) {
                    //$('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                      //  $('#contact_form').data('bootstrapValidator').resetForm();

                    // Prevent form submission
                    e.preventDefault();
                 var $form = $(e.target),
                fv    = $(e.target).data('bootstrapValidator');
                    fv.defaultSubmit();

                    // Get the form instance
                    //var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    //var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    //$.post($form.attr('action'), $form.serialize(), function(result) {
                        //console.log(result);
                    //}, 'json');
                });
        });




        </script> 
        
           
	</body>
</html>
