<?php require_once("library/check_session.php.inc"); ?>
<?php require_once("library/db_connect.php.inc"); ?>
<?php require_once("library/functions.php.inc"); ?>
<?php include_once("library/ps_pagination_simple.php") ?>

<?php

$errorMSG = "";

date_default_timezone_set('Asia/Ho_Chi_Minh');


?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
        <link href="css/regform.css" rel="stylesheet"> 
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
	</head>
	<body>
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar " >
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        
                        
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked">
                                
                                <li><a href="index.html">HOME  </a></li>
                                <li class="active"><a href="introduction.html">INTRODUCTION</a></li>
                                <li><a href="location.html" >LOCATION </a></li>
                                <li><a href="residences.html" >RESIDENCES</a></li>
                                <li><a href="developer.html" >DEVELOPER</a></li>
                                <li id="mnuReg" class="clsGoldbg"><a href="register.html">REGISTER  </a></li>

                            </ul>



                        </div>
                    </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <div class="texture-overlay"></div>
            
			
		</header>
        
        <section id="divKeyVisual" class="clsKeyvisual"> 
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-left-shape hidden-xs" data-parallax="scroll" data-image-src="img/templatekeyvis.jpg"> 

                   
                </div>
                
                <img id="imgKeyVis-mobile" src="img/pgkeyvis.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                
                
                <div id="divPgKyeTxtWrap" class="clsKeyVisParllaxTxtWrap">

                    <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                        <div id="divPgKeyVisHeadTxt1" >
                            <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont text-left">Setting </span> 
                            <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont text-left">THE </span> 
                        </div>

                        <div id="divPgKeyVisHeadTxt2">

                            <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont  ">Standard</span> 
                            <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont">IN&nbsp;</span> 
                            <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont">ASIA </span>
                        </div>
                    </div>
                    <div id="divPgKeyVisParaTxt" class="text-center">
                        <p>
                            <span class="">
                            Feel the energy of District 1, Ho Chi Minh City’s prestigious financial and commercial district. 
                            With the city’s most notable lifestyle destinations right around the corner, D1MENSION brings you closer to the heart of Ho Chi Minh City.
                            </span>
                        </p>
                    </div>


                </div>

            </div>    
           

        </section>

        <section  id="secRegForm" >
                <div class="container-fluid" id="secRegFormContainer" >
                    <div class="row">
                        
                            <div class="col-md-12 nopadding">
                                <div id="txtSuccess">

                                    <!-- OSR - Begin form validation and meail seding --> 

                                    <?php
                                        session_start();
                                        $errorMSG = "";

                                        // NAME
                                        if (empty($_POST["full_name"])) {
                                            $errorMSG = "Name is required <br>";
                                        } else {
                                            $name = $_POST["full_name"];
                                        }

                                       

                                       

                                        // COUNTRY CODE
                                        if (empty($_POST["countrycode"])) {
                                            $errorMSG = "Country Code is required <br>";
                                        } else {
                                            $countrycode = $_POST["countrycode"];
                                        }

                                        // NUMBER
                                        if (empty($_POST["phonenumber"])) {
                                            $errorMSG = "Phone Number is required <br>";
                                        } else {
                                            $phonenumber = $_POST["phonenumber"];
                                        }

                                         // EMAIL
                                        if (empty($_POST["email"])) {
                                            $errorMSG .= "Email is required ";
                                        } else {
                                            $email = $_POST["email"];
                                        }


                                        // HOW DID  YOU KNOW
                                       
                                        
                                        // Preferred method of contact 
                                        if (empty($_POST["prefcontactmethod"])) {
                                            $errorMSG = "Preferred method of contact is required <br>";
                                        } else {
                                            $prefcontmethod = implode(", ", $_POST["prefcontactmethod"]); 
                                        }

                                        // Preferred time of contact 
                                        if (empty($_POST["prefcontacttime"])) {
                                            $errorMSG = "Preferred time of contact is required <br>";
                                        } else {
                                            $prefconttime = implode(", ", $_POST["prefcontacttime"]); 
                                        }

                                       


                                        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                                            if ($_POST['captcha'] == $_SESSION['cap_code']) {
                                                // Captcha verification is Correct. Do something here!
                                                $cap = 'Eq';
                                            } else {
                                                // Captcha verification is wrong. Take other action
                                                $cap = '';
                                                $errorMSG .= "Error in Captcha.   ";
                                            }
                                        }

                                        $title = $_POST["saluation"];

                                       
                                       // $howdoyouknow = implode(', ', $_POST['howdoyouknow']);

                                        //$sendinfo = implode(', ', $_POST['sendinfo']);

                                       $howdoyouknow =  $_POST['howdoyouknow'];

                                       $sendinfo =$_POST['sendinfo'];



                                        $EmailFrom = "info@d1mension.com.vn"; 
                                            
                                        $EmailTo = "clvn.enquiry@capitaland.com";

                                   
                                        $Subject = "New Message Received from CapitaLand - D1MENSION -  Registration Form";

                                        // prepare email body text
                                        $Body = "";
                                        $Body .= "<b>Title:</b> ";
                                        $Body .= $title;
                                        $Body .= "<br />";
                                        $Body .= "<b>Full Name:</b> ";
                                        $Body .= $name;
                                        $Body .= "<br />";
                                        
                                        $Body .= "<b>E-Mail:</b> ";
                                        $Body .= $email;
                                        $Body .= "<br />";
                                       
                                        
                                        $Body .= "<b>Country Code :</b> ";
                                        $Body .= $countrycode;
                                        $Body .= "<br />";
                                        $Body .= "<b>Phone Number:</b> ";
                                        $Body .= $phonenumber;
                                        $Body .= "<br />";

                                        
                                        
                                        $Body .= "<b>Preferred method of contact:</b> ";
                                        $Body .= $prefcontmethod;
                                        $Body .= "<br />";
                                        $Body .= "<b>Preferred time of contact:</b> ";
                                        $Body .= $prefconttime;
                                        $Body .= "<br />";
                                        

                                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                                        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                                        $headers .= 'From:'.$EmailFrom  . "\r\n";
                                        $headers .= 'Cc: clvn.enquiry@capitaland.com' . "\r\n";
                                        //$headers .= 'Bcc: none@none.com' . "\r\n";

                                                
                                        // echo $Body;

                                         
                                        //**** Begin enable this for testing ***//

                                       
                                             $EmailTo = "clvn.enquiry@capitaland.com";

                                        //**** END enable this for testing ***// 

                                        // send email
                                        if($errorMSG == ""){
                                             $success = mail($EmailTo, $Subject, $Body, $headers);
                                        }

                                        // redirect to success page
                                        if ($success && $errorMSG == ""){
                                           echo "<span class='successMsg'>Registration complete! <br /> Our team will be in contact with you shortly.<br /> Thank you!<br /><br /></span>";
                                            
                                            
                                            //***Begin inserting MOVE THIS LATER***

                                            $record = array();
                                            
                                            $record['rdate'] = date('Y-m-d H:i:s');
                                            $record['title'] = trim($title);
                                            $record['fullname'] = trim($name);  
                                           
                                            
                                            
                                            $record['countrycode'] = trim($countrycode);
                                            $record['phone'] = trim($phonenumber);
                                            $record['email'] = trim($email);
                                            $record['prefconttime'] = trim($prefconttime);
                                            $record['prefcontmethod'] = trim($prefcontmethod);
                                            
                                            


                                            $sql = get_quoted_insert('tblregistration',$record);
                                            $execQuery = 1;
                                            //echo $sql;

                                            if ($execQuery <> 0)
                                            {
                                                $q = $pdo->prepare($sql);
                                                $q->execute();		
                                            }



                                            
                                            //***End inserting MOVE THIS LATER***
                                            
                                            
                                        }else{
                                            if($errorMSG == ""){

                                                echo "<span class='errMsg'>Something went wrong :(.</span>";
                                            } else {
                                                echo "<span class='errMsg'>". $errorMSG. "  Please <a href='##' onClick='history.go(-1); return false;'><u>Go back</u></a>  and update the form.</span> ";

                                            }
                                        }

                                    ?>


                                    <!-- OSR - End form validation and meail seding --> 




                                </div>
                            </div>
                        
                       
                         
                    </div>
                    
                     
                    
                    
                </div>
                
                
            </section>
        
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                            We have used reasonable care in preparing this website and in constructing the model(s) and showflats. Please note, however, that neither our agents nor we will be held responsible for any inaccuracy in the contents of this website. 
                            Whilst we believe the contents of this website to be correct and accurate and correct at the time of publication, they are not to be regarded as statements or representations of fact. All information, 
                            specifications and plans herein contained may be subjected to change from time to time by us and/or the competent authorities as may be required and do not form part of an offer or contract. Renderings, depictions and illustrations are artistic impressions. 
                            Photographs do not necessarily represent as-built standard specifications. Floor areas are approximate measurements and are subject to final survey. Likewise the model(s) and showflats are artistic impressions only and should not be considered as representation of fact.
                            
                        
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="msd_assets/js/msd_functions.js"></script>
        
        <!-- MSD JS INCLUDES -->
        <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>





           
	</body>
</html>
