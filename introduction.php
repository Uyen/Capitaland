<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
        <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
        
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MDVCFTH');</script>
		<!-- End Google Tag Manager -->
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P959B4G');</script>
		<!-- End Google Tag Manager -->
		
	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P959B4G"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-MDVCFTH""
		height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar " >
            <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        
                        <div class="main-menu" id="navbarNavDropdown">
                            <ul>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Design concept</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop1">
                                        <a class="dropdown-item" href="#">Introduction (project concept/ Somerset services</a>
                                        <a class="dropdown-item" href="#">Photo mock up unit</a>
                                        <a class="dropdown-item" href="#">Photo facilities</a>
                                        <a class="dropdown-item" href="#">Photo view 360 degree</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Location</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop2">
                                        <a class="dropdown-item" href="#">Location map</a>
                                        <a class="dropdown-item" href="#">Photo facilities near by</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Unit layout</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop3">
                                        <a class="dropdown-item" href="#">Facilities Plan</a>
                                        <a class="dropdown-item" href="#">Floor plan</a>
                                        <a class="dropdown-item" href="#">Unit plan</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Media</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop4">
                                        <a class="dropdown-item" href="#">Mews</a>
                                        <a class="dropdown-item" href="#">E-brochure</a>
                                        <a class="dropdown-item" href="#">Project update</a>
                                        <a class="dropdown-item" href="#">Video events</a>
                                        <a class="dropdown-item" href="#">Video lifestyle/location/teaser</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Promotion</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop5">
                                        <a class="dropdown-item" href="#">New sales program/promotion</a>
                                    <!--</div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Developer/ contact </a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop6">
                                        <a class="dropdown-item" href="#">Developers CL/ The Ascott</a>
                                        <a class="dropdown-item" href="#">introduction</a>
                                        <a class="dropdown-item" href="#">Letter CEO/ The Ascott</a>
                                        <a class="dropdown-item" href="#">Registration</a>
                                    </div>-->
                                </li>
                            </ul>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="introduction-vn.php" data-toggle="elementscroll">Design concept/ inspiration</a></li>
                                <li><a href="location-vn.php" data-toggle="elementscroll">Location</a></li>
                                <li><a href="residences-vn.php" data-toggle="elementscroll">Unit layout</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Media</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Developer/ Contact</a></li>
                                <li id="mnuReg" class="clsGoldbg" data-toggle="elementscroll"><a href="register-vn.php">Register</a></li>
                            </ul>

                        </div>
                    </div>
                    <div id="divLangToggle" > <a href="introduction.php">EN</a> / <a href="introduction-vn.php"> VN</a> </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <!--<div class="texture-overlay"></div> -->
            
			
		</header>
        
        
        <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2 hidden-xs" data-parallax="scroll" data-image-src="img/hero.jpg"> 

                    <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div>
                </div>
                
                <img id="imgIntroKeyVis-mobile" src="img/intro-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                <div  id="divPgKeyOuterTxtWrap">
                
                    <div id="divIntroPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >
                        
                        
                         <div id="divIntroPgKeyVisHeadTxt1" class="clsHeader1">
                            
                            <span class="clsHeaderBigFont  clsHeaderNormalFont  clsHeader1">EXCEPTIONAL</span> 
                            
                        </div>

                        <div id="divIntroPgKeyVisHeadTxt2">
                            <span class="clsHeaderSmallFont clsHeaderItalicFont clsHeader1" style="text-transform: capitalize;">In</span> 
                            <span class="clsHeaderSmallFont  clsHeaderItalicFont  clsHeader1" style="text-transform: capitalize;">Every</span> 
                            
                            <span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">SENSE  </span>
                        </div>
                        


                        
                    </div>

                </div>
            </div>    
           

        </section>

           
         <section id="secIntroPgPara" class="parllaxpadding">
            <div class="container ">
                
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 text-center whitebg">
                    
                        <img src="img/spareimage.png" id="imgIntroMid" alt="" class="img-responsive"/ style="max-width:550px; margin:0 auto; width:100%;">
                    </div>
                
                </div>
                <div class="row clsPurple2" >
                    <div class="col-md-12 col-xs-12 whitebg"  >
                        
                        <div id="divIntroPgParaWrap">
                            <p>
                                Located in the prestigious District 1 of Ho Chi Minh City, Vietnam, D1MENSION is brought to you by reputable Singapore-listed property developer, CapitaLand. Meeting the need for a true luxury 
                                development in a flourishing real estate market, D1MENSION is well placed in terms of product positioning and offering.
                            </p>
                            <p>Managed by The Ascott Limited, the world’s largest international serviced residence owner-operator, the development comprises two high rise towers, Dimension and Somerset, with 102 exclusive residential units in Dimension and 200 serviced apartments in Somerset, 
                                both complete with concierge services.
                            </p>
                            <p>At D1MENSION, quality of life is never compromised. </p>
                        </div>
                
                        
                    </div>
                </div>
                
               
            
            
            </div>
        
        
        
        
        </section>
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                            We have used reasonable care in preparing this website and in constructing the model(s) and showflats. Please note, however, that neither our agents nor we will be held responsible for any inaccuracy in the contents of this website. 
                            Whilst we believe the contents of this website to be correct and accurate and correct at the time of publication, they are not to be regarded as statements or representations of fact. All information, 
                            specifications and plans herein contained may be subjected to change from time to time by us and/or the competent authorities as may be required and do not form part of an offer or contract. Renderings, depictions and illustrations are artistic impressions. 
                            Photographs do not necessarily represent as-built standard specifications. Floor areas are approximate measurements and are subject to final survey. Likewise the model(s) and showflats are artistic impressions only and should not be considered as representation of fact.
                            
                        
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
    <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/js/msd_functions.js"></script>
        
        <!-- MSD JS INCLUDES -->
        
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>

        <div class="brightbox"></div>


    <?php include_once('google_tracking.php') ?>
	</body>
</html>
