<?php
$image_src_en = 'img/banner_27_11.jpg';
$image_src_vn = '';
$image_src    = $image_src_en;
function get_image_popup($lang = 'en')
{
    if ($lang == 'vn') {
        $image_src = $image_src_vn;
    }
}
$is_show         = true;
$end_time_string = '2017-11-27 6:00:00';
$current_time    = new DateTime('now', new DateTimeZone('UTC'));
$end_time        = new DateTime($end_time_string, new DateTimeZone('Asia/Ho_Chi_Minh'));
if ($end_time) {
    $end_time->setTimezone(new DateTimeZone('UTC'));
    $is_show = ($current_time->getTimestamp() < $end_time->getTimestamp()) ? true : false;
}
//CHECK SHOW POPUP
if ($is_show === true) {
    ?>
        <!-- ALL POPUP HERE -->

        <style type="text/css" media="screen">
            #popup_overlay{
              background-color: #222;
            }
            .popup_banner_content img{
                width: 100%;
            }

            .popup_banner{
               position: absolute;
            }
            @media (min-width: 768px)
            {
              .popup_banner{
                max-width: 960px;
                max-height: 600px;
              }
            }
            .popup_banner button.close{
              position: absolute;
              right: 7px;
            }
        </style>
        <div class="modal" id="popup_overlay">
        </div>
        <div class="modal" id="popup_container">
            <div class="popup_banner">
              <button type="button" class="close">&times;</button>
              <div class="popup_banner_content">
                  <img src="img/banner_27_11.jpg" alt="banner quảng cáo">
              </div>
            </div>
        </div>
   <!-- end Modal content-->
         <script>
          var is_popup_open = false;
           function show_popup() {
            is_popup_open = true;
              $('#popup_overlay').show();
              $('#popup_overlay').css('opacity', 0.8);
              $('#popup_container').show();
              var jele = $('#popup_container').find('.popup_banner');
              if(jele.length){
                var viewport_size = device_viewport();

                if(viewport_size.width <= 767){ // Mobile
                  jele.width(viewport_size.width);
                  jele.css({
                    'height' : 'auto'
                  });
                }
                var popup_width = jele.width();
                var popup_height = jele.height();
                var left = parseInt((viewport_size.width - popup_width)/2);
                var top = parseInt((viewport_size.height - popup_height)/2);
                jele.css({
                  'left' : left + 'px',
                  'top' : top + 'px'
                });
                jele.find('button.close').unbind('click').bind('click',hide_popup);
              }
           }
           function hide_popup() {
            is_popup_open = false;
            $('#popup_overlay').hide();
            $('#popup_container').hide();
           }

           function device_viewport() {
            var e = window, a = 'inner';
            if (!('innerWidth' in window )) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return { width : e[ a+'Width'] , height : e[ a+'Height']};
          }
          $(document).ready(function(){
           setTimeout(show_popup,500);
          })
          $(window).on("load resize",function() {
            if(is_popup_open){
             setTimeout(show_popup,500)
            }
          });
        </script>
        <?php }?>