$(document).ready(function(){

	msd_slider_init('slider1','sliderdata1');
	msd_slider_init('slider2','sliderdata2');
	msd_slider_init('slider3','sliderdata3');
	msd_slider_init('slider4','sliderdata4');
	
	
	function msd_slider_init(a,b){
		
		var ctr1=0;	
		var ctr2=0;
		var aa1=0;
		var lgnth=$('#'+b+' .data_a').length;
		  
		// initialise data and display
		$('#'+a+' .msd_slider_col_box:eq(0)').html(msd_fetchData(b,'a',0));
		$('#'+a+' .msd_slider_col_box:eq(1)').html(msd_fetchData(b,'b',0));
		$('#'+a+' .msd_slider_colset:eq(1)').css({'display':'none'});
		$('#'+a+' .controlx').append('<div class="blocker"></div>');
		msd_updateHeight(a);
		setTimeout(function(){msd_updateHeight(a);},100);
		$(document).scroll(function(){msd_updateHeight(a);});
		
		
		$('#'+a+' .controlx_btn').click(function(){
			
			  ctr2++;
			  if(ctr2>1){ctr2=0;}
				  
			  aa1=$(this).index();
			  if(aa1===0){
				  ctr1--;
				  if(ctr1<0){ctr1=(lgnth-1);}
				  msd_changeSlide(a,ctr1,ctr2,0,b);
			  }
			  if(aa1===1){
				  ctr1++;
				  if(ctr1>(lgnth-1)){ctr1=0;}
				  msd_changeSlide(a,ctr1,ctr2,1,b);
			  }
			 $('#'+a).css({'height':$('#'+a+' .active').height()+'px'});
		});
		
		
		
		$(window).resize(function() {
			msd_updateHeight(a);
		});
		
		
		
	}
	
	function msd_changeSlide(a,b,c,d,e){	//a-slider, b-point to content, c(0/1)-swap colset, d(0/1)-left or right cursor, e-sliderdata
	
	$('#'+a+' .blocker').css({'display':'block'});
	$('#'+a+' .blocker').delay(1050).fadeOut(0);
		
		$('#'+a+' .msd_slider_colset').removeClass('active');
		$('#'+a+' .msd_slider_colset:eq('+c+')').addClass('active');
		//$('#'+a).css({'height':$('#'+a+' .msd_slider_colset:eq('+xx+')').height()+'px'});
		//$('#'+a).css({'height':$('#'+a+' .active').height()+'px'});
		
		for(var xx=0; xx<2; xx++){
			
			if($('#'+a+' .msd_slider_colset:eq('+xx+')').hasClass('active')){
		
				$('#'+a+' .msd_slider_colset:eq('+xx+')').css({'display':'block'});
				
				if(d===0){msd_slideAnimate(a,xx,1,[0,0],[0,0],[-100,0],[-200,0],[0,1],[900,1000]);}else{msd_slideAnimate(a,xx,1,[0,0],[0,0],[100,0],[200,0],[0,1],[900,1000]);}
		
			} else{
				
				if(d===0){msd_slideAnimate(a,xx,0,[0,0],[0,0],[0,0],[0,0],[1,0],[600,700]);}else{msd_slideAnimate(a,xx,0,[0,0],[0,0],[0,0],[0,0],[1,0],[600,700]);}
			}		
		
		}
		
		
		$('#'+a+' .msd_slider_colset:eq('+c+') .msd_slider_col_box:eq(0)').html(msd_fetchData(e,'a',b));
		$('#'+a+' .msd_slider_colset:eq('+c+') .msd_slider_col_box:eq(1)').html(msd_fetchData(e,'b',b));
		
	}
	
	function msd_updateHeight(a){
		$('#'+a).css({'height':$('#'+a+' .active').height()+'px'});
		$('#'+a+' .controlx').css({'top':(parseInt($('#'+a).height()/2))+'px'});
	}
	
	function msd_slideAnimate(a,b,c,y1,y2,x1,x2,op1,spd1){ //a-slider, b-activeslide, y1&x1-image, y2&x2-description, op1-opacity, spd1-speed 
	
				$('#'+a+' .msd_slider_colset:eq('+b+') .msd_slider_col:eq(0)').transition({ y:y1[0], x:x1[0], opacity:op1[0]},0);
				$('#'+a+' .msd_slider_colset:eq('+b+') .msd_slider_col:eq(0)').transition({ y:y1[1], x:x1[1], opacity:op1[1]},spd1[0]);
				$('#'+a+' .msd_slider_colset:eq('+b+') .msd_slider_col:eq(1)').transition({ y:y2[0], x:x2[0], opacity:op1[0]},0);
				$('#'+a+' .msd_slider_colset:eq('+b+') .msd_slider_col:eq(1)').transition({ y:y2[1], x:x2[1], opacity:op1[1]},spd1[1],function(){
				if(c===0){
					$('#'+a+' .msd_slider_colset:eq('+b+') .msd_slider_col:eq(0), #'+a+' .msd_slider_colset:eq('+b+') .msd_slider_col:eq(1) ').transition({ y:0, x:0, opacity:0},0);
				}
				});
	
	}
	
	function msd_fetchData(a,b,c){ // a - sliderdata, b - data column, c - data row
		return $('#'+a+' .data_'+b+':eq('+c+')').html();
	}
	
	$('.controlx_btn').click(function() {
    return false;
	});

});