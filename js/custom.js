$(document).ready(function() {
	for (var i = 1; i < 5; i++) {
		$('.lgnd_' + i).mouseenter( function() {
			$(this).css('cursor','pointer');
			$(this).find('span').css('box-shadow','0 0 10px 3px #981d1d');
		}).mouseleave( function () {
			$(this).find('span').css('box-shadow','none');
		});
	}

	for (var i = 5; i < 14; i++) {
		$('.lgnd_' + i).mouseenter( function() {
			$(this).css('cursor','pointer');
			$(this).find('span').css('box-shadow','0 0 10px 3px #981d1d');
		}).mouseleave( function () {
			$(this).find('span').css('box-shadow','none');
		});
	}

	for (var i = 14; i < 24; i++) {
		$('.lgnd_' + i).mouseenter( function() {
			$(this).css('cursor','pointer');
			$(this).find('span').css('box-shadow','0 0 10px 3px #981d1d');
		}).mouseleave( function () {
			$(this).find('span').css('box-shadow','none');
		});
	}
});