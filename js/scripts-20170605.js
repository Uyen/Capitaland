$(document).ready(function() {

	/***************** Waypoints ******************/

	$('.wp1').waypoint(function() {
		$('.wp1').addClass('animated fadeInLeft');
	}, {
		offset: '75%'
	});
	$('.wp2').waypoint(function() {
		$('.wp2').addClass('animated fadeInDown');
	}, {
		offset: '75%'
	});
	$('.wp3').waypoint(function() {
		$('.wp3').addClass('animated bounceInDown');
	}, {
		offset: '75%'
	});
	$('.wp4').waypoint(function() {
		$('.wp4').addClass('animated fadeInDown');
	}, {
		offset: '75%'
	});

	/***************** Flickity ******************/

	$('#featuresSlider').flickity({
		cellAlign: 'left',
		contain: true,
		prevNextButtons: false
	});

	$('#showcaseSlider').flickity({
		cellAlign: 'left',
		contain: true,
		prevNextButtons: false,
		imagesLoaded: true
	});

	/***************** Fancybox ******************/
	$(".youtube-media").on("click", function(e) {
		
		
		var jWindow = $(window).width();
		if (jWindow <= 768) {
			return;
		}
		$.fancybox({
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic',
            
			href: this.href,
			padding: 4,
            'showCloseButton' : true,
			type: "iframe",
			'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
		});
		
						
						/* $('.fancybox-overlay').append('<div class="blackbox_control"><span>CLOSE - X</span></div>'); */
						$('.fancybox-overlay').css({'display':'none'});
						$('.fancybox-overlay').fadeIn(200);
						
                        /*
						$('.fancybox-overlay .blackbox_control').click(function(){
                            
                            
	
							$('.fancybox-wrap').fadeOut(400,function(){
								
										$('.fancybox-overlay').fadeOut(400);
                                        
								
								});
						});

                        */
		
		return false;
	});

});






$(document).ready(function() {
	$("a.single_image").fancybox({
		padding: 4,
	});
});

/***************** Nav Transformicon ******************/



/***************** Smooth Scrolling ******************/


$('[data-toggle="elementscroll"]').click(function() {
	if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html,body').animate({
				scrollTop: target.offset().top
			}, 2000);
			return false;
		}
	}
}); 


/***************** OTHERS ******************/
new WOW().init();   //Initiat WOW JS 
$('body').scrollspy({ target: '#my-nav' }); //Initiat Scrollspy

//Remove focus on clicked item 
$(".nav-pills").mouseup(function(){
    $(':focus').blur();
});

//In collapsed menu - Hide menu after click on a menu item
$('.nav:eq(0) a').on('click', function(){
   $('.navbar-toggle').click() //bootstrap 3.x by Richard
   
});
$('.nav:eq(1) a').on('click', function(){
$('.btn-navbar').click(); //bootstrap 2.x
   
});

$(document).ready(function(){
		
$('.brightbox').css({'position':'fixed','width':'100%','height':'100%','top':'0','left':'0','z-index':'9999','background-color':'#fff'});
$('.brightbox').delay(300).fadeOut(600);

// Custom Fonts for VN Version

var a = getCurentFileName();

if((a.indexOf('vn.') > -1)===true){
	
	$('p').css({'font-family':'Asap-Regular'});
	//$('.clsHeaderItalicFontViet').css({'font-family':'Didot'});
	$('.clsHeaderBigFont').addClass('clsHeaderBigFont_vn');
	$('.clsHeaderSmallFont').addClass('clsHeaderSmallFont_vn');
	
}

	function getCurentFileName(){
		var pagePathName= window.location.pathname;
		return pagePathName.substring(pagePathName.lastIndexOf("/") + 1);
	}
	
	
	
});