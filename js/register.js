 $(document).ready(function() {
      

      
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            full_name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please key-in your Full Name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please key-in your Email address'
                    },
                    emailAddress: {
                        message: 'Please key-in a valid Email address'
                    }
                }
            },
            phonenumber: {
                validators: {
                    notEmpty: {
                        message: 'Please key-in your Phone Number'
                    }
                }
            },
            countrycode: {
                validators: {
                    notEmpty: {
                        message: 'Please key-in Country Code'
                    }
                }
            },
            captcha: {
                validators: {
                     
                    notEmpty: {
                        message: 'Please enter captcha '
                    }
                }
            },
            
            district: {
                validators: {

                    notEmpty: {
                        message: 'Please key-in District'
                    }
                }
            },
            country: {
                validators: {
                    
                    notEmpty: {
                        message: 'Please key-in your Country Name'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'Please select Gender'
                    }
                }
            },
            yob: {
                validators: {
                    notEmpty: {
                        message: 'Please select Year Of Birth'
                    }
                }
            },
            bedrooms: {
                validators: {
                    notEmpty: {
                        message: 'Please select No Of Bedrooms'
                    }
                }
            },
            floorlevel: {
                validators: {
                    notEmpty: {
                        message: 'Please select Floor Level'
                    }
                }
            },
            pricerange: {
                validators: {
                    notEmpty: {
                        message: 'Please select Price Range'
                    }
                }
            },
            purpose: {
                validators: {
                    notEmpty: {
                        message: 'Please select Purpose'
                    }
                }
            },
            aboutdev: {
                validators: {
                    notEmpty: {
                        message: 'Please select How Do You Know About Development'
                    }
                }
            },
            prevprojs: {
                validators: {
                    notEmpty: {
                        message: 'Please select Have you purchased any of CapitaLand’s projects before? '
                    }
                }
            },
            tc: {
                validators: {
                    notEmpty: {
                        message: 'Please check to accept Terms & Conditions '
                    }
                }
            },
            city: {
                validators: {
                     
                    notEmpty: {
                        message: 'Please key-in City'
                        }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            //$('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              //  $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();
         var $form = $(e.target),
        fv    = $(e.target).data('bootstrapValidator');
            fv.defaultSubmit();

            // Get the form instance
            //var $form = $(e.target);

            // Get the BootstrapValidator instance
            //var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            //$.post($form.attr('action'), $form.serialize(), function(result) {
                //console.log(result);
            //}, 'json');
        });
});

