var windowWidth = window.innerWidth;

if(windowWidth > 450) {
    // run your animations here 
    
    // OSR - Begin hiding menu at home pages
    $(".navbar").hide();
    $(document).ready(function(){

         $(".main-navbar").removeClass("hidden");

        // hide .navbar first
        $(".navbar").hide();

        // fade in .navbar
        $(function () {
            $(window).scroll(function () {
                // set distance user needs to scroll before we fadeIn navbar
                if ($(this).scrollTop() > 100) {
                    $('.navbar').fadeIn();
                } else {
                    $('.navbar').fadeOut();
                }
            });


        });

    });
    // OSR - End hiding menu at home pages

}

else
{
    $(".main-navbar").removeClass("hidden");
}




