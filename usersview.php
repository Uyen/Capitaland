<?php require_once("library/check_session.php.inc"); ?>
<?php require_once("library/db_connect.php.inc"); ?>
<?php require_once("library/functions.php.inc"); ?>
<?php include_once("library/ps_pagination_simple.php") ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
        <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
        
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <link href="css/regform.css" rel="stylesheet"> 
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
        
        <style>
            body, tbody{color:black;}
            a  {color:black;}
            a:hover {color:blue;}
        </style>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P959B4G');</script>
		<!-- End Google Tag Manager -->
				
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MDVCFTH');</script>
		<!-- End Google Tag Manager -->
	</head>
	<body>
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P959B4G"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-MDVCFTH""
		height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar " >
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="index.html"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        <span class="breadcrumbx">Page name goes here</span>
                        
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked">
                                
                                <li><a href="index.html">HOME  </a></li>
                                <li><a href="introduction.html">INTRODUCTION</a></li>
                                <li><a href="location.html" >LOCATION </a></li>
                                <li><a href="residences.html" >RESIDENCES</a></li>
                                <li><a href="developer.html" >DEVELOPER</a></li>
                                <li id="mnuReg" class="clsGoldbg active"  ><a href="register.html">REGISTER  </a></li>

                            </ul>



                        </div>
                    </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <div class="texture-overlay"></div>
            
			
		</header>
        
       
       
        <!-- OSR - Begin Body -->
        
        
                
            <?php

							
                if ( $sessionUser != 1) { redirect("adminlogin.php?return_url=" . getPageId() . ".php&event=Authorization Failure"); exit; } 

               

               

            ?>
            
                        <section  id="secLoginForm" >
                <div class="container" id="secLoginFormContainer" >
                    <div class="row">
                    
                        <?php

                            if ( $sessionUser != 1) { redirect("adminlogin.php?return_url=" . getPageId() . ".php&event=Authorization Failure"); exit; } 
                        ?>
                        
                        
                        <div id="regDataTableWrap">
                        <div class="table-responsive text-center regtable" >
                           
                            
                           

                            <div class="col-lg-12 text-right ">
                                
                                <?php
                                
                                if ( $_SESSION['usertype'] != 10) { 
                                        echo "<span class='clsRed'><center><br />You are not authorized to access User Management Area</center></span> <br / > <br /> <a href='admincp.php'>Home</a>"; 

                                    } 
                                else

                                    {
                                        $sql_SelectTable = "";
                                        $sql_listing = "";
                                        $sql_order = "";

                                        $sql_SelectTable = "SELECT * ";

                                        $sql_listing .= " FROM tblusers   ";

                                        $sql_order .= " order by tblusers.userid asc  ";

                                        $sql_listing_result = $sql_SelectTable.$sql_listing. $sql_order ;




                                        $pager = new PS_Pagination($connect, $sql_listing_result, 10, 5, "status=$varStatus");
                                        $pager->setDebug(true);
                                        $result_listing = $pager->paginate();


                                    

                                ?>
                                
                              
                                <span class="clsGold" > <a href="addnewuser.php"><br /> >> Add New User</a> <br /></span>
                                <a href="admincp.php"><span class="clsGoldLight" >Home</a> |</span> <a href="logout.php"><span class="clsGoldLight" >Logout</a></span>
                                
                                <?php
                                    
                                    }
                                ?>
                            
                            <br /><br />
                            </div>
                            
                            
                            <br />
                            
                            <table class="table table-bordered " style="">
                                <thead  class=" clsBlack biggerFont" >
                                    <th class="clsGoldBg text-center">User</th>
                                    <th class="clsGoldBg"></th>
                                    <th class="clsGoldBg"></th>
                                    

                                </thead>

                                <tbody>
                                  <tr>

                                  </tr>
                                    
                                    
                                    <?php
                                        if(!$result_listing) die(mysql_error());
                                            $tmpCount = 0;
                                            while ($row = mysql_fetch_array($result_listing)) {
                                                $tmpCount ++;
                                            ?>
                                                <?php //echo print_r($row);?>


                                                <tr>
                                                    <td><?php echo $row['userid']?></td>
                                                    
                                                    <td><a href="changeuserpw.php?ue=<?php echo $row['usid'] ?>">Change Password</a></td>
                                                    <td><a href="deleteuser.php?ue=<?php echo $row['usid'] ?>">Delete</a></td>
                                                    
                                                  
                                                    
                                                    
                                                    
                                                    
                                                </tr>

                                                <?php
                                            }

                                            //Display the full navigation in one go
                                           // echo "<br><br>Total Records: " .$pager->total_rows;
                                           // echo "<br>";
                                            //echo $pager->renderFullNav();
                                            
                                           // echo "<br /><br>\n";

                                        ?>
                                        
                                    
                                </tbody>
                            </table>
                            <br />
                            
                            
                           
                        </div>
                    </div>

                         
                    </div>
 
                    
                </div>
                
                
            </section>
        
        
        
        <!-- OSR - End Body -->
         
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                            We have used reasonable care in preparing this website and in constructing the model(s) and showflats. Please note, however, that neither our agents nor we will be held responsible for any inaccuracy in the contents of this website. 
                            Whilst we believe the contents of this website to be correct and accurate and correct at the time of publication, they are not to be regarded as statements or representations of fact. All information, 
                            specifications and plans herein contained may be subjected to change from time to time by us and/or the competent authorities as may be required and do not form part of an offer or contract. Renderings, depictions and illustrations are artistic impressions. 
                            Photographs do not necessarily represent as-built standard specifications. Floor areas are approximate measurements and are subject to final survey. Likewise the model(s) and showflats are artistic impressions only and should not be considered as representation of fact.
                            
                        
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
        <script type="text/javascript" src="js/bootstrapValidator.min.js"> </script>
        <script src="js/register.js"></script>
        
        
        
        <!-- MSD JS INCLUDES -->
        <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>
        <script src="msd_assets/js/msd_functions.js"></script>


        <div class="brightbox"></div>

           
	</body>
</html>
