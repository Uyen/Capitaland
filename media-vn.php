

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
      <meta name="description" content="" />
      <meta name="keywords" content="" />
      <meta name="author" content="F8" />
      <!-- Bootstrap -->
      <!-- meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'"> -->
      <meta http-equiv="Content-Security-Policy" content="default-src *;
         img-src * 'self' data: https:; script-src 'self' 'unsafe-inline' 'unsafe-eval' *;
         style-src  'self' 'unsafe-inline' *">
      <script src="js/modernizr.custom.js"></script>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/jquery.fancybox.css" rel="stylesheet">
      <link href="css/flickity.css" rel="stylesheet" >
      <link href="css/animate.css" rel="stylesheet">
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <link href="css/styles.css" rel="stylesheet">
      <link href="css/innerpages.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <!-- Facebook and Twitter integration -->
      <meta property="og:title" content=""/>
      <meta property="og:image" content=""/>
      <meta property="og:url" content=""/>
      <meta property="og:site_name" content=""/>
      <meta property="og:description" content=""/>
      <meta name="twitter:title" content="" />
      <meta name="twitter:image" content="" />
      <meta name="twitter:url" content="" />
      <meta name="twitter:card" content="" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <link rel="shortcut icon" href="assets/images/favicon.ico">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
      <link rel="apple-touch-icon-precomposed" href="img/favicon.png">
      <link rel="icon" type="image/png" href="favicon.ico" />
      <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
      <!-- MSD CSS INCLUDES -->
      <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
      <link href="css/media.css" rel="stylesheet">
   </head>
   <body>
      <header>
         <div class="main-navbar ">
            <nav class="navbar navbar-inverse navbar-fixed-top">
               <div class="navbar-header">
                  <a class="navbar-brand" href="./"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                  <div class="main-menu" id="navbarNavDropdown">
                     <ul>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" id="menu_drop1">Design concept</a>
                           <div class="dropdown-menu" aria-labelledby="menu_drop1">
                              <a class="dropdown-item" href="#">Introduction (project concept/ Somerset services</a>
                              <a class="dropdown-item" href="#">Photo mock up unit</a>
                              <a class="dropdown-item" href="#">Photo facilities</a>
                              <a class="dropdown-item" href="#">Photo view 360 degree</a>
                           </div>
                        </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" id="menu_drop2">Location</a>
                           <div class="dropdown-menu" aria-labelledby="menu_drop2">
                              <a class="dropdown-item" href="#">Location map</a>
                              <a class="dropdown-item" href="#">Photo facilities near by</a>
                           </div>
                        </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" id="menu_drop3">Unit layout</a>
                           <div class="dropdown-menu" aria-labelledby="menu_drop3">
                              <a class="dropdown-item" href="#">Facilities Plan</a>
                              <a class="dropdown-item" href="#">Floor plan</a>
                              <a class="dropdown-item" href="#">Unit plan</a>
                           </div>
                        </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" id="menu_drop4">Media</a>
                           <div class="dropdown-menu" aria-labelledby="menu_drop4">
                              <a class="dropdown-item" href="media-vn.php">News</a>
                              <a class="dropdown-item" href="residences-vn.php">E-brochure</a>
                              <a class="dropdown-item" href="#">Project update</a>
                              <a class="dropdown-item" href="#">Video events</a>
                              <a class="dropdown-item" href="#">Video lifestyle/location/teaser</a>
                           </div>
                        </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="promotions-vn.php" id="menu_drop5">Promotion</a>
                           <!-- <div class="dropdown-menu" aria-labelledby="menu_drop5">
                              <a class="dropdown-item" href="#">New sales program/promotion</a>
                           </div> -->
                        </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link dropdown-toggle" href="#" id="menu_drop6">Developer/ contact </a>
                           <div class="dropdown-menu" aria-labelledby="menu_drop6">
                              <a class="dropdown-item" href="#">Developers CL/ The Ascott</a>
                              <a class="dropdown-item" href="#">introduction</a>
                              <a class="dropdown-item" href="#">Letter CEO/ The Ascott</a>
                              <a class="dropdown-item" href="#">Registration</a>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
               </div>
               <div id="msd-navbar">
                  <div id="my-nav" class="navbar-collapse collapse text-center">
                     <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="introduction-vn.php" data-toggle="elementscroll">Design concept/ inspiration</a></li>
                        <li><a href="location-vn.php" data-toggle="elementscroll">Location</a></li>
                        <li><a href="residences-vn.php" data-toggle="elementscroll">Unit layout</a></li>
                        <li><a href="developer-vn.php" data-toggle="elementscroll">Media</a></li>
                        <li><a href="developer-vn.php" data-toggle="elementscroll">Developer/ Contact</a></li>
                        <li id="mnuReg" class="clsGoldbg" data-toggle="elementscroll"><a href="register-vn.php">Register</a></li>
                     </ul>
                  </div>
               </div>
               <div id="divLangToggle"> <a href="media.php">EN</a> / <a href="media-vn.php"> VN</a> </div>
            </nav>
         </div>
         <div id="main-navbar-cover"></div>
         <div class="texture-overlay"></div>
      </header>
      
      <!--<div class="texture-overlay"></div>-->
      <div class="wrapper media-page">
         <!-- Section - Media -->
         <section class="section section-auto media-page" id="media" data-section-name="media">
            <article class="d-flex flex-wrap align-items-center">
               <div class="d-flex col-12 p-4 justify-content-center">
                  <div class="article-inner col-12 col-lg-10 text-left p-lg-5 d-flex flex-column justify-content-center">
                     <h4 class="headline">
                        <span class="ml-0">
                        <small>NEWS/MEDIA</small>
                        </span>
                     </h4>
                     <div class="media-wrapper">
                        <ul class="d-flex flex-wrap pt-4 align-items-stretch">
                           <li class="item">
                              <!-- de-la-sol-video-emergence-cbd-lite -->
                              <a data-toggle="modal" data-target="#de-la-sol-video-emergence-cbd-lite">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_002.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>26 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>De La Sol by CapitaLand: The Emergence of the CBD Lite</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- de-la-sol-video-feel-the-pulse -->
                              <a data-toggle="modal" data-target="#de-la-sol-video-feel-the-pulse">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_004.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>22 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>Feel The Pulse Of The CBD Lite. All In One Place</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- event-project-introduction -->
                              <a data-toggle="modal" data-target="#event-project-introduction">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_006.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>20 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>De La Sol Project Introduction</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- sales-gallery-grand-opening -->
                              <a data-toggle="modal" data-target="#sales-gallery-grand-opening">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_005.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>12 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>The Grand Opening of Sales Gallery & Show Suites</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- show-suites-vip-preview -->
                              <a data-toggle="modal" data-target="#show-suites-vip-preview">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_007.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>9 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>Show Suites VIP Preview</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- signing-ceremony-4-may -->
                              <a data-toggle="modal" data-target="#signing-ceremony-4-may">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>4 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>Signing Ceremony of Strategic Partnership with Exclusive Distribution Agencies</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </article>
         </section>
      </div>
      <!-- Media Modals [Auto-generated by Jekyll]-->
      <!-- Modal for "Signing Ceremony of Strategic Partnership with Exclusive Distribution Agencies" -->
      <div class="modal modal-media fade" id="signing-ceremony-4-may" tabindex="-1" role="dialog" aria-labelledby="signing-ceremony-4-may" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="signing-ceremony-4-may-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media04.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#signing-ceremony-4-may-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#signing-ceremony-4-may-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>4 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "Show Suites VIP Preview" -->
      <div class="modal modal-media fade" id="show-suites-vip-preview" tabindex="-1" role="dialog" aria-labelledby="show-suites-vip-preview" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="show-suites-vip-preview-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/SRXmhlvreVo?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#show-suites-vip-preview-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#show-suites-vip-preview-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>9 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "The Grand Opening of Sales Gallery & Show Suites" -->
      <div class="modal modal-media fade" id="sales-gallery-grand-opening" tabindex="-1" role="dialog" aria-labelledby="sales-gallery-grand-opening" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="sales-gallery-grand-opening-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/09.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/AWhF3EDkxaM?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#sales-gallery-grand-opening-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#sales-gallery-grand-opening-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>12 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "De La Sol Project Introduction" -->
      <div class="modal modal-media fade" id="event-project-introduction" tabindex="-1" role="dialog" aria-labelledby="event-project-introduction" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="event-project-introduction-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/EOID3Ir4NWE?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#event-project-introduction-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#event-project-introduction-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>20 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "Feel The Pulse Of The CBD Lite. All In One Place" -->
      <div class="modal modal-media fade" id="de-la-sol-video-feel-the-pulse" tabindex="-1" role="dialog" aria-labelledby="de-la-sol-video-feel-the-pulse" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-feel-the-pulse-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch active">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/4wHTRsZUDks?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>22 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "De La Sol by CapitaLand: The Emergence of the CBD Lite" -->
      <div class="modal modal-media fade" id="de-la-sol-video-emergence-cbd-lite" tabindex="-1" role="dialog" aria-labelledby="de-la-sol-video-emergence-cbd-lite" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch active">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/AJ5TOfAHBlk?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <footer>
            <div class="container-fluid">
                
                <div class="row  left-margin">
                    <div class="col-md-8">
                        <p>
                        
                            Chúng tôi đã đặc biệt thận trọng trong việc chuẩn bị nội dung tài liệu này cũng như các bản mẫu. Tuy nhiên vui lòng lưu ý rằng bản thân tập đoàn, cũng như các đại lý bán hàng, sẽ không chịu trách nhiệm cho những thông tin chưa chính xác trong tài liệu này. Tuy bản thân chúng tôi thực sự tin tưởng rằng những nội dung dưới đây là hoàn toàn chính xác, nhưng tài liệu này sẽ không mang tính đại diện hay trở thành một tuyên ngôn riêng. Tất cả thông tin, những thông số đều có thể sẽ được thay đổi bởi chủ đầu tư hoặc bởi các nhà chức trách có thẩm quyền, bởi vậy mà đây không thể trở thành một phần của các hợp đồng hay đề nghị. Các bản vẽ, thiết kế hay hình ảnh đồ họa sẽ không đại diện chính xác cho điều kiện xây dựng thực tế. Các khu vực sàn được đo đạc kĩ lưỡng, có căn cứ từ các bản báo cáo. Trong khi đó, các hình ảnh đồ họa có thể không phản ánh hoàn toàn điều kiện thực tế.
                            
                        
                        </p>
                        
                       
                    </div>
                    <div class="col-md-4">
                        
                    </div>
                   
                    
                </div>
                
                <div class="row  left-margin">
                    <div class="col-md-8">
                        <p><span class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="ftBuild" src="img/ft-building.png" alt="" width="290" height="101">
                    </div>

                </div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol">
                      
                        <img id="f8logo" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55">
                    </div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol">
                        
                        <img id="caplandlogo" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109">
                    </div>
                    
                    <div class="col-md-2 cos-xs-12" id="divMangedbycol">
                        
                        <img id="ascottlogo" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121">
                    </div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
                        <img id="caplandlogoM" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109"> <br><br><br>
                    </div>
                    
                    <div class="col-md-4 cos-xs-12">
                        
                        <img id="ascottlogoM" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121"><br><br><br>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55"><br>
                    </div>
                </div>
                
                
            </div>
        </footer>
        <div class="fixed-bottom fix-bottom d-none bt-register-now">
        <a href="register.php" title="" id="bt-register-now" style="display: block;"><img src="img/register-now.png" alt="">
        </a>
      </div>
      <!-- Scripts -->
       <script>
        window.onscroll = function() {scrollFunction()};
        function scrollFunction() {
         if (document.body.scrollTop > 992 || document.documentElement.scrollTop > 992) {
             document.getElementById("bt-register-now").style.display = "block";
         } else {
             document.getElementById("bt-register-now").style.display = "none";
         }
        }

        </script>
      <script src="msd_assets/js/msd_functions.js"></script>
      <script src="js/app.js" charset="utf-8"></script>
      <script src="js/bootstrap.bundle.min.js"></script>
      <script src="js/parallax.min.js"></script>

      <script src="js/min/toucheffects-min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="js/flickity.pkgd.min.js"></script>
      <script src="js/jquery.fancybox.pack.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/retina.js"></script>
      <script src="js/waypoints.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/parallax.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/scripts.js"></script>
      <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
      <script src="msd_assets/js/msd_functions.js"></script>
      <!-- MSD JS INCLUDES -->
      <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>
      <script type="text/javascript">
         $(document).ready(function () {
          $("li").click(function () {
            

          });
         });
      </script>
   </body>
</html>

