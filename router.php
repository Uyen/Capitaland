<?php 
	$routes = new Routes();
	//bat dau khai bao routes
	//addRoute('bien constant', 'url', 'file view trong thu muc contents');
	
	
	$routes->addRoute('url-trang_chu', 'trang-chu', 'homepage-vn');
	$routes->addRoute('url-homepage', 'homepage', 'homepage-en');

	$routes->addRoute('url-gioi-thieu', 'gioi-thieu', 'introduction-vn');
	$routes->addRoute('url-introduction', 'introduction', 'introduction-en');

	$routes->addRoute('url-vi-tri', 'vi-tri', 'location-vn');
	$routes->addRoute('url-location', 'location', 'location-en');
	
	$routes->addRoute('url-tien-ich', 'tien-ich', 'facilities-vn');
	$routes->addRoute('url-facilities', 'facilities', 'facilities-en');
	
	$routes->addRoute('url-mat-bang-tang', 'mat-bang-tang', 'floor-plan-vn');
	$routes->addRoute('url-floor-plan', 'floor-plan', 'floor-plan-en');

	$routes->addRoute('url-hinh-anh-du-an', 'hinh-anh-du-an', 'gallery-vn');
	$routes->addRoute('url-gallery-vn', 'gallery', 'gallery-en');

	$routes->addRoute('url-thong-tin-truyen-thong','thong-tin-truyen-thong', 'media-vn');
	$routes->addRoute('url-media', 'media', 'media-en');

	$routes->addRoute('url-chinh-sach-khuyen-mai','chinh-sach-khuyen-mai', 'promotion-vn');
	$routes->addRoute('url-promotion', 'promotion', 'promotion-en');

	$routes->addRoute('url-nha-dau-tu', 'chu-dau-tu', 'developer-vn');
	$routes->addRoute('url-developer', 'developer', 'developer-en');

	$routes->addRoute('url-dang-ky', 'dang-ky', 'register-vn');
	$routes->addRoute('url-register', 'register', 'register-en');

	$routes->addRoute('url-thong-tin-lien-lac', 'thong-tin-lien-lac', 'contact-vn');
	$routes->addRoute('url-contac', 'contact', 'contact-en');

	$routes->addRoute('url-chinh-sach', 'chinh-sach', 'policy-vn');
	$routes->addRoute('url-policy', 'policy', 'policy-en');

	$routes->addRoute('url-dieu-khoan', 'dieu-khoan', 'term-vn');
	$routes->addRoute('url-term', 'term', 'term-en');

	//ket thuc khai bao routes

	if (empty($request_path) && strlen($request_path) == 0) {
		$request_path = 'trang-chu';
	}

	if ($view = $routes->getViewByRoute($request_path)) {
		render_view($view);
	} else {
    	render_view('page_not_found');
    }
    exit;
    
	/**
	 * 
	 */
	class Routes
	{

		public $data = array();
		public function addRoute($constant, $url, $view) {
			if (defined($constant)) {
				return;
			}
			define($constant, $url);
			$this->data [constant($constant)] = $view;
		}

		public function getRoutes() {
			return $this->data;
		}

		public function getViewByRoute($route) {
			if(isset($this->data [$route])) {
				return $this->data [$route];
			} else {
				return false;
			}
		}
	}
	
 ?>