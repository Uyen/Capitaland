<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
        <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
        
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
        <link href="msd_assets/css/msd_styles_location.css" rel="stylesheet">
        
        <style>
            #imgCeoSign{width: 30%;}
        
        </style>
        <style>
            .nav{font-family: 'TeXGyreTermes'}
        </style>
	</head>
	<body>
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar ">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="./"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        
                        <div class="main-menu" id="navbarNavDropdown">
                            <ul>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Design concept</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop1">
                                        <a class="dropdown-item" href="#">Introduction (project concept/ Somerset services</a>
                                        <a class="dropdown-item" href="#">Photo mock up unit</a>
                                        <a class="dropdown-item" href="#">Photo facilities</a>
                                        <a class="dropdown-item" href="#">Photo view 360 degree</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Location</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop2">
                                        <a class="dropdown-item" href="#">Location map</a>
                                        <a class="dropdown-item" href="#">Photo facilities near by</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Unit layout</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop3">
                                        <a class="dropdown-item" href="#">Facilities Plan</a>
                                        <a class="dropdown-item" href="#">Floor plan</a>
                                        <a class="dropdown-item" href="#">Unit plan</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Media</a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop4">
                                        <a class="dropdown-item" href="media-vn.php">News</a>
                                        <a class="dropdown-item" href="residences-vn.php">E-brochure</a>
                                        <a class="dropdown-item" href="#">Project update</a>
                                        <a class="dropdown-item" href="#">Video events</a>
                                        <a class="dropdown-item" href="#">Video lifestyle/location/teaser</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="promotions-vn.php" id="menu_drop5">Promotion</a>
                                   
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="menu_drop6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Developer/ contact </a>
                                    <div class="dropdown-menu" aria-labelledby="menu_drop6">
                                        <a class="dropdown-item" href="#developer-vn.php">Developers CL/ The Ascott</a>
                                        <a class="dropdown-item" href="introduction-vn.php">Introduction</a>
                                        <a class="dropdown-item" href="messagefromascott-vn.php">Letter CEO/ The Ascott</a>
                                        <a class="dropdown-item" href="register-vn.php">Registration</a>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="introduction-vn.php" data-toggle="elementscroll">Design concept/ inspiration</a></li>
                                <li><a href="location-vn.php" data-toggle="elementscroll">Location</a></li>
                                <li><a href="residences-vn.php" data-toggle="elementscroll">Unit layout</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Media</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Developer/ Contact</a></li>
                                <li id="mnuReg" class="clsGoldbg" data-toggle="elementscroll"><a href="register-vn.php">Register</a></li>
                            </ul>

                        </div>
                    </div>
                    <div id="divLangToggle"> <a href="index.php">EN</a> / <a href="index-vn.php"> VN</a> </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <div class="texture-overlay"></div>
            
         
      </header>
        
        <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/ascott_hero.jpg"> 

                    <div class="clsArtImpression2">
                        <span class="artimp1">  </span>
                    </div>
                </div>
                
                <img id="imgDevKeyVis-mobile" src="img/developer-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                <div  id="divPgKeyOuterTxtWrap">
                
                    <div id="divDevPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                            
                            
                            <div id="divDevPgKeyVisHeadTxt1" >
                                <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase">Thư ngỏ từ</span> 
                            </div>

                            <div id="divDevPgKeyVisHeadTxt2" >
                                
                               <span class="clsHeaderBigFont clsHeader1   clsHeaderNormalFont">Từ Ascott </span> 
                                    
                            </div>
                        </div>
                        

                    </div>

                </div>
            </div>    
           

        </section>

        <section class="developer-slider parllaxpadding" >
            <div class="container " id="deverloperPgContainer" style="background-color:white;padding:0">
                
                <style>

                    #deverloperPgContainer{width: 85%;}
					.msd_slider{max-width:750px; width:100%;}
					.msd_slider_col_box{padding:50px 60px!important;padding-bottom: 40px !important;}
					.msd_slider_col_box img{}
					.msd_slider_colset{}
					.msd_slider_col{width:100%!important;}
                    
                   
                    
                </style>

                
                <div style="display:block; width:100%; height:50px;"></div>
                
                
                
                <div class="mod_location_slider" id="mod_location_slider1">
                    
                    <div class="mod_location_slider_box">
                
                		
                        <div class="mod_location_slider_item active">
                            <div class="thumb">
                                <img src="img/ascott_cgm-vn.png"  alt="location_location_pic1"/>
                                
                                <div id="" class="clsBarInner zoomMap_facilities1" style="display:none;">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc">&nbsp;</div>
                        </div><!-- END mod_location_slider_item -->
                        
                        
                        
                        
                
                
                		</div><!-- END mod_location_slider_box-->
                        
                <!-- Controls -->
                      <div class="controlx" style="display:none;">
                      	<div class="controlx_box">
                          <div class="controlx_btn"></div>
                          <div class="controlx_btn"></div>
                          <div class="clear"></div>
                        </div>
                      </div>
                        
              	    </div><!-- END mod_location_slider-->
                
                
                
                
                
                
              <div id="secDeveloperPgPara" class="clsPurple2">
                  <p>Quý khách hàng thân mến,</p>
                  <p>
                   Đại diện cho The Ascott Limited, chúng tôi rất đỗi tự hào khi được chỉ định trở thành đơn vị quản lý cho D1MENSION - dự án danh giá của CapitaLand, đồng thời cũng là đơn vị trực tiếp vận hành Somerset D1Mension Hồ Chí Minh tại tháp căn hộ dịch vụ nằm trong tổng thể dự án.
                  
                  </p>
                  <p>
                    Lần đầu tiên tại Việt Nam, The Ascott Limited sẽ cung cấp dịch vụ quản lý cùng các dịch vụ vận hành đẳng cấp cho một dự án căn hộ dân cư. Dù đã từng đồng hành cùng CapitaLand trong một số dự án khác như Somerset Vista - TP. 
                      Hồ Chí Minh, D1MENSION sẽ là cột mốc mới khi chúng tôi lần đầu tiên tham gia toàn diện từ quản lí vận hành dự án cho tới cung cấp các dịch vụ khách hàng đẳng cấp nhất cho cư dân toàn dự án.
 
                  
                  </p>
                 
                  
                  <p>
                 Tại D1MENSION, cư dân của cả hai tòa tháp sẽ được tận hưởng những dịch vụ chăm sóc đẳng cấp nhất được thiết kế riêng phù hợp với từng phong cách sống cá nhân, hứa hẹn mang đến một trải nghiệm sống hoàn toàn khác biệt: 
                      Từ dịch vụ thư tín, đặt xe, đặt chỗ nhà hàng, đặt tạp chí và hàng loạt những dịch vụ mang tính nghiệp vụ khác như bảo trì và dọn phòng. Những công việc này được tính toán kỹ lưỡng, mang tính cá nhân hóa cao để chắc chắn khách hàng luôn cảm thấy dễ chịu, 
                      hài lòng tại không gian sang trọng của dự án.

                  </p>
                  
                 
                  <p>
                      Với chuẩn mực đỉnh cao trong các tổ hợp tiện ích, cho tới sự nồng hậu nhiệt thành của đội ngũ nhân viên, chúng tôi đã sẵn sàng để chào đón bạn tới với cuộc sống hoàn hảo tại D1MENSION.
                  </p>
                  
                  
                 
                  
                  <p>Trân trọng,</p>
                  
                  <img id="imgCeoSign" src="img/ascott_crm_Sig.jpg" alt="" />
                  
                  <p>
                    <strong>Lew Yen Ping</strong><br />
                    Giám đốc điều hành<br />
                    Ascott Vietnam<br />
                  </p>
                </div>              
                
                
                
                
                
            </div>
          
            
            
        </section>
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                             Chúng tôi đã đặc biệt thận trọng trong việc chuẩn bị nội dung tài liệu này cũng như các bản mẫu. Tuy nhiên vui lòng lưu ý rằng bản thân tập đoàn, cũng như các đại lý bán hàng, sẽ không chịu trách nhiệm cho những thông tin chưa chính xác trong tài liệu này. Tuy bản thân chúng tôi thực sự tin tưởng rằng những nội dung dưới đây là hoàn toàn chính xác, nhưng tài liệu này sẽ không mang tính đại diện hay trở thành một tuyên ngôn riêng. Tất cả thông tin, những thông số đều có thể sẽ được thay đổi bởi chủ đầu tư hoặc bởi các nhà chức trách có thẩm quyền, bởi vậy mà đây không thể trở thành một phần của các hợp đồng hay đề nghị. Các bản vẽ, thiết kế hay hình ảnh đồ họa sẽ không đại diện chính xác cho điều kiện xây dựng thực tế. Các khu vực sàn được đo đạc kĩ lưỡng, có căn cứ từ các bản báo cáo. Trong khi đó, các hình ảnh đồ họa có thể không phản ánh hoàn toàn điều kiện thực tế.
                            
                        
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
        
        
        <!-- MSD JS INCLUDES -->
        <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>
        <script src="msd_assets/js/msd_functions.js"></script>
        <script src="msd_assets/js/msd_functions_location.js"></script>

        <script>
        
$(document).ready(function(){

$('.msd_slider_colset').css({'width':$('.controlx').width()+'px'});

	$(document).scroll(function(){
		$('.msd_slider_colset').css({'width':$('.controlx').width()+'px'});
	});
	$(window).resize(function() {
		$('.msd_slider_colset').css({'width':$('.controlx').width()+'px'});
	});

});
            
        </script>
        
        

  
	</body>
</html>
