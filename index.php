<?php
    // var_dump($_REQUEST);
    // var_dump($_SERVER['REDIRECT_URL']);
    // echo '<pre>';
    // var_dump($_SERVER['REQUEST_URI']);
    // echo '<pre>';
    // var_dump($_SERVER['SCRIPT_NAME']);
    /* 
        SCRIPT_FILENAME == C:/xampp/htdocs/capitaland/CODE/index.php 
        REDIRECT_URL == /Capitaland/CODE/abc/xyz 
        REQUEST_METHOD == GET 
        QUERY_STRING == hd=33443&da=3322 
        REQUEST_URI == /Capitaland/CODE/abc/xyz?hd=33443&da=3322 
        SCRIPT_NAME == /capitaland/CODE/index.php 
        PHP_SELF == /capitaland/CODE/index.php 
        REQUEST_TIME_FLOAT == 1544669764.362 
        REQUEST_TIME == 1544669764 
    */
    // exit;
    $request_method = $_SERVER ['REQUEST_METHOD']; // ==> GET
    $request_uri  = $_SERVER ['REQUEST_URI']; // ==> /Capitaland/CODE/abc/xyz?hd=33443&da=3322
    $query_string = $_SERVER ['QUERY_STRING']; // ==> hd=33443&da=3322 
    $script_name  = $_SERVER ['SCRIPT_NAME']; // ==> /capitaland/CODE/index.php 
    $root_path    = str_replace("index.php" ,"", $script_name); // ==> /capitaland/CODE/
    $root_path    = substr($request_uri, 0, strlen($root_path)); // ==> /Capitaland/CODE/
    $request_path = substr($request_uri, strlen($root_path));
    // $request_path = str_replace($root_path, "", $request_uri); // ==> abc/xyz?hd=33443&da=3322
    $request_path = str_replace('?' . $query_string, "", $request_path); // ==> abc/xyz?hd=33443&da=3322
    
    // echo $root_path;
    // echo '<pre>';
    // echo $request_path;
    // echo '<pre>';
    $protocol = (isset($_SERVER ['HTTPS']) && $_SERVER ['HTTPS'] == 'on') ? 'htttps://' : 'http://';
    define('PATH_URL', $protocol . $_SERVER ['HTTP_HOST'] . $root_path);
    define('ROOT_PATH', $root_path);
    define('SCRIPT_NAME', $request_method);
    define('BASEFOLDER', '');
    define('EN_LANG', 'en');
    define('VI_LANG', 'vi');
    define('LIST_MENU', 'lt');

    session_start();
    // $lang = $this->uri->rsegment(3);
    //$_SESSION['current_lang'] = 'vn';
     $_SESSION['current_lang'] = isset($_SESSION['current_lang']) ? $_SESSION['current_lang'] : 'en';
    // $_SESSION['listmenu'] = isset($_SESSION['listmenu']) ? $_SESSION['listmenu'] : 'en';
    // echo 'index.php -- ';
      //var_dump($_SESSION['current_lang']);
    // exit();
    include_once(__DIR__ . '/views/template.php');
    include_once(__DIR__ . '/router.php');
    
?>