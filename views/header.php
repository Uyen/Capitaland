<!DOCTYPE html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>         
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>         
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->

<?php 
$current_lang = $_SESSION ['current_lang']; 
require_once(__DIR__ . '/menu.php');
//var_dump($current_lang);
if (empty($_GET['lag'])) {
   echo' <script>
      window.location.replace("' . PATH_URL . 'gioi-thieu?lag=vi' . '");
    </script>';
	exit;
}
//$listmenu = $_SESSION ['listmenu']; 
  //print_r($current_lang);
// $uri = $_SERVER['REQUEST_URI'];
//  $keys = parse_url($uri); // parse the url
//  $path = explode("/", $keys['path']); // splitting the path
//  $last = end($path); // get the value of the last element 
// $current_lang_last = $_SESSION ['current_lang_last'] = $last;
?> 
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="F8" />
        <!-- Bootstrap --> 

        <!-- <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'"> -->
        <script src="<?=get_resource_url('js/jquery-3.1.1.min.js')?>"></script>
        <script type='text/javascript' src='//c.trazk.com/c.js' async='async' ></script>
		<script src="<?=get_resource_url('js/modernizr.custom.js')?>"></script>
        <link href="<?=get_resource_url('css/bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/jquery.fancybox.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/flickity.css')?>" rel="stylesheet" >
        <link href="<?=get_resource_url('css/animate.css')?>" rel="stylesheet">
        
        <link href="<?=get_resource_url('css/font-awesome.min.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/styles.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/innerpages.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/responsive.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/regform.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/custom.css')?>" rel="stylesheet">
        <link href="<?=get_resource_url('css/main.css')?>" rel="stylesheet">
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="<?=get_resource_url('assets/images/favicon.ico')?>">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">
        <link rel="icon" type="image/png" href="favicon.ico" />
        <link href="<?=get_resource_url('msd_assets/css/msd_styles.css')?>" rel="stylesheet">
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="<?=get_resource_url('msd_assets/plugins/msd_slider/css/style.css')?>">
        <!-- Photo-facilities -->
        <!-- <link rel="stylesheet" href="<?=get_resource_url('css/gallery_new.css')?>"> -->
        <!-- link js -->
		
		
        <!-- <script src="<?=get_resource_url('js/min/toucheffects-min.js')?>"></script>
        <script src="<?=get_resource_url('js/flickity.pkgd.min.js')?>"></script>
         <script src="<?=get_resource_url('js/jquery.fancybox.pack.js')?>"></script>
         <script src="<?=get_resource_url('js/retina.js')?>"></script>
         <script src="<?=get_resource_url('js/waypoints.min.js')?>"></script>
         <script src="<?=get_resource_url('js/bootstrap.min.js')?>"></script>
         <script src="<?=get_resource_url('js/parallax.js')?>"></script>
         <script src="<?=get_resource_url('js/wow.min.js')?>"></script>
         <script src="<?=get_resource_url('js/scripts.js')?>"></script>
         <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/jquery.transit.min.js')?>"></script>
         <script src="<?=get_resource_url('msd_assets/js/msd_functions.js')?>"></script>
         <script src="<?=get_resource_url('msd_assets/js/msd_functions_location.js')?>"></script>
         <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/msdSlider.js')?>"></script>
         <script src="<?=get_resource_url('js/js_gallery/lightgallery.js')?>"></script>
         <script src="<?=get_resource_url('js/js_gallery/lightgallery.min.js')?>"></script> -->
		
         
         <style>
            #main-navbar-cover{z-index: 200;}
            .intro-slider a{color: #a17b8e;text-decoration: underline;}
            .nav{font-family: 'TeXGyreTermes'}
        </style>
         
         <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
         <!-- <link rel="stylesheet" href="<?=get_resource_url('css/main.css')?>"> -->
        <!--  -->
        <link href="<?=get_resource_url('msd_assets/css/msd_styles_location.css')?>" rel="stylesheet">
        <?php
        if($view == 'gallery-en' || $view == 'gallery-vn'){
        ?>
         <style>
        {max-width:inherit!important;}
        .mod_location_slider .mod_location_slider_item{padding:0!important;}
        .clsBarInner,
        .mod_location_slider .mod_location_slider_item .thumb img,
        .mod_location_slider .controlx .controlx_box{max-width:inherit!important;}
        .mod_location_slider .controlx .controlx_btn:nth-child(1):before,
        .mod_location_slider .controlx .controlx_btn:nth-child(2):before{color:#fff!important;padding-left: 50px;padding-right: 50px;}
         #mod_location_slider1 .controlx{margin-top:25%;}
                    
                    #mod_location_slider1{max-width:700px; width:100%; margin:0 auto!important;}
                  
                    @media only screen and (max-width: 3000px){   #mod_location_slider1 .controlx{margin-top:7%;}  }
                    @media only screen and (max-width: 2000px){   #mod_location_slider1 .controlx{margin-top:10%;}  }
                    @media only screen and (max-width: 1600px){   #mod_location_slider1 .controlx{margin-top:15%;}  } 
        </style>
        <?php 
            }
        ?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-MDVCFTH');
        </script>
        <!-- End Google Tag Manager -->
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-P959B4G');
        </script>
        <!-- End Google Tag Manager -->
        <style type="text/css">
            #navbarNavDropdown li.nav-item.open {
                display: inline-block !important;
            }
        </style>
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P959B4G"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src=""https://www.googletagmanager.com/ns.html?id=GTM-MDVCFTH""
            height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!--[if lt IE 7]>
        <p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
        <![endif]-->
        <!-- open/close -->
        <header>
            <div class="main-navbar " >
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <div>
                            <a class="navbar-brand" href="<?=PATH_URL ?>"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        </div>
                        <div class="main-menu" id="navbarNavDropdown">
                            <ul>
                             <?php 
                             $lag = $_GET['lag'] ;
                             // if(! $_GET['lag']){
                             //  $lag = $_GET['lag'] ;
                             //    }else{
                             //        $lag = EN_LANG;
                             //    }
                                // && is_array($menus ['en'])
                                if ( ! empty($menus [$current_lang]) )  {
                                     $counter = 0;
                                   //  print_r($menus [$current_lang]);
                                    foreach ($menus [$current_lang] as $key => $menu_item) {
                                        $parent_data = $menu_item['data'];
                                         $parent_childs = $menu_item['childs']; 
                                          $parent_has_childs = ( ! empty($parent_childs) && is_array($parent_childs)) ? true : false;
                                        // print_r($current_lang_last);
                                         $parent_name = '';
                                         $parent_url = '';
                                          if($lag == EN_LANG){
                                            $parent_name = $parent_data [EN_LANG]['name'];
                                            $parent_url = $parent_data [EN_LANG]['url'];
                                          //  print_r($name);

                                            //chinh lại router
                                            //loi menu prômtion , facilities
                                            ?>
                                                <li class="nav-item <?=($parent_has_childs) ? 'dropdown' : ''?>">
                                                        <a href="<?=$parent_url  . '?lag=' . $_GET['lag']?>" 
                                                         
                                                         class="nav-link <?=($parent_has_childs) ? 'dropdown-toggle' : ''?>" 
                                                         data-toggle="<?=($parent_has_childs) ? 'dropdown' : ''?>" aria-haspopup="true" 
                                                         aria-expanded="false">
                                                            <?=$parent_name?>
                                                        </a>
                                                       
                                                        
                                                    </li>  
                                            <?php
                                        }else{
                                             $parent_name = $parent_data [VI_LANG]['name'];
                                            $parent_url = $parent_data [VI_LANG]['url'];
                                             ?>
                                              <li class="nav-item <?=($parent_has_childs) ? 'dropdown' : ''?>">
                                                        <a href="<?=$parent_url . '?lag=' . $_GET['lag']?>" 
                                                         
                                                         class="nav-link <?=($parent_has_childs) ? 'dropdown-toggle' : ''?>" 
                                                         data-toggle="<?=($parent_has_childs) ? 'dropdown' : ''?>" aria-haspopup="true" 
                                                         aria-expanded="false">
                                                            <?=$parent_name?>
                                                        </a>
                                                       
                                                        
                                                    </li>  
                                             <?php
                                        }

                                      }//end foreach
                                }// end
                            ?> 
                            <!-- new -->
                            
                      
                            </ul>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">
                            <ul class="nav nav-pills nav-stacked">
                              <?php 
                             $lag = $_GET['lag'] ;
                             // if(! $_GET['lag']){
                             //  $lag = $_GET['lag'] ;
                             //    }else{
                             //        $lag = EN_LANG;
                             //    }
                                // && is_array($menus ['en'])
                                if ( ! empty($menus [$current_lang]) )  {
                                     $counter = 0;
                                   //  print_r($menus [$current_lang]);
                                    foreach ($menus [$current_lang] as $key => $menu_item) {
                                        $parent_data = $menu_item['data'];
                                         $parent_childs = $menu_item['childs']; 
                                          $parent_has_childs = ( ! empty($parent_childs) && is_array($parent_childs)) ? true : false;
                                        // print_r($current_lang_last);
                                         $parent_name = '';
                                         $parent_url = '';
                                          if($lag == EN_LANG){
                                            $parent_name = $parent_data [EN_LANG]['name'];
                                            $parent_url = $parent_data [EN_LANG]['url'];
                                          //  print_r($name);

                                            //chinh lại router
                                            //loi menu prômtion , facilities
                                            ?>
                                                <li class="nav-item <?=($parent_has_childs) ? 'dropdown' : ''?>">
                                                        <a href="<?=$parent_url  . '?lag=' . $_GET['lag']?>" 
                                                         
                                                         class="nav-link <?=($parent_has_childs) ? 'dropdown-toggle' : ''?>" 
                                                         data-toggle="<?=($parent_has_childs) ? 'dropdown' : ''?>" aria-haspopup="true" 
                                                         aria-expanded="false">
                                                            <?=$parent_name?>
                                                        </a>
                                                       
                                                        
                                                    </li>  
                                            <?php
                                        }else{
                                             $parent_name = $parent_data [VI_LANG]['name'];
                                            $parent_url = $parent_data [VI_LANG]['url'];
                                             ?>
                                              <li class="nav-item <?=($parent_has_childs) ? 'dropdown' : ''?>">
                                                        <a href="<?=$parent_url . '?lag=' . $_GET['lag']?>" 
                                                         
                                                         class="nav-link <?=($parent_has_childs) ? 'dropdown-toggle' : ''?>" 
                                                         data-toggle="<?=($parent_has_childs) ? 'dropdown' : ''?>" aria-haspopup="true" 
                                                         aria-expanded="false">
                                                            <?=$parent_name?>
                                                        </a>
                                                       
                                                        
                                                    </li>  
                                             <?php
                                        }

                                      }//end foreach
                                }// end
                            ?> 
                            <!-- new -->
                            
                               <!--  <li class="active"><a href="<?=get_url_form_define('abc')?>" data-toggle="elementscroll">Design concept/ inspiration</a></li>
                                <li><a href="location-vn.php" data-toggle="elementscroll">Location</a></li>
                                <li><a href="residences-vn.php" data-toggle="elementscroll">Unit layout</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Media</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Developer/ Contact</a></li>
                                <li id="mnuReg" class="clsGoldbg" data-toggle="elementscroll"><a href="register-vn.php">Register</a></li> -->
                            </ul>
                        </div>
                    </div>
                    
                    <div id="divLangToggle"> 
                      <a href="<?=PATH_URL . 'gioi-thieu?lag=' . VI_LANG ?>">VN</a> / 
                      <a href="<?=PATH_URL . 'introduction?lag=' . EN_LANG ?>"> EN</a> </div>
                </nav>
            </div>
            <div id="main-navbar-cover"></div>
            <!-- <div class="texture-overlay"></div>  -->
            <style>
                .nav{font-family: 'TeXGyreTermes'}
                .t-h1{color: red;}
            </style>
        </header>
		<!--
            <script>
             lightGallery(document.getElementById('show-suites-gallery-2br'));
             lightGallery(document.getElementById('show-suites-gallery-3br'));
             lightGallery(document.getElementById('sales-gallery'));
             lightGallery(document.getElementById('perspectives-gallery-interior'));
             lightGallery(document.getElementById('perspectives-gallery-landscape'));
            </script>
		-->

        