<?php 


// $menu_test = array(
//     // Design concept - menu  
//     array(
//         'data' => array(
//             EN_LANG => array(
//                 'name' => 'Design concept',
//                 'url' => '',
//                 'has_childs' => 1
//             ),
//             VI_LANG => array(
//                 'name' => 'Ý Tưởng',
//                 'url' => '',
//                 'has_childs' => 1
//             ),
//         ), //end parent property
//         'childs' => array(
//         	// Introduction (project concept/ Somerset services)
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => '',
//                         'url' => '#',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Ý Tưởng',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Photo mock up unit
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Photo mock up unit EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Photo mock up unit VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Photo facilities
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Photo facilities EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Photo facilities VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//           
//         ),//end childs property
//     ),//end parent item
//     // Location - menu
//     array(
//         'data' => array(
//             EN_LANG => array(
//                 'name' => 'Location EN',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//             VI_LANG => array(
//                 'name' => 'Location VI',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//         ),
//         'childs' => array(
//     	),
//     ),
//     // Unit layout - menu
//     array(
//         'data' => array(
//             EN_LANG => array(
//                 'name' => 'Unit layout EN',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//             VI_LANG => array(
//                 'name' => 'Unit layout VI',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//         ),
//         'childs' => array(
//             // Facilities Plan
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Facilities Plan EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Facilities Plan VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Facilities Plan
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Facilities Plan EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Facilities Plan VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Unit plan
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Unit plan EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Unit plan VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//     	),
//     ),
//     // Media - menu
//     array(
//         'data' => array(
//             EN_LANG => array(
//                 'name' => 'Media EN',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//             VI_LANG => array(
//                 'name' => 'Media VI',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//         ),
//         'childs' => array(
//             // News
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'News EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'News VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // E-brochure
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'E-brochure EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'E-brochure VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Project update
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Project update EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Project update VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Video events
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Video events EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Video events VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Video lifestyle/location/teaser
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Video lifestyle/location/teaser EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Video lifestyle/location/teaser VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//     	),
//     ),
//     // Promotion - menu
//     array(
//         'data' => array(
//             EN_LANG => array(
//                 'name' => 'Promotion EN',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//             VI_LANG => array(
//                 'name' => 'Promotion VI',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//         ),
//         'childs' => array(
//             // New sales program/promotion
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'New sales program/promotion EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'New sales program/promotion VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//     	),
//     ),
//     // Developer/ contact - menu
//     array(
//         'data' => array(
//             EN_LANG => array(
//                 'name' => 'Developer/ contact EN',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//             VI_LANG => array(
//                 'name' => 'Developer/ contact VI',
//                 'url' => '',
//                 'has_childs' => 0
//             ),
//         ),
//         'childs' => array(
//             // Developers CL/ The As
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Developers CL/ The As EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Developers CL/ The As VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Introduction
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Introduction EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Introduction VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Letter CEO/ The Ascott
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Letter CEO/ The Ascott EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Letter CEO/ The Ascott VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//             // Registration
//             array(
//                 'data' => array(
//                     EN_LANG => array(
//                         'name' => 'Registration EN',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                     VI_LANG => array(
//                         'name' => 'Registration VI',
//                         'url' => '',
//                         'has_childs' => 0
//                     ),
//                 ),
//                 'childs' => array(),
//             ),
//     	),
//     ),
// );


 ?>