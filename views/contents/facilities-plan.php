   <div id="main-navbar-cover"></div>
            
            <style>
            
             #divMenu{position: absolute;top:20px;z-index: 1;width:100%;padding-right:50px;font-family: 'Asap-Regular' !important;font-weight: normal;font-style: normal;}
             #divMenuInner{position: relative;width:200px;z-index: 100;}
            
                #divMenu ul{font-family: 'Asap-Regular' !important;font-weight: normal;font-style: normal;}
                
             #divTop2Logo{position: relative;padding-left: 40%;top:25px;}
             #logos{height: 90px;width: auto;padding-right: 20px;}
             #divTop2LogoMobile{position: relative;}
             #logosMobile{height: 80px;width:auto;}
                #imgHomeSec4Right-mobile{padding-top: 300px;}  
                
                
             </style>
            <div class="wrapper floorplans">  <!-- Main Logo -->
              <nav class="nav-logo">
                <a href="../en">
                  <!-- <img class="logo p-4" src="images/new/logo.svg" alt="De La Sol Logo"> -->
                </a>
              </nav>
            </div>
  <!-- Main Navigation -->
  <!-- <div class="nav-overlay"></div> -->
  <!-- <div class="menuBtn"><i></i></div> -->
<!--   <nav class="nav-wrapper">
    <div class="mainMenu d-flex align-items-center" initW="">
      <ul class="pl-5">
        <li><a href="./">home</a></li>
        <li><a href="location.html">location</a></li>
        <li><a href="facilities.html">facilities</a></li>
        <li class="dropdown"><a class="dropdown-toggle">floor plans</a>
          <ul>
            <li><a href="alto-floor-plans.html">Alto</a></li>
            <li><a href="cello-floor-plans.html">Cello</a></li>
          </ul>
        </li>
        <li><a href="gallery.html">gallery</a></li>
        <li><a href="media.html">media</a></li>
        <li><a href="contact.html">contact</a></li>
        <li><a href="https://www.capitaland.com/vn/en/contact-us.html" target="_blank">registration</a></li>
        <li><a href="promotions.html">promotions</a></li>
      </ul>
      <a class="tnc pl-5" href="#" data-toggle="modal" data-target="#modalTerms">Terms &amp; Conditions</a>
    </div>
  </nav> -->
  
  <div class="bg-fixed-m"></div>
<section class="section section-auto floorplans" id="floorplans" data-section-name="floorplans">
   <!--  <div class="elements">
        <div class="element element-1"><img class="img-fluid" src="images/new/element-01.png"></div>
        <div class="element element-2"><img class="img-fluid" src="images/new/element-02.png"></div>
        <div class="element element-3"><img class="img-fluid" src="images/new/element-03.png"></div>
    </div> -->
    <article class="d-flex flex-wrap align-items-center">
      <div class="col-6 col-lg-2 order-1 order-md-1 pl-5 align-self-start">
        <div class="article-inner col-12 text-left">
          <img class="img-fluid" src="images/new/logo-tower_A.png">
          <h4 class="headline">
            <span class="ml-0">
              <small class="mt-2">FLOOR PLANS</small>
            </span>
          </h4>
          <div class="plans-nav">
            <ul>
              <li data-nav="plans-lv1" class="plans-lv1 active"><a>Level 2 to level 11</a></li>
              <li data-nav="plans-lv2-13" class="plans-lv2-13"><a>Level 12</a></li>
            <!--  <li data-nav="plans-lv14" class="plans-lv14"><a>Level 12</a></li> -->
              <li data-nav="plans-lv15" class="plans-lv15"><a>Level 12A to level 12B</a></li>
              <li data-nav="plans-lv16" class="plans-lv16"><a>Level 15</a></li>
              <li data-nav="plans-lv17-22" class="plans-lv17-22"><a>Level 16</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-8 order-3 order-md-2 pt-3 pt-md-0 align-items-center justify-content-center">
        <div class="plans-floorplans">
          <!-- Level 1 -->
          <div class="plans-lv1 plans-level active">
            <!-- <img class="img-fluid" src="images/new/lv1.png" usemap="#floor1" alt="floor1"> -->
             <img class="img-fluid" src="images/room/3BR.jpg" usemap="#floor1" alt="floor1">
          </div>
          <map name="floor1" id="floor1">
            <area alt="lvl1-A3-04" id="lvl1-A3-04" data-tooltip="Unit No: A3-04<br>Unit Type: 2BR D" href="#modal2BR-D" coords="1370,910,1685,660" shape="rect">
            <area alt="lvl1-A3-05" id="lvl1-A3-05" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D Mirror" href="#modal2BR-D-Mirror" coords="2526,910,2205,660" shape="rect">
            <area alt="lvl1-A3-03" id="lvl1-A3-03" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D Mirror" href="#modal2BR-E1" coords="2526,1380,1905,1145" shape="rect">
             <area alt="lvl1-A3-06" id="lvl1-A3-06" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D Mirror" href="#modal1BR-A" coords="1676,1380,1108,1145" shape="rect">
             <area alt="lvl1-A3-07" id="lvl1-A3-07" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D Mirror" href="#modal2BR-E2-Mirror" coords="1085,1715,768,1480" shape="rect">
              <area alt="lvl1-A3-08" id="lvl1-A3-08" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D Mirror" href="#modal2BR-E2" coords="1480,1680,1328,1400" shape="rect">

               <area alt="lvl1-A3-09" id="lvl1-A3-09" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D Mirror" href="#modal3BR-A" coords="2075,1715,1948,1595" shape="rect">
            <!-- <area alt="lvl1-A3-03" id="lvl1-A3-03" data-tooltip="Unit No: A3-03<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1184,151,1291,150,1291,270,1222,270,1222,254,1163,256,1163,170,1184,167" shape="poly"> -->
            <!-- <area alt="lvl1-A3-06" id="lvl1-A3-06" data-tooltip="Unit No: A3-06<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1314,147,1384,147,1386,163,1441,161,1441,252,1423,250,1423,271,1312,273" shape="poly"> -->
            <!-- <area alt="lvl1-A3-02" id="lvl1-A3-02" data-tooltip="Unit No: A3-02<br>Unit Type: 1BR A" href="#modal1BR-A" coords="1164,270,1290,271,1291,379,1266,379,1267,362,1161,363" shape="poly"> -->
            <!-- <area alt="lvl1-A3-07" id="lvl1-A3-07" data-tooltip="Unit No: A3-07<br>Unit Type: 2BR E2 Mirror" href="#modal2BR-E2-Mirror" coords="1312,274,1423,274,1423,293,1441,295,1442,382,1384,383,1384,399,1312,397" shape="poly"> -->
            <!-- <area alt="lvl1-A3-08" id="lvl1-A3-08" data-tooltip="Unit No: A3-08<br>Unit Type: 2BR E2" href="#modal2BR-E2" coords="1314,399,1384,400,1383,418,1442,418,1441,505,1421,505,1423,523,1312,524" shape="poly"> -->
            <!-- <area alt="lvl1-A3-09" id="lvl1-A3-09" data-tooltip="Unit No: A3-09<br>Unit Type: 3BR A" href="#modal3BR-A" coords="1312,524,1442,670" shape="rect"> -->
            <!-- chua -->
            <!-- <area alt="lvl1-A3-01" id="lvl1-A3-01" data-tooltip="Unit No: A3-01<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1153,541,1281,543,1280,611,1264,612,1261,670,1174,669,1174,647,1153,649" shape="poly"> -->
          </map>
          <!-- Level 2-13 -->
          <div class="plans-lv2-13 plans-level">
            <img class="img-fluid" src="images/new/lv2-13.png" usemap="#floor2_13" alt="floor2_13">
          </div>
          <map name="floor2_13" id="floor2_13">
            <area alt="lvl2_13-A1-03" id="lvl2_13-A1-03" data-tooltip="Unit No: A1-03<br>Unit Type: 3BR B Duo Mirror"  href="#modal3BR-B-Duo-Mirror" coords="4,377,132,379,132,427,155,429,155,519,4,518" shape="poly">
            <area alt="lvl2_13-A1-04" id="lvl2_13-A1-04" data-tooltip="Unit No: A1-04<br>Unit Type: 2BR A"  href="#modal2BR-A" coords="156,449,176,450,175,392,300,392,298,485,223,485,224,519,155,519" shape="poly">
            <area alt="lvl2_13-A1-05" id="lvl2_13-A1-05" data-tooltip="Unit No: A1-05<br>Unit Type: 2BR A Mirror"  href="#modal2BR-A-Mirror" coords="313,393,439,393,439,449,457,449,459,518,392,520,391,483,316,485" shape="poly">
            <area alt="lvl2_13-A1-06" id="lvl2_13-A1-06" data-tooltip="Unit No: A1-06<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="459,414,481,414,481,393,572,392,572,451,589,451,590,520,460,519" shape="poly">
            <area alt="lvl2_13-A2-03" id="lvl2_13-A2-03" data-tooltip="Unit No: A2-03<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="590,451,609,451,607,392,698,393,698,413,717,411,719,519,590,519" shape="poly">
            <area alt="lvl2_13-A2-04" id="lvl2_13-A2-04" data-tooltip="Unit No: A2-04<br>Unit Type: 2BR A"  href="#modal2BR-A" coords="719,450,741,448,740,393,863,392,864,485,787,483,787,520,719,520" shape="poly">
            <area alt="lvl2_13-A2-05" id="lvl2_13-A2-05" data-tooltip="Unit No: A2-05<br>Unit Type: 2BR A Mirror"  href="#modal2BR-A-Mirror" coords="879,392,1003,392,1002,449,1021,449,1023,520,953,520,955,484,879,484" shape="poly">
            <area alt="lvl2_13-A3-03" id="lvl2_13-A3-03" data-tooltip="Unit No: A3-03<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1023,450,1041,450,1041,391,1130,391,1130,414,1149,411,1150,521,1024,521" shape="poly">
            <area alt="lvl2_13-A1-02" id="lvl2_13-A1-02" data-tooltip="Unit No: A1-02<br>Unit Type: 3BR B Duo"  href="#modal3BR-B-Duo" coords="4,541,153,541,153,635,131,635,131,681,4,681" shape="poly">
            <area alt="lvl2_13-A1-01" id="lvl2_13-A1-01" data-tooltip="Unit No: A1-01<br>Unit Type: 2BR C"  href="#modal2BR-C" coords="153,543,221,543,223,581,289,581,289,588,305,591,308,671,176,670,176,633,153,633" shape="poly">
            <area alt="lvl2_13-A1-08" id="lvl2_13-A1-08" data-tooltip="Unit No: A1-08<br>Unit Type: 2BR C Mirror"  href="#modal2BR-C-Mirror" coords="308,590,327,588,326,584,392,584,392,542,459,543,459,635,438,635,438,672,309,670" shape="poly">
            <area alt="lvl2_13-A1-07" id="lvl2_13-A1-07" data-tooltip="Unit No: A1-07<br>Unit Type: 2BR F Duo Mirror"  href="#modal2BR-F-Duo-Mirror" coords="460,541,508,541,505,521,592,521,589,615,573,616,572,670,481,670,483,649,460,650" shape="poly">
            <area alt="lvl2_13-A2-02" id="lvl2_13-A2-02" data-tooltip="Unit No: A2-02<br>Unit Type: 3BR D Triple"  href="#modal3BR-D-triple" coords="593,522,672,522,674,542,717,542,717,668,628,671,628,637,590,638" shape="poly">
            <area alt="lvl2_13-A2-01" id="lvl2_13-A2-01" data-tooltip="Unit No: A2-01<br>Unit Type: 2BR C"  href="#modal2BR-C" coords="719,541,784,541,787,584,853,578,853,591,870,591,870,670,741,670,743,633,717,633" shape="poly">
            <area alt="lvl2_13-A2-06" id="lvl2_13-A2-06" data-tooltip="Unit No: A2-06<br>Unit Type: 2BR B"  href="#modal2BR-B" coords="870,591,888,591,890,582,956,584,955,543,997,541,996,521,1024,523,1023,616,1002,616,1000,671,872,670" shape="poly">
            <area alt="lvl2_13-A3-02" id="lvl2_13-A3-02" data-tooltip="Unit No: A3-02<br>Unit Type: 2BR F Duo"  href="#modal2BR-F-Duo" coords="1023,522,1106,522,1106,539,1147,539,1151,649,1132,649,1129,668,1040,669,1041,613,1024,614" shape="poly">
            <area alt="lvl2_13-A3-01" id="lvl2_13-A3-01" data-tooltip="Unit No: A3-01<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1150,540,1279,541,1279,612,1263,613,1263,670,1174,670,1174,649,1154,649" shape="poly">
           <!--  <area alt="lvl2_13-A3-06" id="lvl2_13-A3-06" data-tooltip="Unit No: A3-06<br>Unit Type: 2BR D"  href="#modal2BR-D" coords="1061,30,1291,148" shape="rect"> -->
            <area alt="lvl2_13-A3-05" id="lvl2_13-A3-05" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1163,170,1184,170,1182,151,1291,148,1291,273,1221,273,1221,256,1161,257" shape="poly">
            <area alt="lvl2_13-A3-04" id="lvl2_13-A3-04" data-tooltip="Unit No: A3-04<br>Unit Type: 2BR A"  href="#modal1BR-A" coords="1160,271,1291,271,1291,379,1264,379,1264,364,1163,365" shape="poly">
            <area alt="lvl2_13-A3-07" id="lvl2_13-A3-07" data-tooltip="Unit No: A3-07<br>Unit Type: 2BR D Mirror"  href="#modal2BR-D-Mirror" coords="1311,30,1443,146" shape="rect">
            <area alt="lvl2_13-A3-08" id="lvl2_13-A3-08" data-tooltip="Unit No: A3-08<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1313,149,1387,146,1385,162,1441,165,1443,251,1421,251,1421,272,1311,271" shape="poly">
            <area alt="lvl2_13-A3-09" id="lvl2_13-A3-09" data-tooltip="Unit No: A3-09<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="1313,274,1423,272,1421,295,1440,296,1443,380,1383,383,1385,401,1311,401" shape="poly">
            <area alt="lvl2_13-A3-10" id="lvl2_13-A3-10" data-tooltip="Unit No: A3-10<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1311,402,1385,403,1385,419,1441,417,1443,504,1421,506,1421,526,1311,526" shape="poly">
            <area alt="lvl2_13-A3-11" id="lvl2_13-A3-11" data-tooltip="Unit No: A3-11<br>Unit Type: 3BR A"  href="#modal3BR-A" coords="1313,524,1443,671" shape="rect">
          </map>
          <!-- Level 14 -->
        <!--   <div class="plans-lv14 plans-level">
            <img class="img-fluid" src="images/new/lv14.png" usemap="#floor14" alt="floor14">
          </div>
          <map name="floor14" id="floor14">
            <area alt="lvl14-A1-03" id="lvl14-A1-03" data-tooltip="Unit No: A1-03<br>Unit Type: 3BR B Duo Mirror" href="#modal3BR-B-Duo-Mirror" coords="4,380,131,380,134,428,155,431,153,521,5,520" shape="poly">
            <area alt="lvl14-A1-04" id="lvl14-A1-04" data-tooltip="Unit No: A1-04<br>Unit Type: 2BR A" href="#modal2BR-A" coords="155,451,176,451,175,391,299,393,299,486,223,486,223,521,156,520" shape="poly">
            <area alt="lvl14-A1-05" id="lvl14-A1-05" data-tooltip="Unit No: A1-05<br>Unit Type: 2BR A Mirror" href="#modal2BR-A-Mirror" coords="313,391,439,391,439,451,460,448,460,518,391,521,392,485,315,486" shape="poly">
            <area alt="lvl14-A1-06" id="lvl14-A1-06" data-tooltip="Unit No: A1-06<br>Unit Type: 2BR E2 Mirror" href="#modal2BR-E2-Mirror" coords="459,414,481,414,481,393,570,393,570,451,586,452,589,521,460,520" shape="poly">
            <area alt="lvl14-A1-02" id="lvl14-A1-02" data-tooltip="Unit No: A1-02<br>Unit Type: 3BR B Duo" href="#modal3BR-B-Duo" coords="4,540,152,540,153,635,135,635,134,680,5,681" shape="poly">
            <area alt="lvl14-A1-01" id="lvl14-A1-01" data-tooltip="Unit No: A1-01<br>Unit Type: 2BR C" href="#modal2BR-C" coords="153,543,223,541,223,581,291,581,293,587,306,586,306,671,175,671,176,635,153,635" shape="poly">
            <area alt="lvl14-A1-08" id="lvl14-A1-08" data-tooltip="Unit No: A1-08<br>Unit Type: 2BR C Mirror" href="#modal2BR-C-Mirror" coords="308,588,327,588,325,581,392,581,391,542,459,543,459,635,439,636,439,670,306,672" shape="poly">
            <area alt="lvl14-A1-07" id="lvl14-A1-07" data-tooltip="Unit No: A1-07<br>Unit Type: 2BR E2 Mirror" href="#modal2BR-E2-Mirror" coords="462,540,587,541,587,613,570,615,570,670,483,670,481,647,460,649" shape="poly">
            <area alt="lvl14-A3-02" id="lvl14-A3-02" data-tooltip="Unit No: A3-02<br>Unit Type: 2BR E2" href="#modal2BR-E2" coords="1021,543,1151,541,1147,650,1130,652,1129,670,1043,670,1041,613,1021,615" shape="poly">
            <area alt="lvl14-A3-01" id="lvl14-A3-01" data-tooltip="Unit No: A3-01<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1151,543,1279,543,1277,613,1263,613,1262,670,1174,669,1175,647,1151,652" shape="poly"> -->
            <!-- <area alt="lvl14-A3-05" id="lvl14-A3-05" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D" href="#modal2BR-D" coords="1161,30,1291,148" shape="rect"> -->
           <!--  <area alt="lvl14-A3-04" id="lvl14-A3-04" data-tooltip="Unit No: A3-04<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1161,170,1182,169,1182,148,1291,148,1291,273,1221,271,1221,256,1161,259" shape="poly">
            <area alt="lvl14-A3-03" id="lvl14-A3-03" data-tooltip="Unit No: A3-03<br>Unit Type: 1BR A" href="#modal1BR-A" coords="1161,273,1291,272,1293,379,1264,381,1264,365,1163,365" shape="poly">
            <area alt="lvl14-A3-06" id="lvl14-A3-06" data-tooltip="Unit No: A3-06<br>Unit Type: 2BR D Mirror" href="#modal2BR-D-Mirror" coords="1311,30,1440,148" shape="rect">
            <area alt="lvl14-A3-07" id="lvl14-A3-07" data-tooltip="Unit No: A3-07<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1313,150,1385,148,1385,162,1440,162,1441,247,1421,253,1423,271,1311,274" shape="poly">
            <area alt="lvl14-A3-08" id="lvl14-A3-08" data-tooltip="Unit No: A3-08<br>Unit Type: 2BR E2 Mirror" href="#modal2BR-E2-Mirror" coords="1313,276,1426,274,1421,294,1441,295,1443,383,1383,384,1383,399,1313,399" shape="poly">
            <area alt="lvl14-A3-09" id="lvl14-A3-09" data-tooltip="Unit No: A3-09<br>Unit Type: 2BR E2" href="#modal2BR-E2" coords="1313,402,1385,402,1383,416,1443,415,1443,504,1421,505,1423,527,1311,525" shape="poly">
            <area alt="lvl14-A3-10" id="lvl14-A3-10" data-tooltip="Unit No: A3-10<br>Unit Type: 3BR A" href="#modal3BR-A" coords="1311,525,1441,672" shape="rect">
          </map> -->
          <!-- Level 15 -->
        <!--   <div class="plans-lv15 plans-level">
            <img class="img-fluid" src="images/new/lv15.png" usemap="#floor15" alt="floor15">
          </div>
          <map name="floor15" id="floor15">
            <area alt="lvl15-A1-03" id="lvl15-A1-03" data-tooltip="Unit No: A1-03<br>Unit Type: 3BR B Duo Mirror" href="#modal3BR-B-Duo-Mirror" coords="4,382,134,383,135,430,153,431,155,520,4,519" shape="poly">
            <area alt="lvl15-A1-04" id="lvl15-A1-04" data-tooltip="Unit No: A1-04<br>Unit Type: 2BR A" href="#modal2BR-A" coords="175,393,299,393,299,485,223,487,223,522,156,522,156,450,175,448" shape="poly">
            <area alt="lvl15-A1-05" id="lvl15-A1-05" data-tooltip="Unit No: A1-05<br>Unit Type: 2BR A Mirror" href="#modal2BR-A-Mirror" coords="315,393,439,392,439,450,459,450,459,519,392,522,392,485,315,485" shape="poly">
            <area alt="lvl15-A1-06" id="lvl15-A1-06" data-tooltip="Unit No: A1-06<br>Unit Type: 2BR E2 Mirror" href="#modal2BR-E2-Mirror" coords="457,414,481,414,480,393,570,393,570,453,587,453,587,520,462,520" shape="poly">
            <area alt="lvl15-A1-02" id="lvl15-A1-02" data-tooltip="Unit No: A1-02<br>Unit Type: 3BR B Duo" href="#modal3BR-B-Duo" coords="4,541,152,543,153,636,132,639,132,684,4,682" shape="poly">
            <area alt="lvl15-A1-01" id="lvl15-A1-01" data-tooltip="Unit No: A1-01<br>Unit Type: 2BR C" href="#modal2BR-C" coords="153,543,223,543,223,582,292,581,291,589,305,589,306,673,175,673,178,636,155,636" shape="poly">
            <area alt="lvl15-A1-08" id="lvl15-A1-08" data-tooltip="Unit No: A1-08<br>Unit Type: 2BR C Mirror" href="#modal2BR-C-Mirror" coords="305,589,327,588,325,584,392,584,392,544,459,543,459,636,436,636,439,673,306,673" shape="poly">
            <area alt="lvl15-A1-07" id="lvl15-A1-07" data-tooltip="Unit No: A1-07<br>Unit Type: 2BR F Duo Mirror" href="#modal2BR-F-Duo-Mirror" coords="460,541,505,541,505,523,589,523,589,615,570,616,572,671,483,673,483,650,460,651" shape="poly">
            <area alt="lvl15-A3-05" id="lvl15-A3-05" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR D" href="#modal2BR-D" coords="1161,31,1291,148" shape="rect">
            <area alt="lvl15-A3-04" id="lvl15-A3-04" data-tooltip="Unit No: A3-04<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1182,152,1290,149,1291,272,1221,273,1219,259,1161,258,1160,169,1182,167" shape="poly">
            <area alt="lvl15-A3-03" id="lvl15-A3-03" data-tooltip="Unit No: A3-03<br>Unit Type: 2BR A" href="#modal1BR-A" coords="1161,273,1291,273,1291,379,1264,379,1264,364,1163,362" shape="poly">
            <area alt="lvl15-A3-06" id="lvl15-A3-06" data-tooltip="Unit No: A3-06<br>Unit Type: 2BR D Mirror" href="#modal2BR-D-Mirror" coords="1311,30,1440,146" shape="rect">
            <area alt="lvl15-A3-07" id="lvl15-A3-07" data-tooltip="Unit No: A3-07<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1313,148,1390,148,1389,162,1443,163,1441,252,1421,254,1423,271,1311,273" shape="poly">
            <area alt="lvl15-A3-08" id="lvl15-A3-08" data-tooltip="Unit No: A3-08<br>Unit Type: 2BR E2 Mirror" href="#modal2BR-E2-Mirror" coords="1311,275,1400,273,1399,295,1443,296,1443,385,1385,386,1385,402,1311,401" shape="poly">
            <area alt="lvl15-A3-09" id="lvl15-A3-09" data-tooltip="Unit No: A3-09<br>Unit Type: 2BR E2" href="#modal2BR-E2" coords="1310,402,1385,402,1385,419,1440,417,1441,506,1421,508,1420,526,1311,526" shape="poly">
            <area alt="lvl15-A3-10" id="lvl15-A3-10" data-tooltip="Unit No: A3-10<br>Unit Type: 2BR A" href="#modal3BR-A" coords="1311,526,1443,673" shape="rect">
            <area alt="lvl15-A3-02" id="lvl15-A3-02" data-tooltip="Unit No: A3-02<br>Unit Type: 2BR F Duo" href="#modal2BR-F-Duo" coords="1021,520,1102,520,1106,542,1150,542,1149,671,1038,671,1041,616,1021,616" shape="poly">
            <area alt="lvl15-A3-01" id="lvl15-A3-01" data-tooltip="Unit No: A3-01<br>Unit Type: 2BR E1" href="#modal2BR-E1" coords="1153,542,1279,541,1279,612,1262,614,1260,672,1173,672,1173,651,1153,652" shape="poly">
          </map> -->
          <!-- Level 16 -->
         <!--  <div class="plans-lv16 plans-level">
            <img class="img-fluid" src="images/new/lv16.png" usemap="#floor16" alt="floor16">
          </div>
          <map name="floor16" id="floor16">
            <area alt="lvl16-A1-03" id="lvl16-A1-03" data-tooltip="Unit No: A1-03<br>Unit Type: 3BR B Duo Mirror"  href="#modal3BR-B-Duo-Mirror" coords="4,381,133,381,137,428,155,429,152,523,4,523" shape="poly">
            <area alt="lvl16-A1-04" id="lvl16-A1-04" data-tooltip="Unit No: A1-04<br>Unit Type: 2BR A"  href="#modal2BR-A" coords="175,391,301,391,299,486,224,486,224,523,154,523,158,449,176,448" shape="poly">
            <area alt="lvl16-A1-05" id="lvl16-A1-05" data-tooltip="Unit No: A1-05<br>Unit Type: 2BR A Mirror"  href="#modal2BR-A-Mirror" coords="313,391,439,393,439,449,459,451,459,518,391,523,393,486,315,486" shape="poly">
            <area alt="lvl16-A1-06" id="lvl16-A1-06" data-tooltip="Unit No: A1-06<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="458,412,482,412,482,392,572,392,572,450,588,450,586,521,462,521" shape="poly">
            <area alt="lvl16-A1-02" id="lvl16-A1-02" data-tooltip="Unit No: A1-02<br>Unit Type: 3BR B Duo"  href="#modal3BR-B-Duo" coords="5,543,154,542,152,635,133,637,134,683,5,682" shape="poly">
            <area alt="lvl16-A1-01" id="lvl16-A1-01" data-tooltip="Unit No: A1-01<br>Unit Type: 2BR C"  href="#modal2BR-C" coords="155,542,223,543,222,584,289,582,289,589,306,591,305,671,175,672,176,635,157,635" shape="poly">
            <area alt="lvl16-A1-08" id="lvl16-A1-08" data-tooltip="Unit No: A1-08<br>Unit Type: 2BR C Mirror"  href="#modal2BR-C-Mirror" coords="306,592,325,591,325,582,393,585,393,540,459,542,459,636,438,636,438,671,308,673" shape="poly">
            <area alt="lvl16-A1-07" id="lvl16-A1-07" data-tooltip="Unit No: A1-07<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="460,542,586,542,588,615,571,615,571,672,482,671,483,649,460,651" shape="poly">
            <area alt="lvl16-A3-06" id="lvl16-A3-06" data-tooltip="Unit No: A3-06<br>Unit Type: 2BR D"  href="#modal2BR-D" coords="1163,29,1290,148" shape="rect">
            <area alt="lvl16-A3-05" id="lvl16-A3-05" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1181,151,1290,150,1290,271,1221,273,1221,258,1163,260,1163,168,1184,167" shape="poly">
            <area alt="lvl16-A3-04" id="lvl16-A3-04" data-tooltip="Unit No: A3-04<br>Unit Type: 1BR A"  href="#modal1BR-A" coords="1160,272,1289,272,1292,380,1265,380,1263,364,1163,364" shape="poly">
            <area alt="lvl16-A3-03" id="lvl16-A3-03" data-tooltip="Unit No: A3-03<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1039,392,1129,392,1129,415,1152,415,1152,522,1024,524,1024,451,1040,450" shape="poly">
            <area alt="lvl16-A3-07" id="lvl16-A3-07" data-tooltip="Unit No: A3-07<br>Unit Type: 2BR D Mirror"  href="#modal2BR-D-Mirror" coords="1311,29,1440,147" shape="rect">
            <area alt="lvl16-A3-08" id="lvl16-A3-08" data-tooltip="Unit No: A3-08<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1311,148,1385,148,1386,162,1440,161,1441,253,1423,253,1423,273,1313,275" shape="poly">
            <area alt="lvl16-A3-09" id="lvl16-A3-09" data-tooltip="Unit No: A3-09<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="1313,276,1424,276,1424,296,1441,296,1441,385,1383,387,1385,401,1311,401" shape="poly">
            <area alt="lvl16-A3-10" id="lvl16-A3-10" data-tooltip="Unit No: A3-10<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1313,402,1385,402,1383,416,1440,416,1441,505,1422,507,1422,527,1311,528" shape="poly">
            <area alt="lvl16-A3-11" id="lvl16-A3-11" data-tooltip="Unit No: A3-11<br>Unit Type: 3BR A"  href="#modal3BR-A" coords="1311,525,1441,672" shape="rect">
            <area alt="lvl16-A3-02" id="lvl16-A3-02" data-tooltip="Unit No: A3-02<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1022,541,1150,542,1147,651,1128,652,1129,672,1040,672,1040,613,1022,613" shape="poly">
            <area alt="lvl16-A3-01" id="lvl16-A3-01" data-tooltip="Unit No: A3-01<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1153,542,1279,542,1279,614,1260,616,1262,669,1171,672,1173,650,1149,652" shape="poly">
          </map> -->
          <!-- Level 17-22 -->
         <!--  <div class="plans-lv17-22 plans-level">
            <img class="img-fluid" src="images/new/lv17-22.png" usemap="#floor17_22" alt="floor17_22">
          </div>
          <map name="floor17_22" id="floor17_22">
            <area alt="lvl17_22-A1-03" id="lvl17_22-A1-03" data-tooltip="Unit No: A1-03<br>Unit Type: 3BR B Duo Mirror"  href="#modal3BR-B-Duo-Mirror" coords="4,379,134,379,134,427,157,426,154,524,4,522" shape="poly">
            <area alt="lvl17_22-A1-04" id="lvl17_22-A1-04" data-tooltip="Unit No: A1-04<br>Unit Type: 2BR A"  href="#modal2BR-A" coords="175,393,298,393,299,483,223,486,223,522,155,522,157,451,175,451" shape="poly">
            <area alt="lvl17_22-A1-05" id="lvl17_22-A1-05" data-tooltip="Unit No: A1-05<br>Unit Type: 2BR A Mirror"  href="#modal2BR-A-Mirror" coords="313,393,439,392,439,447,460,448,459,522,390,523,391,485,316,486" shape="poly">
            <area alt="lvl17_22-A1-06" id="lvl17_22-A1-06" data-tooltip="Unit No: A1-06<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="458,413,482,413,482,393,571,393,571,450,588,451,588,522,459,523" shape="poly">
            <area alt="lvl17_22-A1-02" id="lvl17_22-A1-02" data-tooltip="Unit No: A1-02<br>Unit Type: 3BR B Duo"  href="#modal3BR-B-Duo" coords="4,543,152,543,154,637,135,638,134,682,4,683" shape="poly">
            <area alt="lvl17_22-A1-01" id="lvl17_22-A1-01" data-tooltip="Unit No: A1-01<br>Unit Type: 2BR C"  href="#modal2BR-C" coords="157,541,223,541,223,582,288,581,289,588,308,588,308,674,175,673,176,636,155,637" shape="poly">
            <area alt="lvl17_22-A1-08" id="lvl17_22-A1-08" data-tooltip="Unit No: A1-08<br>Unit Type: 2BR C Mirror"  href="#modal2BR-C-Mirror" coords="309,589,323,588,325,581,391,582,391,540,459,541,459,637,439,637,438,673,309,673" shape="poly">
            <area alt="lvl17_22-A1-07" id="lvl17_22-A1-07" data-tooltip="Unit No: A1-07<br>Unit Type: 2BR F Duo Mirror"  href="#modal2BR-F-Duo-Mirror" coords="462,540,504,541,506,524,589,521,589,616,572,616,572,671,482,671,483,650,462,650" shape="poly">
            <area alt="lvl17_22-A3-06" id="lvl17_22-A3-06" data-tooltip="Unit No: A3-06<br>Unit Type: 2BR D"  href="#modal2BR-D" coords="1160,27,1292,146" shape="rect">
            <area alt="lvl17_22-A3-05" id="lvl17_22-A3-05" data-tooltip="Unit No: A3-05<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1183,145,1293,147,1292,273,1221,271,1221,258,1163,258,1162,168,1184,167" shape="poly">
            <area alt="lvl17_22-A3-04" id="lvl17_22-A3-04" data-tooltip="Unit No: A3-04<br>Unit Type: 1BR A"  href="#modal1BR-A" coords="1162,271,1292,271,1293,381,1265,381,1266,364,1163,363" shape="poly">
            <area alt="lvl17_22-A3-03" id="lvl17_22-A3-03" data-tooltip="Unit No: A3-03<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1039,393,1130,393,1130,414,1152,414,1152,520,1022,520,1022,453,1040,453" shape="poly">
            <area alt="lvl17_22-A3-02" id="lvl17_22-A3-02" data-tooltip="Unit No: A3-02<br>Unit Type: 2BR F Duo"  href="#modal2BR-F-Duo" coords="1024,522,1109,520,1105,541,1149,541,1149,652,1130,653,1130,673,1039,671,1040,615,1022,615" shape="poly">
            <area alt="lvl17_22-A3-01" id="lvl17_22-A3-01" data-tooltip="Unit No: A3-01<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1150,542,1280,543,1280,614,1265,615,1263,673,1171,672,1173,652,1150,652" shape="poly">
            <area alt="lvl17_22-A3-07" id="lvl17_22-A3-07" data-tooltip="Unit No: A3-07<br>Unit Type: 2BR D Mirror"  href="#modal2BR-D-Mirror" coords="1311,28,1441,145" shape="rect">
            <area alt="lvl17_22-A3-08" id="lvl17_22-A3-08" data-tooltip="Unit No: A3-08<br>Unit Type: 2BR E1"  href="#modal2BR-E1" coords="1311,145,1385,147,1385,159,1441,159,1441,251,1422,254,1423,271,1313,270" shape="poly">
            <area alt="lvl17_22-A3-09" id="lvl17_22-A3-09" data-tooltip="Unit No: A3-09<br>Unit Type: 2BR E2 Mirror"  href="#modal2BR-E2-Mirror" coords="1313,273,1424,273,1422,295,1441,296,1443,384,1385,385,1383,401,1311,401" shape="poly">
            <area alt="lvl17_22-A3-10" id="lvl17_22-A3-10" data-tooltip="Unit No: A3-10<br>Unit Type: 2BR E2"  href="#modal2BR-E2" coords="1313,403,1382,403,1382,417,1443,415,1441,504,1424,506,1422,527,1311,527" shape="poly">
            <area alt="lvl17_22-A3-11" id="lvl17_22-A3-11" data-tooltip="Unit No: A3-11<br>Unit Type: 3BR A"  href="#modal3BR-A" coords="1311,526,1441,673" shape="rect">
          </map> -->
          <!-- Legend Plans -->
        <!--   <div class="row justify-content-center">
            <div class="col-12 col-sm-8">
              <div class="plans-legend pt-4 d-flex flex-wrap">
                <h5 class="mb-0 mr-3">Legend:</h5>
                <ul class="bg-color d-flex flex-wrap mb-0">
                  <li><div class="legend-item cheese"></div> 1 Bedroom</li>
                  <li><div class="legend-item spanish-white"></div> 2 Bedrooms</li>
                  <li><div class="legend-item shilo"></div> 3 Bedrooms</li>
                  <li><div class="legend-item foam"></div> Lift/Corridor/M&amp;E</li>
                </ul>
              </div>
            </div>
          </div> -->
          <!-- Disclaimer -->
         <!--  <div class="plans-disclaimer pt-4">
            <h5>Disclaimer</h5>
            <ul class="pl-3">
              <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
            </ul>
          </div> -->
        </div>
      </div>
      <div class="col-6 col-lg-2 order-2 order-md-3 align-self-end align-self-md-center">
        <!-- <img class="img-fluid" src="images/new/keyplan.png" alt="keyplan"> -->
      </div>
    </article>
  </section>

 
  

<!-- Modal 1BR-A -->
<div class="modal fade modalPlans" id="modal1BR-A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>1BR-A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L1)</span></dd>
                        
                        <dd><span>A3.03 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.04 (L2 - L13, L16 - L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>67.82 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>59.80 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-A -->
<div class="modal fade modalPlans" id="modal2BR-A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.04 (L2-L22)</span></dd>
                        
                        <dd><span>A2.04 (L2 - L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>81.42 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>72.87 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-A.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-A.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-A.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-A-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-A-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-A-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.05 (L2-L22)</span></dd>
                        
                        <dd><span>A2.05 (L2 - L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>81.42 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>72.87 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-A-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-A-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-A-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-B -->
<div class="modal fade modalPlans" id="modal2BR-B" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-B</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A2.06 (L2-L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L13</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>86.37 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>77.57 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-B.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-B.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-B.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-C -->
<div class="modal fade modalPlans" id="modal2BR-C" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-C</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.01 (L2-L22)</span></dd>
                        
                        <dd><span>A2.01 (L2-L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>85.55 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>76.86 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-C.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-C.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-C.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-C-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-C-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-C-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.08 (L2-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.84 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>76.99 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-C-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-C-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-C-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-D -->
<div class="modal fade modalPlans" id="modal2BR-D" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-D</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.04 (L1)</span></dd>
                        
                        <dd><span>A3.05 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.06 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.76 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.81 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-01.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-01.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-D-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-D-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-D-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-E1 -->
<div class="modal fade modalPlans" id="modal2BR-E1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-E1</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.01 (L1-L22)</span></dd>
                        
                        <dd><span>A3.03 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L1)</span></dd>
                        
                        <dd><span>A3.04 (L14-L15)</span></dd>
                        
                        <dd><span>A3.07 (L14-L15)</span></dd>
                        
                        <dd><span>A3.05 (L2-L13, L16-L22)</span></dd>
                        
                        <dd><span>A3.08 (L2-L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.65 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>73.94 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-16.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-16.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-E2 -->
<div class="modal fade modalPlans" id="modal2BR-E2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-E2</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A2.03 (L2-L13)</span></dd>
                        
                        <dd><span>A3.02 (L14, L16)</span></dd>
                        
                        <dd><span>A3.03 (L2-L13, L16-L22)</span></dd>
                        
                        <dd><span>A3.08 (L1)</span></dd>
                        
                        <dd><span>A3.09 (L14-L15)</span></dd>
                        
                        <dd><span>A3.10 (L2-L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.68 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.96 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-07.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-03.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-04.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-05.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-07.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-E2-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-E2-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-E2-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.06 (L2 - L22)</span></dd>
                        
                        <dd><span>A1.07 (L14, L16)</span></dd>
                        
                        <dd><span>A3.07 (L1)</span></dd>
                        
                        <dd><span>A3.08 (L14-15)</span></dd>
                        
                        <dd><span>A3.09 (L2-13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.12 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>75.12 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-15.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-03.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-04.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-15.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-F-Duo -->
<div class="modal fade modalPlans" id="modal2BR-F-Duo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-F-Duo</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L2-L13, L15 & L17-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>94.17 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>84.27 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-F-duo.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-F-duo.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-F-duo.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-F-Duo-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-F-Duo-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-F-Duo-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.07 (L2-L13, L15 & L17-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>94.17 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>84.27 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-F-duo-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-F-duo-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/2BR-F-duo-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-A -->
<div class="modal fade modalPlans" id="modal3BR-A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.09 (L1)</span></dd>
                        
                        <dd><span>A3.10 (L14-15)</span></dd>
                        
                        <dd><span>A3.11 (L2-13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>103.77 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>93.18 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-06.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/room/3BR-06.jpg" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-B-Duo -->
<div class="modal fade modalPlans" id="modal3BR-B-Duo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-B-Duo</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.02 (L2-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>111.44 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>99.23 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/3BR-B-duo.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-B-duo.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/3BR-B-duo.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-B-Duo-Mirror -->
<div class="modal fade modalPlans" id="modal3BR-B-Duo-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-B-Duo-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.03 (L2-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>111.44 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>99.23 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/3BR-B-duo-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-B-duo-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/3BR-B-duo-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-D-Triple -->
<div class="modal fade modalPlans" id="modal3BR-D-Triple" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="images/new/logo-tower_A.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-D-Triple</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A2.02 (L2-L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L13</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>95.97 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>86.15 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/3BR-D-triple.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-D-triple.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-md-9 d-none d-md-block">
                    <img class="img-fluid unit-plans" src="images/new/3BR-D-triple.png" alt="">
                </div>
                <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>
