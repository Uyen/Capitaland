<section id="secRes2" class="parllaxpadding">
    <div class="container-fluid">
        <div class="msd_maincontainer">
            <div id="" class="clsHeader1 loclabel" style="text-align:center;">
                <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1 clsHeaderBigFont_vn">Facilities</span> 
            </div>
            <div class="mod_location_slider" id="mod_location_slider1" style="height: 591px;">
               <div class="mod_location_slider_box">
                        <div class="mod_location_slider_item active" style="display: block; width: 700px;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_001.jpg" alt="location_location_pic1" style="transform: translate(0px, 0px); opacity: 1;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities1">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(0px, 0px); opacity: 1;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Entrance Plaza</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                            
                            <p>Verdant green walls surround the Entrance Plaza, creating a lush yet stately setting as visitors and residents arrive at D1MENSION. The architectural highlights of sweeping curves and strong pillars combine to make a striking impression, even before one steps inside. </p> 
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_003.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities3">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Residential Porte Cochere</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                            
                            <p>Porte Cocheres have historically been a feature of mansions and grand public buildings, serving both as a drop-off and pick up point as well as a grand entryway. This tradition has been revived at D1MENSION, 
                                where residents enjoy an exclusive covered entrance surrounded by lush landscaping.</p> 
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_007.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities7">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">The Playtime Park</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                            
                            <p>Surrounded by lush greenery, The Playtime Park provides a space for little ones to simply be themselves. From discovering the world around them to engaging in imaginative play with each other, it is ideal for families with children of all ages.</p> 
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_008.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities8">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Koi Pond</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                              <p>38 “Lucky Stars” at D1MENSION:  In Eastern culture, 38 is one of the luckiest numbers, often referred to as “triple prosperity” or “growing towards success”. Introducing D1MENSION’s “lucky stars” – our 38 auspicious Koi to preserve your prosperity as a valued resident of D1MENSION.
                              </p>
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_009.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities9">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Sky Gym</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                              <p>Enjoy a stunning view of Ho Chi Minh City as you work out in the exclusive Sky Gym. Equipped with state-of-the-art equipment and located on the 15<sup>th</sup> level of Dimension, the adjacent Sky Changing Rooms, 
                                Sauna &amp; Steam Bath provide a holistic experience in luxurious style.</p> 
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_011.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities11">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Sky Changing Room with Sauna & Steam Bath</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_014.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities14">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Sky Party House</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                              <p>The expansive Sky Party House on the 16<sup>th</sup> level comes equipped with a lounge area, private dining room, cigar room and wine cellar. With indulgent yet intimate décor, it makes the perfect setting for celebrating special occasions with friends and family. </p>
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_015.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities15">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Sky Wine & Cigar Lounge</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        <div class="mod_location_slider_item" style="width: 700px; display: none;">
                            <div class="thumb">
                            
                                <img src="img/img_location/Facilities_005.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                                
                                <div id="" class="clsBarInner zoomMap_facilities5">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                            
                            <!-- header -->
                              <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                      
                                      <div class="groupheader">
                                          <div id="">
                                              <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Sky Infinity Pool with Glass Bottom</span> 
                                          </div>
                                      </div>
                                      
                                  </div>  
                              <!-- end header -->
                            
                            <p>The Sky Infinity Pool on the 16th level provides a truly exquisite experience. Whether you are simply relaxing on the pool deck or enjoying a swim, the city unfolds beneath you and provides a striking backdrop at any time of the day.</p> 
                            
                            </div>
                        </div>
                        <!-- END mod_location_slider_item -->
                        </div>
                <!-- END mod_location_slider_box-->
                <!-- Controls -->
                <style>
                    .controlx_btn img{width:70%!important; height:auto;}
                    .controlx_btn{width:5%; text-align:center;}
                    .controlx_btn:before{content:normal!important;}
                </style>
                <div class="controlx" style="width: 700px;">
                    <div class="controlx_box" >
                        <div class="controlx_btn" style="position: unset;"><img src="msd_assets/plugins/slideshow/img/slider_arrowl.png" alt=""></div>
                        <div class="controlx_btn" style="position: unset;"><img src="msd_assets/plugins/slideshow/img/slider_arrowr.png" alt=""></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="paginax" style="width: 700px;">
                    <div class="paginax_box"><span class="activex"></span><span></span><span></span><span></span><span></span><span></span><span></span></div>
                </div>
            </div>
            <!-- END mod_location_slider-->
            <div id="" class="clsHeader1 loclabel" style="text-align:center;">
                <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1 clsHeaderBigFont_vn">Interior</span> 
            </div>
            <style>
                /* jssor slider bullet navigator skin 05 css */
                /*
                .jssorb05 div           (normal)
                .jssorb05 div:hover     (normal mouseover)
                .jssorb05 .av           (active)
                .jssorb05 .av:hover     (active mouseover)
                .jssorb05 .dn           (mousedown)
                */
                .jssorb05 {
                position: absolute;
                display:none!important;
                }
                .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                position: absolute;
                /* size of bullet elment */
                width: 16px;
                height: 16px;
                background: url('msd_assets/plugins/slideshow/img/b05.png') no-repeat;
                overflow: hidden;
                cursor: pointer;
                }
                .jssorb05 div { background-position: -7px -7px; }
                .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
                .jssorb05 .av { background-position: -67px -7px; }
                .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
                /* jssor slider arrow navigator skin 12 css */
                /*
                .jssora12l                  (normal)
                .jssora12r                  (normal)
                .jssora12l:hover            (normal mouseover)
                .jssora12r:hover            (normal mouseover)
                .jssora12l.jssora12ldn      (mousedown)
                .jssora12r.jssora12rdn      (mousedown)
                */
                .jssora12l, .jssora12r {
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 30px;
                height: 46px;
                cursor: pointer;
                /*background: url('msd_assets/plugins/slideshow/img/a12.png') no-repeat;
                overflow: hidden;*/
                color:#fff!important;
                font-size:24px!important;
                text-align:center;
                }
                .jssora12l img, .jssora12r img{width:70%; height:auto;}
                /*
                .jssora12l:before {
                content: '\2329';
                }
                .jssora12r:before {
                content: '\232A';
                }
                */
                .jssora12l { background-position: -16px -37px; }
                .jssora12r { background-position: -75px -37px; }
                .jssora12l:hover { background-position: -136px -37px; }
                .jssora12r:hover { background-position: -195px -37px; }
                .jssora12l.jssora12ldn { background-position: -256px -37px; }
                .jssora12r.jssora12rdn { background-position: -315px -37px; }
            </style>

            <div class="container">
                <!-- Jssor Slider Begin -->

                <style>
                    /* jssor slider loading skin spin css */
                    .jssorl-009-spin img {
                        animation-name: jssorl-009-spin;
                        animation-duration: 1.6s;
                        animation-iteration-count: infinite;
                        animation-timing-function: linear;
                    }

                    @keyframes jssorl-009-spin {
                        from {
                            transform: rotate(0deg);
                        }

                        to {
                            transform: rotate(360deg);
                        }
                    }
                </style>

                <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto; width: 1140px; height: 700px; overflow: hidden;">
                    <!-- Loading Screen -->
                    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
                    </div>

                    <!-- Slides Container -->
                    <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1140px; height: 700px;
                    overflow: hidden;">
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/3BR-01.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/3BR-02.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/3BR-04.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/3BR-07.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/3BR-08.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/3BR-11.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-02.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-05.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-06.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-07.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-08.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-13.jpg')?>" />
                        </div>
                        <div>
                            <img data-u="image" src="<?=get_resource_url('img/img_gallery/slider/4BR-15.jpg')?>" />
                        </div>
                    </div>
                    
                    <!--#region Bullet Navigator Skin Begin -->
                    <!-- Help: https://www.jssor.com/development/slider-with-bullet-navigator.html -->
                    <style>
                        .jssorb031 {position:absolute;}
                        .jssorb031 .i {position:absolute;cursor:pointer;}
                        .jssorb031 .i .b {fill:#000;fill-opacity:0.5;stroke:#fff;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.3;}
                        .jssorb031 .i:hover .b {fill:#fff;fill-opacity:.7;stroke:#000;stroke-opacity:.5;}
                        .jssorb031 .iav .b {fill:#fff;stroke:#000;fill-opacity:1;}
                        .jssorb031 .i.idn {opacity:.3;}
                    </style>
                    <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                        <div data-u="prototype" class="i" style="width:16px;height:16px;">
                            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                            </svg>
                        </div>
                    </div>
                    <!--#endregion Bullet Navigator Skin End -->
                
                    <!--#region Arrow Navigator Skin Begin -->
                    <!-- Help: https://www.jssor.com/development/slider-with-arrow-navigator.html -->
                    <style>
                        .jssora051 {display:block;position:absolute;cursor:pointer;}
                        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
                        .jssora051:hover {opacity:.8;}
                        .jssora051.jssora051dn {opacity:.5;}
                        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
                    </style>
                    <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                        </svg>
                    </div>
                    <!--#endregion Arrow Navigator Skin End -->
                </div>
                <!-- Jssor Slider End -->
            </div>


            <!-- #endregion Jssor Slider End -->
            <div id="" class="clsHeader1 loclabel" style="text-align:center; display:none!important;">
                <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1 clsHeaderBigFont_vn">Level Plan</span> 
            </div>
            <div class="mod_location_slider" id="mod_location_slider2" style="display: none; height: 0px;">
                <div class="mod_location_slider_box">
                    <div class="mod_location_slider_item active" style="width: 1343px;">
                        <div class="thumb">
                            <img src="img/img_location/Levelplan_001.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_levelplan1">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc">
                            <div class="legend_levelplan">
                                <p style="font-family: Asap-Regular;">LEGEND</p>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;2 BEDROOMS<br>
                                    <span style="background-color:#8e929d;"></span>&nbsp;&nbsp;3 BEDROOMS
                                </div>
                                <div class="legend_levelplan_col1">
                                    <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/Levelplan_002.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_levelplan2">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc">
                            <div class="legend_levelplan">
                                <p style="font-family: Asap-Regular;">LEGEND</p>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;2 BEDROOMS<br>
                                    <span style="background-color:#8e929d;"></span>&nbsp;&nbsp;3 BEDROOMS
                                </div>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#9eb2ad;"></span>&nbsp;&nbsp;4 BEDROOMS<br>
                                    <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/Levelplan_003.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_levelplan3">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc">
                            <div class="legend_levelplan">
                                <p style="font-family: Asap-Regular;">LEGEND</p>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;PENTHOUSE<br>
                                    <span style="background-color:#8e929d;"></span>&nbsp;&nbsp;SKY CHANGING ROOMS<br class="nonex"><span class="nonex"></span><i class="nonex">&nbsp;</i>&nbsp;WITH SAUNA &amp; STEAM BATH<br>
                                    <span style="background-color:#696e80;"></span>&nbsp;&nbsp;SKY GYM
                                </div>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#607b74;"></span>&nbsp;&nbsp;LANDSCAPE<br>
                                    <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/Levelplan_004.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_levelplan4">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc">
                            <div class="legend_levelplan">
                                <p style="font-family: Asap-Regular;">LEGEND</p>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;PENTHOUSE<br>
                                    <span style="background-color:#676d7a;"></span>&nbsp;&nbsp;SKY PARTY HOUSE<br>
                                    <span style="background-color:#767987;"></span>&nbsp;&nbsp;SKY INFINITY POOL<br class="nonex"><span class="nonex"></span><i class="nonex">&nbsp;</i>&nbsp;WITH GLASS BOTTOM<br>
                                </div>
                                <div class="legend_levelplan_col1">
                                    <span style="background-color:#7d6650;"></span>&nbsp;&nbsp;SKY POOL DECK<br>
                                    <span style="background-color:#627e76;"></span>&nbsp;&nbsp;LANDSCAPE<br>
                                    <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                </div>
                <!-- END mod_location_slider_box-->
                <!-- Controls -->
                <div class="controlx" style="width: 1343px;">
                    <div class="controlx_box">
                        <div class="controlx_btn"></div>
                        <div class="controlx_btn"></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="paginax" style="width: 1343px;">
                    <div class="paginax_box"><span class="activex"></span><span></span><span></span><span></span></div>
                </div>
            </div>
            <!-- END mod_location_slider-->
            <div class="disclaimerx" style="display:none!important;"><strong>DISCLAIMER:</strong>  THE PLAN IS SUBJECT TO CHANGE AS MAY BE APPROVED BY THE RELEVANT AUTHORITIES. |  THE PLAN IS NOT DRAWN TO SCALE. DIMENSIONS ARE SHOWN IN MILLIMETERS. | THE AREA(S) QUOTED IS AN APPROXIMATE MEASUREMENT AND SUBJECT TO FINAL SURVEY BY REGISTERED SURVEYOR.</div>
            <div id="" class="clsHeader1 loclabel" style="text-align:center; display:none!important;">
                <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1 clsHeaderBigFont_vn">Unit Plan</span> 
            </div>
            <div class="mod_location_slider" id="mod_location_slider3" style="display: none; height: 0px;">
                <div class="mod_location_slider_box">
                    <div class="mod_location_slider_item active" style="width: 1343px;">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_001.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit1">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_002.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit2">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_003.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit3">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_004.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit4">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_005.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit5">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_006.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit6">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_007.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit7">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_008.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit8">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                    <div class="mod_location_slider_item">
                        <div class="thumb">
                            <img src="img/img_location/UnitPlan_009.jpg" alt="location_location_pic1">
                            <div id="" class="clsBarInner zoomMap_unit9">
                                <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                    <a>CLICK TO VIEW 
                                    <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="desc" style="display:none;">
                            No Content
                        </div>
                    </div>
                    <!-- END mod_location_slider_item -->
                </div>
                <!-- END mod_location_slider_box-->
                <!-- Controls -->
                <div class="controlx" style="width: 1343px;">
                    <div class="controlx_box">
                        <div class="controlx_btn"></div>
                        <div class="controlx_btn"></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="paginax" style="width: 1343px;">
                    <div class="paginax_box"><span class="activex"></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></div>
                </div>
            </div>
            <!-- END mod_location_slider-->
            <div class="disclaimerx" style="display:none!important;"><strong>DISCLAIMER:</strong>  THE PLAN IS SUBJECT TO CHANGE AS MAY BE APPROVED BY THE RELEVANT AUTHORITIES. | THE PLAN IS NOT DRAWN TO SCALE. DIMENSIONS ARE SHOWN IN MILLIMETERS. | THE AREA(S) QUOTED IS AN APROXIMATE MEASUREMENT AND SUBJECT TO FINAL SURVEY BY REGISTERED SURVEYOR. | FURNITURE ARE SHOWN FOR ILLUSTRATION PURPOSES ONLY AND ARE NOT NECESSARILY PROVIDED UPON HANDOVER.</div>
            <div style="width:100%; height:20px;"></div>
        </div>
        <!-- END msd_maincontainer -->
    </div>
</section>


<!-- Bootstrap core JavaScript
================================================== -->
<script src="<?=get_resource_url('/js/jquery-3.1.1.min.js')?>"></script>
<!-- jssor slider scripts-->
<script type="text/javascript" src="<?=get_resource_url('msd_assets/plugins/slideshow/js/jssor.slider-22.1.5.mini.js')?>"></script>
<script>

    jQuery(document).ready(function ($) {
        var options = {
            $AutoPlay: 1,                                       //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
            $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
            $Idle: 2000,                                        //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
            $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

            $ArrowKeyNavigation: 1,   			                //[Optional] Steps to go for each navigation request by pressing arrow key, default value is 1.
            $SlideEasing: $Jease$.$OutQuint,                    //[Optional] Specifies easing for right to left animation, default value is $Jease$.$OutQuad
            $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
            $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide, default value is 20
            //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
            //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
            $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
            $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
            $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
            $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)

            $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
            },

            $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                $SpacingX: 12,                                  //[Optional] Horizontal space between each item in pixel, default value is 0
                $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
            }
        };

        var jssor_slider1 = new $JssorSlider$("slider1_container", options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizing
        function ScaleSlider() {
            var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
            if (parentWidth) {
                jssor_slider1.$ScaleWidth(parentWidth - 30);
            }
            else
                window.setTimeout(ScaleSlider, 30);
        }
        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
</script>