  <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2 hidden-xs" data-parallax="scroll" data-image-src="img/locationhero.jpg"> 

                    <div class="clsArtImpression2">
                        <!--<span class="artimp2">Artist's impression  </span>-->
                    </div>
                    
                </div>
                
                <img id="imgLocKeyVis-mobile" src="img/location-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                <div  id="divPgKeyOuterTxtWrap">
                
                    <div id="divLocPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center locHeaderVNAlign" >

                            <div id="divLocPgKeyVisHeadTxt1">
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont text-left ">Setting </span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont text-left ">THE </span> 
                            </div>

                            <div id="divLocPgKeyVisHeadTxt2">

                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont ">Standard</span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont ">IN&nbsp;</span> 
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont ">ASIA </span>
                            </div>
                        </div>
                        


                    </div>

                </div>
            </div>    
           

        </section>

        
        
        <section id="sec" class="parllaxpadding"> 
                
            
            <div class="container-fluid">
	
				<div class="msd_maincontainer">
                
                    
                    <!-- header -->
                    <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                            
                            <div class="groupheader">
                                <!--
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Sophistication</span> 
                                </div>

                                <div id="divIntroSlideHeadTxt2">
                                	<span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inside</span>
                                    <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">and&nbsp;</span> 
                                    <span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">out</span>
                                </div>
                                -->
                            </div>
                            
                        </div>  
                    <!-- end header -->
                    
                    <style>
                      

                       
                        
                    </style>
                    
                    
                    
                    <div class="loc_map1"><img src="img/LocationMap4.jpg" alt="location_map1" style="margin-bottom:20px;" /></div>
                    
                    <div id="IntroBarInner" class="clsBarInner zoomLocationMap1">
                        <div class=" col-xs-12 text-center clsNormalFont text-uppercase">
                            <a>Click to see more
                            <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
     
                    
   
                    
                <div class="loc2_map1_legend">
                            
                           <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon1">LANDMARKS</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">1</span> Bitexco Financial Tower</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">2</span> Pullman Saigon Centre</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">3</span> Park Hyatt Saigon</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">4</span> InterContinental Asiana Saigon</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">5</span> Le Méridien Saigon</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">6</span> The Reverie Saigon</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">7</span> Hotel Nikko Saigon</p>
                                    
                                    
                                </div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">8</span> Hotel Equatorial Ho Chi Minh City</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">9</span> Ben Thanh Market</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">10</span> Notre-Dame Cathedral Basilica of Saigon</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">11</span> Saigon Opera House</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">12</span> Cholon Market</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">13</span> Government Guest House</p>
                                    <p class="lgnd_1"><span  class="a1 amenitiesCircle">14</span> Saigon Central Post Office</p>
                                    
                                </div>
                                <div class="clear"></div>
                            </div>
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon4">RETAIL &amp; LEISURE</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">15</span> Vincom Center Shopping Mall</span></p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">16</span> GEM Center</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">17</span> Chill Skybar</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">18</span> Riverside Palace</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">19</span> Saigon Center</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">20</span> Chinatown</p>
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">21</span> Now Zone Plaza</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">22</span> An Dong Market</p>
                                    <p class="lgnd_1"><span  class="a2 amenitiesCircle">23</span> 23/9 Park</p>
                                     <p class="lgnd_1"><span  class="a2 amenitiesCircle">24</span> Tao Dan Park</p>
                                      <p class="lgnd_1"><span  class="a2 amenitiesCircle">25</span> Fine Arts Museum</p>
                                      <p class="lgnd_1"><span  class="a2 amenitiesCircle">26</span> Thong Nhat Stadium</p>
                                     
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                            
                
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon6">BUSINESS ADMINISTRATION </div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">27</span> HCM City People’s Committee</p>
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">28</span> State Bank of Vietnam</p>
                                    
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">29</span> HCM City Police Office</p>
                                    <p class="lgnd_1"><span  class="a3 amenitiesCircle">30</span> HCM Fire Department</p>
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                
                
                            
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon2">MEDICAL</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">31</span> Saigon General Hospital</p>
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">32</span> Columbia Asia International Clinic – Saigon</p>
                                    
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">33</span> FV Saigon Clinic</p>
                                    <p class="lgnd_1"><span  class="a4 amenitiesCircle">34</span> Hospital for Traumatology and Orthopaedics</p>
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                            
                           
                            
                            <div class="loc_map1_legend_set">
                                <div class="loc_map1_legend_cols_h1 icon5">EDUCATION</div>
                                
                                <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a5 amenitiesCircle">35</span> American International School</p>
                                    <p class="lgnd_1"><span  class="a5 amenitiesCircle">36</span> Ho Chi Minh University of Science</p>
                                    
                                </div>
                                
                                 <div class="loc_map1_legend_cols">
                                    <p class="lgnd_1"><span  class="a5 amenitiesCircle">37</span> Le Hong Phong High School</p>
                                     <p class="lgnd_1"><span  class="a5 amenitiesCircle">38</span> Saigon University</p>
                                    
                                </div>
                                
                                
                                <div class="clear"></div>
                            </div>
                            
                            
                           
                            
                            
                        </div>

                    
                    <div style="width:100%; height:50px;"></div>
            

                    <!-- /* OSR  Begin - KEY PLACES */ -->
            
                        <style>
                            #divLocImgs {width:97% !important;margin:0 auto;}
                            
                            .recent-work-wrap .overlay .preview {
                                bottom: 0;
                                display: inline-block;
                                height: 35px;
                                line-height: 35px;
                                border-radius: 0;
                                background: transparent;
                                text-align: center;
                                color: #fff;
                                z-index: 1;
                                position: absolute;
                                
                                width: 100%;
                                height: 100%;

                                top: 50%;
                                left: 0;
                                padding: 0;

                            }
                            
                            @media only screen and (max-width : 740px) {#divLocImgs {width:90% !important;}}
                        </style>

                    <div id="divLocImgs">
                        
                       
                        <div class="row">
                            <div class="portfolio-items">
                                <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <a href="" ><img class="img-responsive" src="img/keyplaces/thumbs/Ben-Thanh-Market.jpg" alt=""></a>
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item joomla bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Bitexco-Financial-Tower.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item bootstrap wordpress col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Cho-Lon-Market.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>        
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item joomla wordpress apps col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/City-People-Committee.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>           
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item joomla html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/City-Post-Office.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>      
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item wordpress html apps col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Columbia-Asia-Hospital.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>         
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item wordpress html col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Gem-Center.jpg" alt="">
                                        <div class="overlay">
                                            <div class="recent-work-inner">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->

                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Hotel-Inter-Continental.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Hotel-Park-Hyatt.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Le-Meridien.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Museum-of-Ho-Chi-Minh-City.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Opera-House.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Saigon-Center.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Saigon-Notre-Dame-Basilica.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/State-Bank-of-Vietnam.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Sunwah-Tower.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Tao-Dan-Park.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/The-Reverie.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Thien-Hau-Temple.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                                <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                    <div class="recent-work-wrap">
                                        <img class="img-responsive" src="img/keyplaces/thumbs/Vincom-Center.jpg" alt="">
                                        <div class="overlay ">
                                            <div class="recent-work-inner ">
                                                <h3></h3>
                                                <p></p>
                                                <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                            </div> 
                                        </div>
                                    </div>          
                                </div><!--/.portfolio-item-->
                                
                            </div>
                        </div>
                        
                        
                    </div>
                    <!-- /* OSR  End - KEY PLACES */ -->
            
            
                
                </div><!-- END msd_maincontainer -->
                
                

				
                


            </div>
     
            
            

        </section>