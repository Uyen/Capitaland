<section id="divKeyVisual" class="clsKeyvisual">
   <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
      <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/developerhero.jpg">
         <!-- <div class="clsArtImpression2">
            <span class="artimp1">Artist's impression  </span>
         </div> -->
      </div>
      <img id="imgDevKeyVis-mobile" src="img/developer-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
      <div  id="divPgKeyOuterTxtWrap">
         <div id="divDevPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >
            <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">
               <div id="divDevPgKeyVisHeadTxt1" >
                  <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont ">Where Extraordinary </span> 
               </div>
               <div id="divDevPgKeyVisHeadTxt2">
                  <span class="clsHeaderSmallFont clsHeader1   clsHeaderNormalFont">IS THE </span> 
                  <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont">Only Way </span> 
                  <span class="clsHeaderSmallFont clsHeader1 clsHeaderNormalFont">TO </span>
                  <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFont">LIVE </span>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="developer-slider parllaxpadding" >
   <div class="container " id="deverloperPgContainer" style="background-color:white;padding:0">
      <div style="display:block; width:100%; height:50px;"></div>
      <div class="mod_location_slider" id="mod_location_slider1">
         <div class="mod_location_slider_box">
            <div class="mod_location_slider_item active">
               <div class="thumb">
                  <img src="img/developer11.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities1" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="mod_location_slider_item">
               <div class="thumb">
                  <img src="img/developer2.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities2" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="mod_location_slider_item">
               <div class="thumb">
                  <img src="img/developer3.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities3" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="mod_location_slider_item">
               <div class="thumb">
                  <img src="img/developer4.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities4" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="controlx">
               <div class="controlx_box">
                  <div class="controlx_btn"></div>
                  <div class="controlx_btn right"></div>
                  <div class="clear"></div>
               </div>
            </div>
         </div>
         <!-- END mod_location_slider_box-->
         <!-- Controls -->
      </div>
      <!-- END mod_location_slider-->
      <div id="secDeveloperPgPara" class="clsPurple2">
         <p>
            CapitaLand is one of Asia’s largest real estate companies headquartered and listed in Singapore. The company leverages its significant asset base, design and development capabilities, active capital management strategies, extensive market network and operational capabilities to develop high-quality real estate products and services. Its diversified global real estate portfolio includes integrated developments, shopping malls, serviced residences, offices and homes.
         </p>
         <p>
            Vietnam is one of CapitaLand’s key markets in Asia. The real estate market in Vietnam is supported by the country’s strong economic growth, rapid urbanization and a young and growing population. CapitaLand is committed to being a long-term real estate developer in the country. CapitaLand Group's current presence in Vietnam is in the six major cities – Ho Chi Minh City, Hanoi, Hai Phong, Da Nang, Binh Duong, and Nha Trang in the residential and serviced residences sectors.
         </p>
         <p>
            Vietnam is one of CapitaLand’s key markets in Asia. The real estate market in Vietnam is supported by the country’s strong economic growth, rapid urbanization and a young and growing population. CapitaLand is committed to being 
            a long-term real estate developer in the country. CapitaLand Group's current presence in Vietnam is in the six major cities – Ho Chi Minh City, Hanoi, Hai Phong, Da Nang, Binh Duong, and Nha Trang in the residential and serviced residences sectors.
         </p>
      </div>
   </div>
</section>


<style type="text/css" >
      #secIntroSlider{background:none;}
      .msd_slider_colset.active{display: block !important;}
</style>
<section class="intro-slider" id="secIntroSlider">
  <!--  -->
            <div class="container-fluid nopadding" style="background-color:white;padding:0">
            
                  <!-- MSD SLIDER 1 -->
                  <div id="slider1" class="msd_slider" style="width: 90%; height: 962px;">
      
                      <div class="msd_slider_colset active">
                  
                          <div class="msd_slider_col"><div class="msd_slider_col_box" style="text-align: center;">Thumbnail</div></div>
						  <div class="msd_slider_col"><div class="msd_slider_col_box">Description</div></div>
                      
                      </div><!-- end msd_slider_colset -->
                      
                      <div class="msd_slider_colset">
                  
                          <div class="msd_slider_col"><div class="msd_slider_col_box" style="text-align: center;">Thumbnail</div></div>
						  <div class="msd_slider_col"><div class="msd_slider_col_box">Description</div></div>
                      
                      </div><!-- end msd_slider_colset -->
                      
                      <div class="clear"></div>
                      
                      <!-- Controls -->
                      <div class="controlx"><div class="controlx_btn"></div><div class="controlx_btn"></div><div class="clear"></div></div>
                  
                  </div><!-- end msd_slider -->
                  
                  <div id="sliderdata1" class="msd_slider_data">
                  
                    <!-- Slider 1 Data 1 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                      <div class="data_a"><img id="imgCeo" src="img/d1_ceo-vn.png" alt="" width="300" height="508" /></div>
                      <div class="data_b">
                      
                      
                       <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                            
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">A</span> 
                                    <span class="clsHeaderBigFont  clsHeaderNormalFont  clsHeader1">Message</span> 
                                    <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">from</span> 
                                </div>

                                <div id="divIntroSlideHeadTxt2">
                                    
                                    <span class="clsHeaderBigFont clsHeaderNormalFont clsHeader1 ">CapitaLand </span>
                                </div>
                            </div>
                            <div class=" clsPurple2">
                                <p class="text-center">
                                    It is with great pleasure that I introduce D1MENSION, the first branded residence available for sale located in District 1, Ho Chi Minh City by international developer, CapitaLand and with property management by the world-renowned The Ascott Limited. 
                                    From the exclusive lifestyle to the quality of provisions, CapitaLand invites you to truly make a statement in life at D1MENSION.
                                    
                                    
                                </p>
                                <p class="text-center"><a href="messagefromcapitaland.html">Read More</a></p>
                            </div>  
                        </div>
                                          
                      
                      </div><!-- end data_b -->
                      
                      <!-- Slider 1 Data 2 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                      <div class="data_a"><img id="imgAscott" src="img/ascott_cgm-vn.png" alt="" width="300" height="508"/></div>
                      <div class="data_b">
                      
                        <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                             
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1_2" class="clsHeader1">
                                     <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">A</span> 
                                    <span class="clsHeaderBigFont  clsHeaderNormalFont  clsHeader1">Message</span> 
                                    <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">from</span> 
                                </div>

                                <div id="divIntroSlideHeadTxt2_2">
                                    <span class="clsHeaderBigFont clsHeaderNormalFont clsHeader1 ">The Ascott Limited </span>
                                   
                                </div>
                            </div>
                            <div class="text-center clsPurple2">
                                <p class="text-center">
                                    On behalf of The Ascott Limited, we are proud to be appointed the future property manager of CapitaLand’s residential project D1MENSION and the operator of the Somerset D1MENSION Ho Chi Minh City, located within the D1MENSION development.  
                                    From the comfort of residential-style amenities to the warmth of our staff, we invite you to truly live life at its best at D1MENSION.
                                </p>
                                <p class="text-center"><a href="messagefromascott.html">Read More</a></p>
                            </div>
                        
                        </div>
                 
                      
                      </div><!-- end data_b -->
                      
                  </div><!-- MSD SLIDER 1 END -->
                  
               

                
        
            </div>
        </section>
		<style>
		#secIntroSlider {
			min-height: 700px !important;  
		}
		#secIntroSlider #slider1.msd_slider {
			min-height: 599px !important;  
		}
		#secIntroSlider .container-fluid {
			max-width: 1140px !important;
			margin: 0 auto !important;
		}
		#secIntroSlider .msd_slider_colset, #secIntroSlider .msd_slider .msd_slider_col {
			width: 100% !important;
			max-width: 1024px !important;
		}
		#secIntroSlider .msd_slider_col_box img {
			/* width: 550px !important; */
			margin: 0 auto !important;
			display: inline-block;
      text-align: center;
		}
		</style>

        