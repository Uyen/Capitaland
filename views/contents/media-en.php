 <div class="wrapper media-page">
        
         <!-- Section - Media -->
         <section class="section section-auto" id="media" data-section-name="media">
               <div class="msd_maincontainer" style="    color: black; background: none;">
                  <div class="col-md-12">
                     <h4 class="headline" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                        <span class="ml-0">
                        <a data-toggle="modal" data-target="#promo-28052018"><small>NEWS/MEDIA</small></a>
                        </span>
                     </h4>
                  </div>
                  <div class="col-md-12">
                     <div class="media-wrapper row">
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#the-ascott-services">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd7.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>19 June 2019</small></div>
                                 <div class="media-caption">
                                    <p>The Ascott Services' Excellence at D1MENSION</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#infinite-beauty">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd8.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>22 May 2019</small></div>
                                 <div class="media-caption">
                                    <p>Infinite Beauty | A Sunset Soiree at D1MENSION</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#de-la-sol-video-emergence-cbd-lite">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd1.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>26 May 2018</small></div>
                                 <div class="media-caption">
                                    <p>Make a Statement in Life</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#de-la-sol-video-feel-the-pulse">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd2.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>22 May 2018</small></div>
                                 <div class="media-caption">
                                    <p>Make a Statement in Life - VN</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#event-project-introduction">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd3.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>20 May 2018</small></div>
                                 <div class="media-caption">
                                    <p>Timeless Elegance & Rare Extravagances</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#sales-gallery-grand-opening">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd4.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>12 May 2018</small></div>
                                 <div class="media-caption">
                                    <p>One Day In Saigon | Một Ngày Ở Sài Gòn</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#show-suites-vip-preview">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd5.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>9 May 2018</small></div>
                                 <div class="media-caption">
                                    <p>Lễ Ra Mắt Đặc Biệt | Exclusive Sales Event</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-3 col-sm-6 pl-0 pt-4">
                           <a class="news_child" data-toggle="modal" data-target="#signing-ceremony-4-may">
                              <div class="media-thumb"><img class="img-fluid" src="img/media/vd6.jpg"></div>
                              <div class="media-info position-relative">
                                 <div class="media-date"><small>4 May 2018</small></div>
                                 <div class="media-caption">
                                    <p>Cơ Hội Đầu Tư Danh Giá| An Irresistible Investment Opportunity</p>
                                 </div>
                                 <div class="media-arrow d-flex justify-content-end">
                                    <div class="lg-next lg-icon"></div>
                                 </div>
                              </div>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
         </section>
      </div>
      <!-- Media Modals [Auto-generated by Jekyll]-->
      <!-- Modal for "Signing Ceremony of Strategic Partnership with Exclusive Distribution Agencies" -->
    <div class="modal modal-media" id="signing-ceremony-4-may" tabindex="-1" role="dialog" aria-labelledby="signing-ceremony-4-may" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <!-- <div class="carousel-item align-self-stretch active">
                          <iframe width="560" height="315" src="https://www.youtube.com/embed/iV_h3gnzY_Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/iV_h3gnzY_Q" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "Show Suites VIP Preview" -->

      <div class="modal modal-media" id="show-suites-vip-preview" tabindex="-1" role="dialog" aria-labelledby="show-suites-vip-preview" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <!-- <div class="carousel-item align-self-stretch active">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/Pw28LLVwp5I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/Pw28LLVwp5I" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal for "Show Suites VIP Preview" -->
      <div class="modal modal-media" id="sales-gallery-grand-opening" tabindex="-1" role="dialog" aria-labelledby="sales-gallery-grand-opening" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <!-- <div class="carousel-item align-self-stretch active">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/U7Lup1CkXho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/U7Lup1CkXho" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "The Grand Opening of Sales Gallery & Show Suites" -->
<div class="modal modal-media" id="event-project-introduction" tabindex="-1" role="dialog" aria-labelledby="event-project-introduction" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <!-- <div class="carousel-item align-self-stretch active">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/BaS5mrxSTRE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/BaS5mrxSTRE" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "De La Sol Project Introduction" -->
      <div class="modal modal-media" id="event-project-introduction" tabindex="-1" role="dialog" aria-labelledby="event-project-introduction" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="event-project-introduction-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/BaS5mrxSTRE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#event-project-introduction-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#event-project-introduction-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>20 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "Feel The Pulse Of The CBD Lite. All In One Place" -->
      <div class="modal modal-media" id="de-la-sol-video-feel-the-pulse" tabindex="-1" role="dialog" aria-labelledby="de-la-sol-video-feel-the-pulse" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-feel-the-pulse-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <!-- <div class="carousel-item align-self-stretch active">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/xsvlYmJT6wA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/xsvlYmJT6wA" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>22 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal for "De La Sol by CapitaLand: The Emergence of the CBD Lite" -->
      <div class="modal modal-media" id="de-la-sol-video-emergence-cbd-lite" tabindex="-1" role="dialog" aria-labelledby="de-la-sol-video-emergence-cbd-lite" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/pZBvI7BuzY4" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal for "The Ascott Services" -->
      <div class="modal modal-media" id="the-ascott-services" tabindex="-1" role="dialog" aria-labelledby="the-ascott-services" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/uc4fXpbmZFU" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>19 June 2019</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal for "Infinite Beauty" -->
      <div class="modal modal-media" id="infinite-beauty" tabindex="-1" role="dialog" aria-labelledby="infinite-beauty" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <div class="videoWrapper">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/NHFBN5k3XvI" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>22 May 2019</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      