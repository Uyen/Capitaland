<div class="main-navbar">
                <nav class="navbar navbar-inverse navbar-fixed-top" style="display: none;">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        
                        
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            
                        </button>
                        
                        
                        
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">

                            <ul class="nav nav-pills nav-stacked text-uppercase">
                                <li id="mnuReg" class="clsGoldbg" data-toggle="elementscroll"><a href="register-vn.php">Đăng kí thông tin  </a></li>

                                <li class=""><a href="introduction-vn.php" data-toggle="elementscroll">Giới thiệu</a></li>
                                <li><a href="location-vn.php" data-toggle="elementscroll">Vị trí </a></li>
                                <li><a href="residences-vn.php" data-toggle="elementscroll">Chi tiết dự án</a></li>
                                <li><a href="developer-vn.php" data-toggle="elementscroll">Chủ đầu tư</a></li>

                            </ul>



                        </div>
                    </div>
                    
                    <div id="divLangToggle"> <a href="index-en.php">EN</a> / <a href="index.php"> VN</a> </div>
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
           <style>
           
           
               
            #divTop2Logo{position: relative; padding-left: 4%; top: 75px;}
            #logos{height: auto; width: 18%; padding-right: 20px;}
            /*#divTop2LogoMobile{position: relative;}*/
            /*#logosMobile{height: 80px;width:auto;}*/
               /*#imgHomeSec4Right-mobile{padding-top: 300px;}  */
               
               
            </style>
            
            <div id="divMenu" class="hidden-xs ">
              
                <div id="divTop2Logo">
                
                    <img id="logos" src="img/logos.png" alt="capitaland logo" width="379" height="161">
                   
                </div>

            </div>
            
            
            <section id="secIntro" class="hero"> 
                
                <div class="texture-overlay"></div> 
                <div class="container-fluid">
                    
                    
                    <div class="row hero-content"> 
                        <div class="col-md-12  nopadding  wow fadeIn animated" data-wow-duration="500ms" data-wow-delay="10ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 10ms; animation-name: fadeIn;">
                            <div id="divTopLogo" class="hidden-xs">
                                <!-- <img id="imgTopLogo" src="img/D1Logo.png" alt="" width="927" height="235"> -->
                            </div>
                            <img id="imgHomeSec4Right" src="img/Website_V5-2-2.jpg" alt="" class="hidden-xs" width="1311" height="680">
                            <img id="imgHomeSec4Right-mobile" src="img/mobile-build-3.png" alt="" class="visible-xs" width="1209" height="908">
                            
                            <div id="divHeroTxt" class="text-center wow fadeInLeft animated" data-wow-duration="1000ms" data-wow-delay="200ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 200ms; animation-name: fadeInLeft;">
                                 <div>
                                    
                                 <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Make</span> 
                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 ">A&nbsp;</span> 
                                     <span class="clsHeaderBigFont  clsHeaderItalicFont clsHeader1  ">Statement</span> 
                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 ">IN</span> 
                                    <span class="clsHeaderBigFont clsHeaderNormalFont clsHeader1">LIFE </span>
                                     
                                </div>
                                
                              
                                <div id="divHeroTxt2">
                                    <p>Panoramic views of the city skyline. <br> Coveted location in Ho Chi Minh City’s prized District 1.<br>Exclusive residences complete with artfully-designed interiors.</p>
                                    <p>There is only one name that sets the standard of luxury in Ho Chi Minh City. <br><b>D1MENSION</b>.</p>
                                </div>
                                
                                <!-- <div id="divTop2LogoMobile" class="visible-xs">
                
                                    <img id="logosMobile" src="img/logos.png" alt="capitaland logo" width="379" height="161">

                                </div> -->
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
             
                
             
                

              
                
            </section>

            <section id="secIntroPgPara" class="parllaxpadding">
            <div class="container ">
                
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 text-center whitebg">
                    
                        <img src="img/spareimage.png" id="imgIntroMid" alt="" class="img-responsive" style="max-width:550px; margin:0 auto; width:100%;">
                    </div>
                
                </div>
                <div class="row clsPurple2">
                    <div class="col-md-12 col-xs-12 whitebg">
                        
                        <div id="divIntroPgParaWrap">
                            <p style="font-family: Asap-Regular;text-align: justify;">
                                Located in the prestigious District 1 of Ho Chi Minh City, Vietnam, D1MENSION is brought to you by reputable Singapore-listed property developer, CapitaLand. Meeting the need for a true luxury development in a flourishing real estate market, D1MENSION is well placed in terms of product positioning and offering.
                            </p>
                            <p style="font-family: Asap-Regular;text-align: justify;">
                                Managed by The Ascott Limited, the world’s largest international serviced residence owner-operator, the development comprises two high rise towers, D1MENSION and Somerset, with 102 exclusive residential units in D1MENSION and 200 serviced apartments in Somerset, both complete with concierge services.

                            </p>
                            <p style="font-family: Asap-Regular;">At D1MENSION, quality of life is never compromised.</p>
                        </div>
                
                        
                    </div>
                </div>
                
               
            
            
            </div>
        
        
        
        
        </section>
         <!-- facitities plan -->
            <!-- POPUP VN -->
          <!--   <div class="modal fade modal-js-vn" id="myModal-js-vn">
                <div class="modal-dialog w800 fix_800">
                    <div class="modal-content fixmodal">
                       
                        <div class="modal-body">
                            <button type="button" id="close_pupop" class="close" data-dismiss="modal">&times;</button>
                            <div class="img_tks">
                                <img src="img/English_1.png" alt="">
                            </div>
                            <div class="text-center"></div>
                        </div>
                       
                    </div>
                 </div>
            </div>  -->