
<div class="wrapper facilities">  <!-- Main Logo -->
  <!-- Main Navigation -->
  <div class="nav-overlay"></div>
  <nav class="nav-wrapper">
    <div class="mainMenu d-flex align-items-center" initW="">
      <ul class="pl-5">
        <li><a href="./">home</a></li>
        <li><a href="location.html">location</a></li>
        <li><a href="facilities.html">facilities</a></li>
        <li class="dropdown"><a class="dropdown-toggle">floor plans</a>
          <ul>
            <li><a href="alto-floor-plans.html">Alto</a></li>
            <li><a href="cello-floor-plans.html">Cello</a></li>
          </ul>
        </li>
        <li><a href="gallery.html">gallery</a></li>
        <li><a href="media.html">media</a></li>
        <li><a href="contact.html">contact</a></li>
        <li><a href="vn/en/contact-us.html" target="_blank">registration</a></li>
        <li><a href="promotions.html">promotions</a></li>
      </ul>
      <a class="tnc pl-5" href="#" data-toggle="modal" data-target="#modalTerms">Terms &amp; Conditions</a>
    </div>
  </nav>
  <div class="bg-fixed-m"></div>
  <!-- Section - Facilities -->
  <section class="section section-auto facilities" id="facilities" data-section-name="facilities">
    <div class="elements">
      <div class="element element-5"><img class="img-fluid" src="images/element-05.png"></div>
    </div>
    <article class="d-flex flex-wrap">

      <!-- Header -->
      <div class="col-12 col-sm-12 pl-sm-5 justify-content-center">
          <!-- Headline -->
          <div class="row">
            <h4 class="headline col pt-3">
              <span class="ml-0">
                <small>FACILITIES</small>
                <div class="fac-gf">GROUND FLOOR</div>
                <div class="fac-14th fac-hide">14TH FLOOR &amp; ROOFTOP</div>
              </span>
            </h4>
          </div>
          <!-- Floor Selector -->
          <div class="row mb-5">
            <div class="col text-uppercase fac-nav" data-fac-nav>
              <a href="#" class="fac-nav-gf active">Ground</a>
              &nbsp;/&nbsp;
              <a href="#" class="fac-nav-14">14th &amp; 15th Floor</a>
            </div>
          </div>
      </div>
      <!-- Facilities Plan -->
     <div class="col-12 col-lg-7 pb-5 pb-sm-0 order-md-2" style=" position: relative; top: -141px; ">
       <!-- Ground Gloor Plan -->
        <div id="" class="clsHeader1 loclabel" style="text-align:center;">
            <span style="color: black;" class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1 clsHeaderBigFont_vn">Tiện ích</span> 
        </div>
        <!-- 14th Floor Plan -->
        <div class="mod_location_slider" id="mod_location_slider1" style="height: 591px;">
                    
            <div class="mod_location_slider_box">
        
                
                <div class="mod_location_slider_item" style="width: 700px; display: none;">
                    <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn">Lối vào chính</span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular;">Được phủ lên một màu xanh tươi non tự nhiên, lối vào của dự án tạo cho cư dân cùng khách ghé thăm cảm giác thư thái từ những bước chân đầu tiên. Để sau đó là ấn tượng choáng ngợp bởi điểm nhấn kiến trúc độc đáo, thể hiện thông qua mái vòm, 
                            những đường cong mềm mại cùng các cột trụ lớn cứng cáp. </p> 
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_001.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                        
                        <div id="" class="clsBarInner zoomMap_facilities1">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem 
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                    
                </div><!-- END mod_location_slider_item -->
                
                
                <div class="mod_location_slider_item" style="width: 700px; display: none;">
                    <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn">Tổ hợp bể bơi và sân tiệc</span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular;color: black;">Trang hoàng bằng những biểu tượng chạm khắc ánh kim, tổ hợp bể bơi và sân tiệc tại tầng trệt của D1mension trở thành không gian hoàn hảo cho những cuộc vui. Mang hình bóng của những hồ bơi La Mã xưa kia, nơi đây được tô điểm với khuôn mẫu kiến trúc cổ điển và tinh xảo.</p>
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_002.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                        
                        <div id="" class="clsBarInner zoomMap_facilities2">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                   
                </div><!-- END mod_location_slider_item -->
                
                <div class="mod_location_slider_item" style="width: 700px; display: none;">
                    <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn"> Sảnh Vòm Đón Khách</span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular; color: black;">Sảnh vòm vốn luôn là một điểm đặc trưng của các tòa tháp, lâu đài, dinh thự lớn từ xa xưa, đóng vai trò như lối ra vào chính của công trình. Điểm nhấn này được tái hiện tại D1MENSION, đem tới trải nghiệm xa hoa bậc nhất cho cư dân.</p> 
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_003.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                        
                        <div id="" class="clsBarInner zoomMap_facilities3">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem 
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                   
                </div><!-- END mod_location_slider_item -->
                
                
                <div class="mod_location_slider_item" style="width: 700px; display: none;">
                    <div class="desc" style="transform: translate(-50px, 0px); opacity: 0;">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn">Phòng tập thể dục trên không</span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular; color: black;">Nằm trên tầng 15 của tòa tháp và sở hữu tầm nhìn toàn cảnh thành phố tráng lệ, phòng tập trên không được trang bị hàng loạt máy tập tối tân. Khi được tích hợp kèm thêm phòng thay đồ và xông hơi, 
                            nơi đây trở thành một tổ hợp rèn luyện đẳng cấp, xa hoa cho cư dân của D1mension.</p> 
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_004.jpg" alt="location_location_pic1" style="transform: translate(-50px, 0px); opacity: 0;">
                        
                        <div id="" class="clsBarInner zoomMap_facilities4">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                    
                </div><!-- END mod_location_slider_item -->
                
                
                <div class="mod_location_slider_item active" style="display: block; width: 700px;">
                    <div class="desc" style="transform: translate(0px, 0px); opacity: 1;">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn">Hồ bơi sàn kính trên không </span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular;color: black;">Sảng khoái cảm nhận làn nước mát với khung cảnh hoa lệ của cả thành phố thu gọn trong tầm mắt, đó là trải nghiệm tuyệt vời mà hồ bơi sàn kính trên không mang lại. Nằm trên tầng 16, đây sẽ là tiện ích độc đáo tạo nên sự hứng khởi và thư giãn tuyệt vời cho cư dân mỗi ngày.</p> 
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_005.jpg" alt="location_location_pic1" style="transform: translate(0px, 0px); opacity: 1;">
                        
                        <div id="" class="clsBarInner zoomMap_facilities5">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                   
                </div><!-- END mod_location_slider_item -->
                
                <div class="mod_location_slider_item" style="display: none;">
                    <div class="desc">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn">Sảnh tiệc trên không</span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular; color: black;">Là một khu vực rộng lớn xa hoa gồm sảnh thư giãn, phòng dùng bữa, quầy rượu và cigar. Với thiết kế sang trọng và tinh tế, đây là nơi cư dân cùng gia đình có thể tổ chức những sự kiện quan trọng hoặc đơn thuần tận hưởng khoảnh khắc riêng mình.</p>
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_006.jpg" alt="location_location_pic1">
                        
                        <div id="" class="clsBarInner zoomMap_facilities6">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                    
                </div><!-- END mod_location_slider_item -->
                
                
                <div class="mod_location_slider_item" style="display: none;">
                    <div class="desc">
                    
                        <!-- header -->
                          <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                                  
                                  <div class="groupheader">
                                      <div id="">
                                          <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1 clsHeaderSmallFont_vn">Công viên giải trí</span> 
                                      </div>
                                  </div>
                                  
                              </div>  
                          <!-- end header -->
                        
                        <p style="font-family: Asap-Regular;color: black;">Rạng rỡ trong sắc xanh cỏ cây, khu vực công viên giải trí sẽ là nơi các em nhỏ vui chơi thỏa thích. Khám phá thế giới tự nhiên thu nhỏ hay đơn thuần kết thân với bè bạn, nơi đây thực sự lý tưởng cho các em phát triển thể chất và tâm hồn.</p> 
                        
                        </div>
                    <div class="thumb">
                    
                        <img src="img/img_location/Facilities_007.jpg" alt="location_location_pic1">
                        
                        <div id="" class="clsBarInner zoomMap_facilities7">
                            <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                <a>Click để xem
                                <span class="glyphicon glyphicon-menu-right   wow shake animated" style="float: right; visibility: visible; animation-duration: 2000ms; animation-delay: 200ms; animation-name: shake;" data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                            </div>
                            <div class="clear"></div>
                        </div>  
                          
                    </div>
                    
                   
                </div><!-- END mod_location_slider_item -->
                
        
        
                </div><!-- END mod_location_slider_box-->
                
        <!-- Controls -->
        <style>
        .controlx_btn img{width:70%!important; height:auto;}
        .controlx_btn{width:5%; text-align:center;}
        .controlx_btn:before{content:normal!important;}
        </style>
              <div class="controlx" style="width: 700px;">
                  <div class="controlx_box">
                  <div class="controlx_btn"><img src="msd_assets/plugins/slideshow/img/slider_arrowl.png" alt=""></div>
                  <div class="controlx_btn"><img src="msd_assets/plugins/slideshow/img/slider_arrowr.png" alt=""></div>
                  <div class="clear"></div>
                </div>
              </div>
                
              <div class="paginax" style="width: 700px;"><div class="paginax_box"><span class=""></span><span class=""></span><span class=""></span><span class=""></span><span class="activex"></span><span></span><span></span></div></div></div>
      </div>
      <!-- Facilities List -->
      <div class="d-flex col-12 col-lg-4 pl-sm-5 justify-content-center order-md-1" style="color: black;">
        <div class="article-inner col-12 text-left pt-0" >
          <!-- Ground Floor List -->
          <div class="facilities-list fac-gf">
            <h4><div class="fac-dot-indicate fac-black d-inline-block"></div>Outdoor Facilities</h4>
            <ul class="row col-12">
              <!-- Outdoor Facilities -->
              <div class="col-12 col-sm-6">
                <li>Cadenza Drop-off</li>
                <li>Tempo Plaza</li>
                <li>Parking Lot</li>
                <li>Guardhouse</li>
                <li>Security Gate</li>
                <li>Street Art Wall</li>
                <li>Ensemble Canopy</li>
                <li>Piano Welcome Garden</li>
                <li>Grand Welcome Deck</li>
                <li>Swing Canopy</li>
                <li>Relaxing Hammocks</li>
                <li>Monkey Bars</li>
                <li>Green Multi-purpose Area</li>
                <li>Skate Plaza</li>
                <li>Outdoor Working Space</li>
                <li>Lap Pool 50m</li>
                </div>
              <div class="col-12 col-sm-6">
                <li>Jacuzzi</li>
                <li>Pool Deck</li>
                <li>Pool Bar</li>
                <li>Party Pool</li>
                <li>Cinema Pool</li>
                <li>Party Deck</li>
                <li>Kid’s Pool</li>
                <li>Kid’s Slide Tube</li>
                <li>Musical Play Park</li>
                <li>Outdoor Shower</li>
                <li>Picnic Lawn</li>
                <li>Hip Hop Plaza</li>
                <li>Chimes Welcome Garden</li>
                <li>Guitar Welcome Garden</li>
                <li>Acapella Drop-off</li>
                </div>

              <!-- Break -->
              <div class="w-100">
                <h4><div class="fac-dot-indicate fac-orange d-inline-block"></div>Indoor Facilities</h4>
              </div>

              <!-- Indoor Facilities -->
              <div class="col-12 col-sm-6">
                <li>Alto Lobby</li>
                <li>Bass Lobby</li>
                <li>Cello Lobby</li>
                <li>High Jump Arena</li>
                <li>The Beat Box Gym</li>
                <li>Maestro Junior Room</li>
                <li>Multimedia Room</li>
                <li>Press Game Room</li>
                <li>Sync Space (Co-Working)</li>
                </div>

            </ul>
          </div>
          <!-- 14th Floor & Rooftop List -->
          <div class="facilities-list fac-14th fac-hide">
            <ul class="row col-12">

              <!-- 14th Floor -->
              <div class="col-6">
                <h4>14th Floor</h4>
                <!-- 14th Floor: Indoor Facilities -->
                <h5><div class="fac-dot-indicate fac-magenta d-inline-block"></div>Indoor Facilities</h5>
                <li>Changing Room</li>
                <li>Party House</li>
                <!-- 14th Floor: Outdoor Facilities -->
                <h5><div class="fac-dot-indicate fac-cyan d-inline-block"></div>Outdoor Facilities</h5>
                <li>Celestial Garden</li>
                <li>Symphony Stairs</li>
                <li>Sky Theatre</li>
                </div>

              <!-- Rooftop -->
              <div class="col-6">
                <h4>Rooftop</h4>
                <!-- Rooftop: Outdoor Facilities -->
                <h5><div class="fac-dot-indicate fac-cyan d-inline-block"></div>Outdoor Facilities</h5>
                <li>Jazz Pool</li>
                <li>Sky Jacuzzi</li>
                <li>Sweet Daybed</li>
                <li>Jazz Terrace</li>
                <li>Sun Bed</li>
                <li>Tea Pavilion</li>
                <li>Outdoor Shower</li>
                <li>Pool Cabana</li>
                <li>Duet Hammock</li>
                <li>Vertical Green</li>
                <li>Tranquil Area</li>
                </div>

            </ul>
          </div>
        </div>
      </div>

    </article>

    <!-- Disclaimer -->
    <div class="container">
      <div class="row mt-5 mb-5">
        <div class="col">
          <p style="color: black;">
            <small><strong>Disclaimer:</strong> All information contained herein is subject to changes without notification as may be required by the relevant authorities and cannot form part of an offer or contract. Whilst every care has been taken in providing this information, the owner, developer and managers cannot be held liable for variations. All illustrations and pictures are artist impressions only. The items are subject to variation, modifications and substitutions as may be recommended by the relevant Approving Authorities.</small>
          </p>
        </div>
      </div>
    </div>

  </section>

  <!-- Facilities Plan Popup (Ground Floor) -->
  <div id="fac-plan-modal-gf" class="fac-plan-modal modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <!-- Close Button -->
        <button type="button" class="fac-plan-modal__close close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <!-- Plan -->
        <div class="fac-plan-modal__body modal-body">
          <div class="fac-plan-modal__plan">

            <!-- Plan Image -->
            <img class="img-fluid" src="images/facilities-gf.png" alt="Facilities">

            <!-- Counters -->
            <ul class="fac-gf fac-nolist">
              <li class="fac-no fac-01"></li>
              <li class="fac-no fac-02"></li>
              <li class="fac-no fac-03 fac-03-1"></li>
              <li class="fac-no fac-03 fac-03-2"></li>
              <li class="fac-no fac-03 fac-03-3"></li>
              <li class="fac-no fac-03 fac-03-4"></li>
              <li class="fac-no fac-03 fac-03-5"></li>
              <li class="fac-no fac-03 fac-03-6"></li>
              <li class="fac-no fac-04 fac-04-1"></li>
              <li class="fac-no fac-04 fac-04-2"></li>
              <li class="fac-no fac-05 fac-05-1"></li>
              <li class="fac-no fac-05 fac-05-2"></li>
              <li class="fac-no fac-05 fac-05-3"></li>
              <li class="fac-no fac-05 fac-05-4"></li>
              <li class="fac-no fac-06"></li>
              <li class="fac-no fac-07"></li>
              <li class="fac-no fac-08"></li>
              <li class="fac-no fac-09"></li>
              <li class="fac-no fac-10"></li>
              <li class="fac-no fac-11"></li>
              <li class="fac-no fac-12"></li>
              <li class="fac-no fac-13"></li>
              <li class="fac-no fac-14"></li>
              <li class="fac-no fac-15"></li>
              <li class="fac-no fac-16"></li>
              <li class="fac-no fac-17"></li>
              <li class="fac-no fac-18 fac-18-1"></li>
              <li class="fac-no fac-18 fac-18-2"></li>
              <li class="fac-no fac-18 fac-18-3"></li>
              <li class="fac-no fac-19"></li>
              <li class="fac-no fac-20"></li>
              <li class="fac-no fac-21"></li>
              <li class="fac-no fac-22"></li>
              <li class="fac-no fac-23"></li>
              <li class="fac-no fac-24"></li>
              <li class="fac-no fac-25"></li>
              <li class="fac-no fac-26"></li>
              <li class="fac-no fac-27"></li>
              <li class="fac-no fac-28"></li>
              <li class="fac-no fac-29"></li>
              <li class="fac-no fac-30"></li>
              <li class="fac-no fac-31"></li>
              <li class="fac-no fac-32 fac-32-1 fac-orange"></li>
              <li class="fac-no fac-32 fac-32-2 fac-orange"></li>
              <li class="fac-no fac-33 fac-orange"></li>
              <li class="fac-no fac-34 fac-orange"></li>
              <li class="fac-no fac-35 fac-orange"></li>
              <li class="fac-no fac-36 fac-orange"></li>
              <li class="fac-no fac-37 fac-orange"></li>
              <li class="fac-no fac-38 fac-orange"></li>
              <li class="fac-no fac-39 fac-orange"></li>
              <li class="fac-no fac-40 fac-orange"></li>
            </ul>

          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- Facilities Plan Popup (Ground Floor) -->
  <div id="fac-plan-modal-14th" class="fac-plan-modal modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <!-- Close Button -->
        <button type="button" class="fac-plan-modal__close close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <!-- Plan -->
        <div class="fac-plan-modal__body modal-body">
          <div class="fac-plan-modal__plan">

            <!-- Plan Image -->
            <img class="img-fluid" src="images/facilities-14th.png" alt="Facilities">

            <!-- Counters -->
            <ul class="fac-14th fac-nolist">
              <li class="fac-no fac-01 fac-magenta"></li>
              <li class="fac-no fac-02 fac-magenta"></li>
              <li class="fac-no fac-03"></li>
              <li class="fac-no fac-04"></li>
              <li class="fac-no fac-05"></li>
              <li class="fac-no fac-06"></li>
              <li class="fac-no fac-07"></li>
              <li class="fac-no fac-08"></li>
              <li class="fac-no fac-09"></li>
              <li class="fac-no fac-10"></li>
              <li class="fac-no fac-11"></li>
              <li class="fac-no fac-12"></li>
              <li class="fac-no fac-13"></li>
              <li class="fac-no fac-14"></li>
              <li class="fac-no fac-15 fac-15-1"></li>
              <li class="fac-no fac-15 fac-15-2"></li>
              <li class="fac-no fac-15 fac-15-3"></li>
              <li class="fac-no fac-15 fac-15-4"></li>
              <li class="fac-no fac-16"></li>
            </ul>

          </div>
        </div>

      </div>
    </div>
  </div>

</div>
