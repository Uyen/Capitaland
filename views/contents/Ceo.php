 <div id="IntroBarOuter" class="clsBarOuter wow fadeInUp" data-wow-duration="500ms" data-wow-delay="100ms" style="padding-top: 24px;" >
                    <div id="IntroBarInner" class="clsBarInner">
                        <a href="introduction-vn.php">
                            <div class=" col-xs-12 text-center clsNormalFont">
                                <span class="text-uppercase">Letter CEO/ The Ascott</span>
                                <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span>
                            </div>
                            <div class="clear"></div>
                        </a>
                    </div>
                </div>
<section class="intro-slider" id="secIntroSlider" style="margin-top: 25px;">
            <div class="container-fluid nopadding">
            
                  <!-- MSD SLIDER 1 -->
                  <div id="slider1" class="msd_slider" style="height: 425px;">
      
                      <div class="msd_slider_colset" style="display: block;">
                  
                          <div class="msd_slider_col" style="transform: translate(0px, 0px); opacity: 1;"><div class="msd_slider_col_box"><img id="imgCeo" src="img/d1_ceo-vn.png" alt="" width="732" height="508"></div></div><div class="msd_slider_col" style="transform: translate(0px, 0px); opacity: 1;"><div class="msd_slider_col_box">
                      
                      
                        <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                            
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase clsHeaderBigFont_vn">Thư ngỏ từ</span> 
                                    
                                </div>

                                <div id="divIntroSlideHeadTxt2">
                                    
                                    <span class="clsHeaderBigFont clsHeaderNormalFontViet clsHeader1 text-uppercase clsHeaderBigFont_vn">CapitaLand </span>
                                </div>
                            </div>
                            <div class=" clsPurple2">
                                <p class="text-center" style="font-family: Asap-Regular;">
                                    Tôi cảm thấy vô cùng tự hào khi chính thức giới thiệu tới các bạn D1MENSION, dự án Branded Residence (Được thương hiệu quốc tế quản lý sau khi hoàn thành) đầu tiên tại Quận 1 TP. Hồ Chí Minh, phát triển bởi Tập đoàn phát triển Bất Động Sản Quốc tế CapitaLand, 
                                    và được quản lý bởi tập toàn quản lý căn hộ dịch vụ cho thuê cao cấp hàng đầu thế giới – The Ascott Limited. Từ phong cách sống đỉnh cao cho tới những tuyệt tác nội thất, CapitaLand mang niềm tin sâu sắc rằng D1MENSION sẽ là nơi kiến tạo nên một dấu ấn thượng lưu mới.
                                    
                                    
                                </p>
                                <p class="text-center" style="font-family: Asap-Regular;"><a href="messagefromcapitaland-vn.php">Đọc Thêm</a></p>
                            </div>  
                        </div>  
                        
                                          
                      
                      </div></div>
                      
                      </div><!-- end msd_slider_colset -->
                      
                      <div class="msd_slider_colset active" style="display: block;">
                  
                          <div class="msd_slider_col" style="transform: translate(0px, 0px); opacity: 1;"><div class="msd_slider_col_box"><img id="imgAscott" src="img/ascott_cgm-vn.png" alt="" width="732" height="508"></div></div><div class="msd_slider_col" style="transform: translate(0px, 0px); opacity: 1;"><div class="msd_slider_col_box">
                      
                        <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                             
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1_2" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase clsHeaderBigFont_vn">Thư ngỏ từ</span> 
                                    
                                </div>

                                <div id="divIntroSlideHeadTxt2_2">
                                    <span class="clsHeaderBigFont clsHeaderNormalFontViet clsHeader1  text-uppercase clsHeaderBigFont_vn">The Ascott Limited</span>
                                   
                                </div>
                            </div>
                            <div class="text-center clsPurple2">
                                <p class="text-center" style="font-family: Asap-Regular;">
                                    Đại diện cho The Ascott Limited, chúng tôi rất đỗi tự hào khi được chỉ định trở thành đơn vị quản lý cho D1MENSION - dự án danh giá của CapitaLand, đồng thời cũng là đơn vị trực tiếp vận hành Somerset D1Mension Hồ Chí Minh 
                                    - tháp căn hộ dịch vụ nằm trong tổng thể dự án. Từ chuẩn mực đỉnh cao trong các tổ hợp tiện ích, cho tới sự nhiệt thành của đội ngũ nhân viên, chúng tôi chào đón bạn tới với cuộc sống hoàn hảo tại D1MENSION.

                                </p>
                                <p class="text-center" style="font-family: Asap-Regular;"><a href="messagefromascott-vn.php">Đọc Thêm</a></p>
                            </div>
                        
                        </div>
                 
                      
                      </div></div>
                      
                      </div><!-- end msd_slider_colset -->
                      
                      <div class="clear"></div>
                      
                      <!-- Controls -->
                      <div class="controlx" style="top: 215px;"><div class="controlx_btn"></div><div class="controlx_btn"></div><div class="clear"></div><div class="blocker" style="display: none;"></div></div>
                  
                  </div><!-- end msd_slider -->
                  
                  <div id="sliderdata1" class="msd_slider_data">
                  
                    <!-- Slider 1 Data 1 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                      <div class="data_a"><img id="imgCeo" src="img/d1_ceo-vn.png" alt="" width="732" height="508"></div>
                      <div class="data_b">
                      
                      
                        <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                            
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase clsHeaderBigFont_vn">Thư ngỏ từ</span> 
                                    
                                </div>

                                <div id="divIntroSlideHeadTxt2">
                                    
                                    <span class="clsHeaderBigFont clsHeaderNormalFontViet clsHeader1 text-uppercase clsHeaderBigFont_vn">CapitaLand </span>
                                </div>
                            </div>
                            <div class=" clsPurple2">
                                <p class="text-center" style="font-family: Asap-Regular;">
                                    Tôi cảm thấy vô cùng tự hào khi chính thức giới thiệu tới các bạn D1MENSION, dự án Branded Residence (Được thương hiệu quốc tế quản lý sau khi hoàn thành) đầu tiên tại Quận 1 TP. Hồ Chí Minh, phát triển bởi Tập đoàn phát triển Bất Động Sản Quốc tế CapitaLand, 
                                    và được quản lý bởi tập toàn quản lý căn hộ dịch vụ cho thuê cao cấp hàng đầu thế giới – The Ascott Limited. Từ phong cách sống đỉnh cao cho tới những tuyệt tác nội thất, CapitaLand mang niềm tin sâu sắc rằng D1MENSION sẽ là nơi kiến tạo nên một dấu ấn thượng lưu mới.
                                    
                                    
                                </p>
                                <p class="text-center" style="font-family: Asap-Regular;"><a href="messagefromcapitaland-vn.php">Đọc Thêm</a></p>
                            </div>  
                        </div>  
                        
                                          
                      
                      </div><!-- end data_b -->
                      
                      <!-- Slider 1 Data 2 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                      <div class="data_a"><img id="imgAscott" src="img/ascott_cgm-vn.png" alt="" width="732" height="508"></div>
                      <div class="data_b">
                      
                        <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                             
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1_2" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase clsHeaderBigFont_vn">Thư ngỏ từ</span> 
                                    
                                </div>

                                <div id="divIntroSlideHeadTxt2_2">
                                    <span class="clsHeaderBigFont clsHeaderNormalFontViet clsHeader1  text-uppercase clsHeaderBigFont_vn">The Ascott Limited</span>
                                   
                                </div>
                            </div>
                            <div class="text-center clsPurple2">
                                <p class="text-center" style="font-family: Asap-Regular;">
                                    Đại diện cho The Ascott Limited, chúng tôi rất đỗi tự hào khi được chỉ định trở thành đơn vị quản lý cho D1MENSION - dự án danh giá của CapitaLand, đồng thời cũng là đơn vị trực tiếp vận hành Somerset D1Mension Hồ Chí Minh 
                                    - tháp căn hộ dịch vụ nằm trong tổng thể dự án. Từ chuẩn mực đỉnh cao trong các tổ hợp tiện ích, cho tới sự nhiệt thành của đội ngũ nhân viên, chúng tôi chào đón bạn tới với cuộc sống hoàn hảo tại D1MENSION.

                                </p>
                                <p class="text-center" style="font-family: Asap-Regular;"><a href="messagefromascott-vn.php">Đọc Thêm</a></p>
                            </div>
                        
                        </div>
                 
                      
                      </div><!-- end data_b -->
                      
                  </div><!-- MSD SLIDER 1 END -->
                  
               

                
        
            </div>
        </section>
        <section class="" id="SecVideo" style="background:transparent;">
            <div class="container-fluid">
                <div class="row ">
                    <div class="col-md-12 text-center nopadding">
                        <!--<div class="videopop" style="color:#000;">View the video</div>-->
                        
                        <a href="http://www.youtube.com/embed/xsvlYmJT6wA?autoplay=1&amp;wmode=opaque&amp;fs=1" class="youtube-media">
                            <img class="hidden-xs" id="imgVideo" src="img/video_viet.jpg" alt="" width="1366" height="558">
                            <img class="visible-xs" id="imgVideo2" src="img/video-mobile.jpg" width="892" height="558" alt="">
                            </a>
                        
                    </div>
                </div>
            </div>
        </section>