<section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding ">
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2 hidden-xs" data-parallax="scroll" data-image-src="img/hero.jpg"> 

                    <!-- <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div> -->
                </div>
                
                <img id="imgIntroKeyVis-mobile" src="img/intro-mobile.jpg" width="1209" height="908" alt="" class="visible-xs">
                
                <div id="divPgKeyOuterTxtWrap">
                
                    <div id="divIntroPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap">
                        
                        
                         <div id="divIntroPgKeyVisHeadTxt1" class="clsHeader1">
                            
                            <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 clsHeaderBigFontTitleCase clsHeaderBigFont_vn">Mê Đắm</span> 
                            
                        </div>

                        <div id="divIntroPgKeyVisHeadTxt2VN">
                            <span class="clsHeaderSmallFont clsHeaderNormalFontViet clsHeader1 text-uppercase clsHeaderSmallFont_vn">mọi</span> 
                            
                            <span class="clsHeaderBigFont clsHeaderNormalFontViet clsHeader1 text-uppercase  clsHeaderBigFont_vn">Giác Quan</span>
                        </div>
                        


                        
                    </div>

                </div>
            </div>    
           

        </section>



<section id="secIntroPgPara" class="parllaxpadding">
            <div class="container ">
                
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 text-center whitebg">
                    
                        <img src="img/spareimage.png" id="imgIntroMid" alt="" class="img-responsive" style="max-width:550px; margin:0 auto; width:100%;">
                    </div>
                
                </div>
                <div class="row clsPurple2">
                    <div class="col-md-12 col-xs-12 whitebg">
                        
                        <div id="divIntroPgParaWrap">
                            <p style="font-family: Asap-Regular;">
                                Nằm tại quận 1 – trung tâm phồn hoa của thành phố Hồ Chí Minh, D1MENSION được phát triển bởi chủ đầu tư Singapore danh giá: CapitaLand. Mang tầm vóc của một công trình biểu tượng, dự án được định vị sẽ trở thành một hình mẫu chuẩn mực mới.
                            </p>
                            <p style="font-family: Asap-Regular;">
                                Được quản lý bởi The Ascott Limited, tập đoàn quản lý căn hộ dịch vụ lớn nhất thế giới, dự án bao gồm hai tòa tháp, D1MENSION và Somerset, với 102 căn hộ độc nhất dành cho cư dân và 200 căn hộ dịch vụ sở hữu những dịch vụ hoàn hảo và mang bản sắc riêng.

                            </p>
                            <p style="font-family: Asap-Regular;">Tại D1MENSION, mọi giới hạn về một cuộc sống đẳng cấp sẽ được định nghĩa lại.</p>
                        </div>
                
                        
                    </div>
                </div>
                
               
            
            
            </div>
        
        
        
        
        </section>
        <div class="modal fade modal-js-vn" id="myModal-js-vn">
            <div class="modal-dialog w800 fix_800">
                <div class="modal-content fixmodal">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <button type="button" id="close_pupop" class="close" data-dismiss="modal">&times;</button>
                        <div class="img_tks">
                            <img src="img/Vietnamese_3.png" alt="">
                        </div>
                        <div class="text-center"></div>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div> 