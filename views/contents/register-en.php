<section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding ">
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/hero2.jpg"> 
                    <!-- <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div> -->
                   
                </div>
                
                <img id="imgRegKeyVis-mobile" src="img/register-mobile.jpg" width="1209" height="908" alt="" class="visible-xs">
                
                <div id="divPgKeyOuterTxtWrap">
                
                    <div id="divRegPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap">

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                            <div id="divRegPgKeyVisHeadTxt1">
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont text-left">Setting </span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont text-left">THE </span> 
                            </div>

                            <div id="divRegPgKeyVisHeadTxt2">

                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont  ">Standard</span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont">IN&nbsp;</span> 
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont">ASIA </span>
                            </div>
                        </div>
                       

                    </div>

                </div>
            </div>    
           

        </section>
        <section id="secRegForm" class="parllaxpadding">
                <div class="container whitebg" id="secRegFormContainer">
                    <div class="row">
                        <form class="well form-horizontal  bv-form w-100 text-center" action="registersend.php" method="post" id="contact_form" accept-charset="UTF-8" novalidate="novalidate" style="color: black">
                            <p>Contact 𝟏𝟖𝟎𝟎 𝟓𝟗𝟗 𝟗𝟖𝟔 to schedule for a private tour visit today.</p>
                            <p>For other questions & enquiries, kindly contact via:</p>
                             
                            <p> 
                                <b>D1MENSION Facebook Page:</b> 
                                    <a href="https://www.facebook.com/D1MENSION.MakeAStatementInLife/"  target="blank" style="color: #000;">
                                        https://www.facebook.com/D1MENSION.MakeAStatementInLife/
                                    </a>
                            </p>

                            <p>
                                <b> CapitaLand Website:</b> 
                                <a href="https://www.capitaland.com/vn/vi/contact-us.html"  target="blank" style="color: #000;">
                                    https://www.capitaland.com/vn/vi/contact-us.html
                                </a>
                            </p>
                         </form>
                         
                    </div>
                    
                     
                    
                    
                </div>
                
                
            </section>