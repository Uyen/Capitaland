<section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding ">
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/hero2.jpg"> 
                    <!-- <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div> -->
                   
                </div>
                
                <img id="imgRegKeyVis-mobile" src="img/register-mobile.jpg" width="1209" height="908" alt="" class="visible-xs">
                
                <div id="divPgKeyOuterTxtWrap">
                
                    <div id="divRegPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap">

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                            <div id="divRegPgKeyVisHeadTxt1" >
                                
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet text-left">Kiến tạo</span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderNormalFontViet text-left text-uppercase">Chuẩn Mực Mới</span> 
                                
                            </div>

                            <div id="divRegPgKeyVisHeadTxt2-">

                            
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFontViet"> tại&nbsp;</span> 
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">CHÂU Á</span>
                                
                                
                            </div>
                        </div>
                       

                    </div>

                </div>
            </div>    
           

        </section>
        <section id="secRegForm" class="parllaxpadding">
            <div class="container whitebg" id="secRegFormContainer">
                <div class="row">
                    <form class="well form-horizontal  bv-form w-100 text-center" action="registersend.php" method="post" id="contact_form" accept-charset="UTF-8" novalidate="novalidate" style="color: black">
                        <p>Liên hệ 𝟏𝟖𝟎𝟎 𝟓𝟗𝟗 𝟗𝟖𝟔 để đặt hẹn tham quan và khám phá những căn hộ mẫu ngay hôm nay.</p>

                    <p>Để tư vấn thêm thông tin, vui lòng liên hệ tại:</p>
                         
                        <p> 
                            <b> Trang Facebook dự án D1MENSION: </b> 
                                <a href="https://www.facebook.com/D1MENSION.MakeAStatementInLife/"  target="blank" style="color: #000;">
                                  https://www.facebook.com/D1MENSION.MakeAStatementInLife/
                                </a>
                        </p>

                        <p>
                            <b>Website CapitaLand:</b> 
                            <a href="https://www.capitaland.com/vn/vi/contact-us.html"  target="blank" style="color: #000;">
                                 https://www.capitaland.com/vn/vi/contact-us.html
                            </a>
                        </p>
                    </form>  
                </div>            
            </div>                        
        </section>