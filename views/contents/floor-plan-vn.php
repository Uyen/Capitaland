   <div id="main-navbar-cover"></div>
            
            <style>
            
             #divMenu{position: absolute;top:20px;z-index: 1;width:100%;padding-right:50px;font-family: 'Asap-Regular' !important;font-weight: normal;font-style: normal;}
             #divMenuInner{position: relative;width:200px;z-index: 100;}
            
                #divMenu ul{font-family: 'Asap-Regular' !important;font-weight: normal;font-style: normal;}
                
             #divTop2Logo{position: relative;padding-left: 40%;top:25px;}
             #logos{height: 90px;width: auto;padding-right: 20px;}
             #divTop2LogoMobile{position: relative;}
             #logosMobile{height: 80px;width:auto;}
                #imgHomeSec4Right-mobile{padding-top: 300px;}  
                
                
             </style>
            <div class="wrapper floorplans">  <!-- Main Logo -->
              <nav class="nav-logo">
                <a href="../en">
                  <!-- <img class="logo p-4" src="images/new/logo.svg" alt="De La Sol Logo"> -->
                </a>
              </nav>
            </div>
 
  
  <div class="bg-fixed-m"></div>
<section class="section section-auto floorplans" id="floorplans" data-section-name="floorplans">
    <article class="d-flex flex-wrap align-items-center">
      <div class="col-6 col-lg-3 order-1 order-md-1 pl-5 align-self-start">
        <div class="article-inner col-12 text-left">
          <img class="img-fluid" src="img/D1Logo.png">
          <h4 class="headline">
            <span class="ml-0">
              <small class="mt-2">MẶT BẰNG TẦNG /<br> MẶT BẰNG CĂN HỘ</small>
            </span>
          </h4>
          <div class="plans-nav">
            <ul>
              <li data-nav="plans-lv1" class="plans-lv1 active"><a>Tầng 2</a></li>
                 <li data-nav="plans-lv2" class="plans-lv2 "><a>Tầng 3 đến tầng 11</a></li>
              <li data-nav="plans-lv2-13" class="plans-lv2-13"><a>Tầng 12</a></li>
            <!--  <li data-nav="plans-lv14" class="plans-lv14"><a>Level 12</a></li> -->
              <li data-nav="plans-lv12a" class="plans-lv12a"><a>Tầng 12A đến Tầng 12B</a></li>
              <li data-nav="plans-lv15" class="plans-lv15"><a>Tầng 15</a></li>
              <li data-nav="plans-lv16" class="plans-lv16"><a>Tầng 16</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-8 order-3 order-md-2 pt-3 pt-md-0 align-items-center justify-content-center">
        <div class="plans-floorplans">
          <!-- Level 2 -->
          <div class="plans-lv1 plans-level active">
            <!-- <img class="img-fluid" src="images/new/lv1.png" usemap="#floor1" alt="floor1"> -->
            <img class="img-fluid" src="images/map/Vietnamese2.png" usemap="#floor1" alt="floor1">
          </div>
          <map name="floor1" id="floor1">
            <area alt="lvl2-3BR-4" id="lvl2-3BR-4" ata-tooltip="Unit No: A3-04<br>Unit Type: 3BR-4" href="#modal3BR-4" coords="181,1158,738,720" shape="rect">
            <area alt="lvl2-2BR-4A" id="lvl2-2BR-4A" ata-tooltip="Unit No: 2BR-4A<br>Unit Type: 2BR-4A" href="#modal2BR-4A" coords="741,1158,1198,720" shape="rect">
            <area alt="lvl2-2BR-1A" id="lvl2-2BR-1A" ata-tooltip="Unit No: 2BR-1A<br>Unit Type: 2BR-1A" href="#modal2BR-1A" coords="1531,1158,1978,720" shape="rect">
            <area alt="lvl2-3BR-1" id="lvl2-3BR-1" ata-tooltip="Unit No: 3BR-1<br>Unit Type: 3BR-1 Mirror" href="#modal3BR-1" coords="1982,1158,2528,720 " shape="rect">
            <area alt="lvl2-2BR-3A" id="lvl2-2BR-3A" ata-tooltip="Unit No: 2BR-3A<br>Unit Type: 2BR-3A Mirror" href="#modal2BR-3A" coords="270,1644,650,1220" shape="rect">
            <area alt="lvl2-3BR-3A" id="lvl2-3BR-3A" ata-tooltip="Unit No: 3BR-3A<br>Unit Type: 3BR-3A Mirror" href="#modal3BR-3A" coords="650,1644,1188,1220" shape="rect">
            <area alt="lvl2-3BR-2A" id="lvl2-3BR-2A" ata-tooltip="Unit No: 3BR-2A<br>Unit Type: 3BR-2A Mirror" href="#modal3BR-2A" coords="1520,1644,2068,1220" shape="rect">
            <area alt="lvl2-2BR-2A" id="lvl2-2BR-2A" ata-tooltip="Unit No: 2BR-2A<br>Unit Type: 2BR-2 Mirror" href="#modal2BR-2A" coords="2068,1644,2448,1220" shape="rect">
          </map>

          <!-- Level 3-11 -->
          <div class="plans-lv2 plans-level">
            <img class="img-fluid" src="images/map/Vietnamese3.png" usemap="#floor11" alt="floor11">
          </div>
          <map name="floor11" id="floor11">
            <area alt="lvl3-3BR-4" id="lvl3-3BR-4" ata-tooltip="Unit No: 3BR-4<br>Unit Type: 3BR-4" href="#modal3BR-4" coords="181,1158,740,720" shape="rect">
            <area alt="lvl3-2BR-4" id="lvl3-2BR-4" ata-tooltip="Unit No: 2BR-4<br>Unit Type: 2BR-4" href="#modal2BR-4" coords="741,1158,1190,720" shape="rect">
            <area alt="lvl3-2BR-1" id="lvl3-2BR-1" ata-tooltip="Unit No: 2BR-1<br>Unit Type: 2BR-1" href="#modal2BR-1" coords="1531,1158,1988,720" shape="rect">
            <area alt="lvl3-3BR-1" id="lvl3-3BR-1" ata-tooltip="Unit No: 3BR-1<br>Unit Type: 3BR-1 Mirror" href="#modal3BR-1" coords="1982,1158,2528,720 " shape="rect">
            <area alt="lvl3-2BR-3A" id="lvl3-2BR-3A" ata-tooltip="Unit No: 2BR-3A<br>Unit Type: 2BR-3A Mirror" href="#modal2BR-3A" coords="260,1644,650,1220" shape="rect">
            <area alt="lvl3-3BR-3" id="lvl3-3BR-3" ata-tooltip="Unit No: 3BR-3<br>Unit Type: 3BR-3 Mirror" href="#modal3BR-3" coords="640,1644,1190,1220" shape="rect">
            <area alt="lvl3-3BR-2" id="lvl3-3BR-2" ata-tooltip="Unit No: 3BR-2<br>Unit Type: 3BR-2 Mirror" href="#modal3BR-2" coords="1528,1644,2070,1220" shape="rect">
            <area alt="lvl3-2BR-2A" id="lvl3-2BR-2A" ata-tooltip="Unit No: 2BR-2A<br>Unit Type: 2BR-2A Mirror" href="#modal2BR-2A" coords="2070,1644,2460,1220" shape="rect">
          </map>

          <!-- Level 12 -->
          <div class="plans-lv2-13 plans-level">
            <img class="img-fluid" src="images/map/Vietnamese12.png" usemap="#floor12" alt="floor12">
          </div>
          <map name="floor12" id="floor12">
            <area alt="lvl12-3BR-4" id="lvl12-3BR-4" ata-tooltip="Unit No: 3BR-4<br>Unit Type: 3BR-4" href="#modal3BR-4" coords="181,1158,730,720" shape="rect">
            <area alt="lvl12-2BR-4" id="lvl12-2BR-4" ata-tooltip="Unit No: 2BR-4<br>Unit Type: 2BR-4" href="#modal2BR-4" coords="731,1158,1190,720" shape="rect">
            <area alt="lvl12-2BR-1" id="lvl12-2BR-1" ata-tooltip="Unit No: 2BR-1<br>Unit Type: 2BR-1" href="#modal2BR-1" coords="1531,1158,1990,720" shape="rect">
            <area alt="lvl12-3BR-1" id="lvl12-3BR-1" ata-tooltip="Unit No: 3BR-1<br>Unit Type: 3BR-1 Mirror" href="#modal3BR-1" coords="1982,1158,2548,720 " shape="rect">
            <area alt="lvl12-2BR-3A" id="lvl12-2BR-3A" ata-tooltip="Unit No: 2BR-3A<br>Unit Type: 2BR-3A Mirror" href="#modal2BR-3A" coords="260,1644,640,1220" shape="rect">
            <area alt="lvl12-3BR-3A" id="lvl12-3BR-3A" ata-tooltip="Unit No: 3BR-3A<br>Unit Type: 3BR-3A Mirror" href="#modal3BR-3A" coords="650,1644,1178,1220" shape="rect">
            <area alt="lvl12-3BR-2A" id="lvl12-3BR-2A" ata-tooltip="Unit No: 3BR-2A<br>Unit Type: 3BR-2A Mirror" href="#modal3BR-2A" coords="1520,1644,2068,1220" shape="rect">
            <area alt="lvl12-2BR-2A" id="lvl12-2BR-2A" ata-tooltip="Unit No: 2BR-2A<br>Unit Type: 2BR-2A Mirror" href="#modal2BR-2A" coords="2080,1644,2460,1220" shape="rect">
          </map>

          <!-- Level 12a -->
          <div class="plans-lv12a plans-level">
            <img class="img-fluid" src="images/map/Vietnamese12b.png" usemap="#floor12a" alt="floor12a">
          </div>
          <map name="floor12a" id="floor12a">
            <area alt="lvl12a-4BR-2" id="lvl12a-4BR-2" ata-tooltip="Unit No: 4BR-2<br>Unit Type: 4BR-2" href="#modal4BR-2" coords="181,1158,1198,720" shape="rect">
            <area alt="lvl12a-4BR-1" id="lvl12a-4BR-1" ata-tooltip="Unit No: 4BR-1<br>Unit Type: 4BR-1" href="#modal4BR-1" coords="238,1644,1178,1220" shape="rect">
            <area alt="lvl12a-2BR-1" id="lvl12a-4BR-1" ata-tooltip="Unit No: 2BR-1<br>Unit Type: 2BR-1 mirror" href="#modal2BR-1" coords="1531,1158,1968,720" shape="rect">
            <area alt="lvl12a-3BR-1" id="lvl12a-3BR-1" ata-tooltip="Unit No: 3BR-1<br>Unit Type: 3BR-1" href="#modal3BR-1" coords="1972,1158,2528,720 " shape="rect">
            <area alt="lvl12a-3BR-2" id="lvl12a-3BR-2" ata-tooltip="Unit No: 3BR-2<br>Unit Type: 3BR-2 mirror" href="#modal3BR-2" coords="1520,1644,2068,1220" shape="rect">
            <area alt="lvl12a-2BR-2" id="lvl12a-2BR-2" ata-tooltip="Unit No: 2BR-2<br>Unit Type: 2BR-2 mirror" href="#modal2BR-2" coords="2068,1644,2438,1220" shape="rect">
          </map>

          <!-- Level 15 -->
          <div class="plans-lv15 plans-level">
            <img class="img-fluid" src="images/map/Vietnamese15.png" usemap="#floor15" alt="floor15">
          </div>
          <map name="floor15" id="floor15">
            <area alt="lvl15-Penthouse" id="lvl15-Penthouse" ata-tooltip="Unit No: PENTHOUSE<br>PH-1" href="#PENTHOUSE" coords="188,1658,1198,720" shape="rect">
            <area alt="lvl15-Sauna" id="lvl15-Sauna" ata-tooltip="Unit No: SAUNA" href="#SAUNA" coords="1520,1158,2528,720 " shape="rect">
            <area alt="lvl15-SkyGym" id="lvl15-SkyGym" ata-tooltip="Unit No: SKY GYM" href="#THEDUC" coords="1520,1644,2458,1220" shape="rect">
          </map>
          
          <!-- Level 16 -->
          <div class="plans-lv16 plans-level">
            <img class="img-fluid" src="images/map/Vietnamese16.png" usemap="#floor16" alt="floor16">
          </div>
          <map name="floor16" id="floor16">
            <area alt="lvl16-Penthouse" id="lvl16-Penthouse" ata-tooltip="Unit No: PENTHOUSE<br>PH-1" href="#PENTHOUSE" coords="188,1658,1198,720" shape="rect">
            <area alt="lvl16-SkyParty" id="lvl16-SkyParty" ata-tooltip="Unit No: SKY PARTY" href="#SANHTIEC" coords="1520,1158,2528,720 " shape="rect">
            <area alt="lvl16-SkyPool" id="lvl16-SkyPool" ata-tooltip="Unit No: SKY POOL" href="#HOBOI" coords="1520,1644,2458,1220" shape="rect">
          </map>
        </div>
      </div>
      <div class="col-6 col-lg-2 order-2 order-md-3 align-self-end align-self-md-center">
        <!-- <img class="img-fluid" src="images/new/keyplan.png" alt="keyplan"> -->
      </div>
    </article>
  </section>


<!-- Modal 4BR-2 -->
<div class="modal fade modalPlans" id="modal4BR-2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>4BR-2</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L1)</span></dd>
                        
                        <dd><span>A3.03 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.04 (L2 - L13, L16 - L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>67.82 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>59.80 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">  
                                     
                        <img class="img-fluid unit-plans" src="images/2BR-VI/BR12.jpg" alt="">                  
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 1BR-A -->
<div class="modal fade modalPlans" id="modal2BR-2A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-2A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L1)</span></dd>
                        
                        <dd><span>A3.03 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.04 (L2 - L13, L16 - L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>67.82 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>59.80 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">

                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR2.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-2 -->
<div class="modal fade modalPlans" id="modal2BR-2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-2</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L1)</span></dd>
                        
                        <dd><span>A3.03 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.04 (L2 - L13, L16 - L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>67.82 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>59.80 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR/English-2BR-2-RO.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 4BR-1 -->
<div class="modal fade modalPlans" id="modal4BR-1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>4BR-1</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L1)</span></dd>
                        
                        <dd><span>A3.03 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.04 (L2 - L13, L16 - L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>67.82 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>59.80 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR11.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 1BR-A -->
<div class="modal fade modalPlans" id="modal3BR-1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-1</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L1)</span></dd>
                        
                        <dd><span>A3.03 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.04 (L2 - L13, L16 - L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>67.82 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>59.80 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-09.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/1BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12  d-md-block">
                    <!-- d-none -->
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR7.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-A -->
<div class="modal fade modalPlans" id="modal2BR-A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.04 (L2-L22)</span></dd>
                        
                        <dd><span>A2.04 (L2 - L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>81.42 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>72.87 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-A.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-A.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12  d-md-block">
                    <!-- d-none -->
                    <img class="img-fluid unit-plans" src="images/new/2BR-A.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-A-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-A-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-A-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.05 (L2-L22)</span></dd>
                        
                        <dd><span>A2.05 (L2 - L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>81.42 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>72.87 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-A-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-A-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12  d-md-block">
                    <!-- d-none -->
                    <img class="img-fluid unit-plans" src="images/new/2BR-A-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-B -->
<div class="modal fade modalPlans" id="modal2BR-B" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-B</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A2.06 (L2-L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L13</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>86.37 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>77.57 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-B.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-B.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12  d-md-block">
                    <!-- d-none -->
                    <img class="img-fluid unit-plans" src="images/new/2BR-B.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-C -->
<div class="modal fade modalPlans" id="modal2BR-C" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-C</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.01 (L2-L22)</span></dd>
                        
                        <dd><span>A2.01 (L2-L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>85.55 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>76.86 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-C.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-C.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12  d-md-block">
                 <!-- d-none   -->
                  <img class="img-fluid unit-plans" src="images/new/2BR-C.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-C-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-C-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-C-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.08 (L2-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.84 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>76.99 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-C-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-C-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12  d-md-block">
                    <!-- d-none -->
                    <img class="img-fluid unit-plans" src="images/new/2BR-C-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-D -->
<div class="modal fade modalPlans" id="modal3BR-4" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Kế Hoạch Dự Án</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-4</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.04 (L1)</span></dd>
                        
                        <dd><span>A3.05 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.06 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.76 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.81 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-01.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center"> 
                <!-- d-none -->
                    <img class="img-fluid unit-plans "src="images/2BR-VI/BR10.jpg" alt="">              
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal HOBOI -->
<div class="modal fade modalPlans" id="HOBOI" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>HỒ BƠI NƯỚC NẶM ĐÁY KÍNH TRÊN KHÔNG</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="img/img_location/Facilities_005.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal SANHTIEC -->
<div class="modal fade modalPlans" id="SANHTIEC" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>SẢNH TIỆC TRÊN KHÔNG</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="img/img_location/Facilities_014.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal THEDUC -->
<div class="modal fade modalPlans" id="THEDUC" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>PHÒNG THỂ THAO TRÊN KHÔNG</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/2BR/English-2BR-1-RO.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="img/img_location/Facilities_009.jpg" alt="">
                </div>
                <!-- <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div> -->
            </div>
              
        </div>
    </div>
</div>
<!-- Modal SAUNA -->
<div class="modal fade modalPlans" id="SAUNA" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>PHÒNG XÔNG HƠI TRÊN KHÔNG</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="img/img_location/Facilities_011.jpg" alt="">
                </div>
                <!-- <div class="col-12">
                    <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div>
                </div> -->
            </div>
              
        </div>
    </div>
</div>
<!-- Modal PENTHOUSE -->
<div class="modal fade modalPlans" id="PENTHOUSE" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>PENTHOUSE</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR/English-PH-2.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 2BR-D-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-4" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-4A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR3.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 2BR-4A -->
<div class="modal fade modalPlans" id="modal2BR-4A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-4A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.05 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L14 - L15)</span></dd>
                        
                        <dd><span>A3.07 (L2 - L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.09 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.90 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-14.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-D-mirror-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR1.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 2BR-E1 -->
<div class="modal fade modalPlans" id="modal2BR-1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-1</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.01 (L1-L22)</span></dd>
                        
                        <dd><span>A3.03 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L1)</span></dd>
                        
                        <dd><span>A3.04 (L14-L15)</span></dd>
                        
                        <dd><span>A3.07 (L14-L15)</span></dd>
                        
                        <dd><span>A3.05 (L2-L13, L16-L22)</span></dd>
                        
                        <dd><span>A3.08 (L2-L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.65 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>73.94 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-16.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR/English-2BR-1-RO.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 2BR-E1 -->
<div class="modal fade modalPlans" id="modal2BR-1A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-1A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.01 (L1-L22)</span></dd>
                        
                        <dd><span>A3.03 (L1)</span></dd>
                        
                        <dd><span>A3.06 (L1)</span></dd>
                        
                        <dd><span>A3.04 (L14-L15)</span></dd>
                        
                        <dd><span>A3.07 (L14-L15)</span></dd>
                        
                        <dd><span>A3.05 (L2-L13, L16-L22)</span></dd>
                        
                        <dd><span>A3.08 (L2-L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.65 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>73.94 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-16.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E1-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR1.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-E2 -->
<div class="modal fade modalPlans" id="modal3BR-3A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-3A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A2.03 (L2-L13)</span></dd>
                        
                        <dd><span>A3.02 (L14, L16)</span></dd>
                        
                        <dd><span>A3.03 (L2-L13, L16-L22)</span></dd>
                        
                        <dd><span>A3.08 (L1)</span></dd>
                        
                        <dd><span>A3.09 (L14-L15)</span></dd>
                        
                        <dd><span>A3.10 (L2-L13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>83.68 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>74.96 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-07.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-03.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-04.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-05.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR8.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-E2-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-3A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-3A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.06 (L2 - L22)</span></dd>
                        
                        <dd><span>A1.07 (L14, L16)</span></dd>
                        
                        <dd><span>A3.07 (L1)</span></dd>
                        
                        <dd><span>A3.08 (L14-15)</span></dd>
                        
                        <dd><span>A3.09 (L2-13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>84.12 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>75.12 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-15.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-03.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-E2-mirror-04.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR2.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-F-Duo -->
<div class="modal fade modalPlans" id="modal2BR-F-Duo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-F-Duo</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.02 (L2-L13, L15 & L17-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>94.17 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>84.27 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-F-duo.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-F-duo.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/new/2BR-F-duo.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 2BR-F-Duo-Mirror -->
<div class="modal fade modalPlans" id="modal2BR-F-Duo-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>2BR-F-Duo-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.07 (L2-L13, L15 & L17-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>94.17 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>84.27 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/2BR-F-duo-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/2BR-F-duo-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/new/2BR-F-duo-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-A -->
<div class="modal fade modalPlans" id="modal3BR-2A" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-2A</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.09 (L1)</span></dd>
                        
                        <dd><span>A3.10 (L14-15)</span></dd>
                        
                        <dd><span>A3.11 (L2-13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>103.77 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>93.18 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-06.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR8.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-2 -->
<div class="modal fade modalPlans" id="modal3BR-2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-2</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.09 (L1)</span></dd>
                        
                        <dd><span>A3.10 (L14-15)</span></dd>
                        
                        <dd><span>A3.11 (L2-13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>103.77 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>93.18 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-06.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR9.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 3BR-3 -->
<div class="modal fade modalPlans" id="modal3BR-3" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-2</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A3.09 (L1)</span></dd>
                        
                        <dd><span>A3.10 (L14-15)</span></dd>
                        
                        <dd><span>A3.11 (L2-13, L16-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L01 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>103.77 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>93.18 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/room/3BR-06.jpg" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-01.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-02.png" alt="">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-A-03.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/2BR-VI/BR9.jpg" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
<!-- Modal 3BR-B-Duo -->
<div class="modal fade modalPlans" id="modal3BR-B-Duo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-B-Duo</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.02 (L2-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>111.44 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>99.23 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/3BR-B-duo.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-B-duo.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/new/3BR-B-duo.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-B-Duo-Mirror -->
<div class="modal fade modalPlans" id="modal3BR-B-Duo-Mirror" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plan</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-B-Duo-Mirror</span></dd>
                    </li>
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A1.03 (L2-L22)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L22</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>111.44 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>99.23 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/3BR-B-duo-mirror.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-B-duo-mirror.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/new/3BR-B-duo-mirror.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>

<!-- Modal 3BR-D-Triple -->
<div class="modal fade modalPlans" id="modal3BR-D-Triple" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <div class="crossBtn" data-dismiss="modal" aria-label="Close"><i></i></div>
            </div>
            <div class="row modal-plans-padding">
                <div class="col-md-3 d-none">
                    <div class="floorplans-logo">
                    <img class="img-fluid" src="img/D1Logo2.png">
                    </div>
                    <h2>Unit Plans</h2>
                    <ul class="unit-info plans-data">
                    <li class="unit-type">
                        <dt>Unit Type</dt>
                        <dd><span>3BR-D-Triple</span></dd>
                    </li> 
                    <li class="unit-no">
                        <dt>Unit No :</dt>
                        
                        <dd><span>A2.02 (L2-L13)</span></dd>
                        
                    </li>
                    <li>
                        <dt>Floor No</dt>
                        <dd><span>L02 - L13</span></dd>
                    </li>
                    <li>
                        <dt>Gross Floor Area</dt>
                        <dd><span>95.97 sqm</span></dd>
                    </li>
                    <li>
                        <dt>Net Saleable Area</dt>
                        <dd><span>86.15 sqm</span></dd>
                    </li>
                    </ul>
                    <div class="col d-md-none">
                        <img class="img-fluid unit-plans" src="images/new/3BR-D-triple.png" alt="">
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        
                        <img class="img-fluid unit-plans storey-plans" src="images/new/3BR-D-triple.png" alt="">
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 d-md-block text-center">
                    <img class="img-fluid unit-plans" src="images/new/3BR-D-triple.png" alt="">
                </div>
                <div class="col-12">
                    <!-- <div class="floorplans-disclaimer">
                        <h5>Disclaimer</h5>
                        <ul class="pl-3">
                            <li>This plan is subject to change as may be approved by the relevant authorities.</li> <li>This plan is not drawn to scale. Dimensions shown are in millimeters.</li> <li>The areas quoted are approximate measurements and are subject to final measurements by the registered surveyor.</li>
                        </ul>
                    </div> -->
                </div>
            </div>
              
        </div>
    </div>
</div>
