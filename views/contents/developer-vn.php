<section id="divKeyVisual" class="clsKeyvisual">
   <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
      <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/developerhero.jpg">
         <!-- <div class="clsArtImpression2">
            <span class="artimp1">Artist's impression  </span>
         </div> -->
      </div>
      <img id="imgDevKeyVis-mobile" src="img/developer-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
      <div  id="divPgKeyOuterTxtWrap">
         <div id="divDevPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >
            <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">
               <div id="divDevParllaxTxt1" class="clsHeader1">
                  <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">Mở ra</span>
                  <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">những </span> 
                  <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet"> giới hạn</span> 
                </div>
                <div id="divDevParllaxTxt2" class="divDevPGTxt2Viet">
                  <span class="clsHeaderBigFont clsHeader1   clsHeaderNormalFontViet">CHƯA TỪNG</span> 
                  <span class="clsHeaderBigFont_vn clsHeader1 clsHeaderItalicFontViet">được chạm tới</span>
                </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="developer-slider parllaxpadding" >
   <div class="container " id="deverloperPgContainer" style="background-color:white;padding:0">
      <div style="display:block; width:100%; height:50px;"></div>
      <div class="mod_location_slider" id="mod_location_slider1">
         <div class="mod_location_slider_box">
            <div class="mod_location_slider_item active">
               <div class="thumb">
                  <img src="img/developer11.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities1" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="mod_location_slider_item">
               <div class="thumb">
                  <img src="img/developer2.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities2" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="mod_location_slider_item">
               <div class="thumb">
                  <img src="img/developer3.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities3" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="mod_location_slider_item">
               <div class="thumb">
                  <img src="img/developer4.png" alt="location_location_pic1"/>
                  <div id="" class="clsBarInner zoomMap_facilities4" style="display:none;">
                     <div class=" col-xs-12 text-center clsNormalFont">
                        <a>CLICK TO VIEW
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="desc">&nbsp;</div>
            </div>
            <!-- END mod_location_slider_item -->
            <div class="controlx">
               <div class="controlx_box">
                  <div class="controlx_btn"></div>
                  <div class="controlx_btn right"></div>
                  <div class="clear"></div>
               </div>
            </div>
         </div>
         <!-- END mod_location_slider_box-->
         <!-- Controls -->
      </div>
      <!-- END mod_location_slider-->
      <div id="secDeveloperPgPara" class="clsPurple2">
         <p>
         Được niêm yết và đặt trụ sở chính tại Singapore, CapitaLand là một trong những tập đoàn Bất động sản lớn nhất Châu Á. Tại đây, tập đoàn không ngừng tận dùng nguồn lực dồi dào, tối ưu chiến lược quản lý vốn, kết hợp cùng sự am hiểu thị 
            trường và khả năng vận hành để phát triển nên các dự án Bất động sản cao cấp xuất sắc. Tập đoàn cũng đa dạng danh mục phát triển bất động sản của mình với rất nhiều công trình tổ hợp, trung tâm mua sắm, căn hộ dịch vụ, văn phòng và các dự án căn hộ.

        
        </p>
        <p>
        Việt Nam là một trong số các thị trường trọng điểm của CapitaLand tại châu Á. Tại Việt Nam, thị trường Bất động sản được thúc đẩy bởi nguồn vốn trong nước dồi dào, tốc độ đô thị hóa nhanh chóng, cùng dân số trẻ đang ngày càng phát triển. 
            CapitaLand cam kết trở thành một nhà phát triển bất động sản bền vững tại Việt Nam. Tập đoàn CapitaLand hiện diện tại 6 thành phố lớn – Hồ Chí Minh, Hà Nội, Hải Phòng, Đà Nẵng, Bình Dương và Nha Trang với các dự án căn hộ nhà ở và căn hộ dịch vụ.

        
        </p>
        <p>
        
        Với các dự án căn hộ, CapitaLand sở hữu danh mục lên tới gần 9,100 các căn hộ cao cấp qua 9 dự án tại TP Hồ Chí Minh và Hà Nội. Với các dự án căn hộ dịch vụ, 
            The Ascott Limited– tập đoàn quản lý trực thuộc CapitaLand, đồng thời sở hữu danh mục hơn 4,000 căn hộ dịch vụ tại 19 dự án trên khắp 7 thành phố lớn, trở thành tập đoàn sở hữu – vận hành căn hộ dịch vụ lớn nhất tại Việt Nam.
        </p>
      </div>
   </div>
</section>


<style type="text/css" >
      #secIntroSlider{background:none;}
      .msd_slider_colset.active{display: block !important;}
</style>
<section class="intro-slider" id="secIntroSlider">
  <!--  -->
            <div class="container-fluid nopadding" style="background-color:white;padding:0">
            
                  <!-- MSD SLIDER 1 -->
                  <div id="slider1" class="msd_slider" style="width: 90%; height: 962px;">
      
                      <div class="msd_slider_colset active">
                  
                          <div class="msd_slider_col"><div class="msd_slider_col_box" style="text-align: center;">Thumbnail</div></div>
              <div class="msd_slider_col"><div class="msd_slider_col_box">Description</div></div>
                      
                      </div><!-- end msd_slider_colset -->
                      
                      <div class="msd_slider_colset">
                  
                          <div class="msd_slider_col"><div class="msd_slider_col_box" style="text-align: center;">Thumbnail</div></div>
              <div class="msd_slider_col"><div class="msd_slider_col_box">Description</div></div>
                      
                      </div><!-- end msd_slider_colset -->
                      
                      <div class="clear"></div>
                      
                      <!-- Controls -->
                      <div class="controlx"><div class="controlx_btn"></div><div class="controlx_btn"></div><div class="clear"></div></div>
                  
                  </div><!-- end msd_slider -->
                  
                  <div id="sliderdata1" class="msd_slider_data">
                  
                    <!-- Slider 1 Data 1 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                      <div class="data_a"><img id="imgCeo" src="img/d1_ceo-vn.png" alt="" width="300" height="508" /></div>
                      <div class="data_b">
                      
                      
                       <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                            
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase">Thư ngỏ từ</span> 
                                </div>

                                <div id="divIntroSlideHeadTxt2">
                                    
                                    <span class="clsHeaderBigFont clsHeaderNormalFont clsHeader1 ">CapitaLand</span>
                                </div>
                            </div>
                            <div class=" clsPurple2">
                                <p class="text-center">
                                    Tôi cảm thấy vô cùng tự hào khi chính thức giới thiệu tới các bạn D1MENSION, dự án Branded Residence (Được thương hiệu quốc tế quản lý sau khi hoàn thành) đầu tiên tại Quận 1 TP. Hồ Chí Minh, phát triển bởi Tập đoàn phát triển Bất Động Sản Quốc tế CapitaLand, 
                                    và được quản lý bởi tập toàn quản lý căn hộ dịch vụ cho thuê cao cấp hàng đầu thế giới – The Ascott Limited. Từ phong cách sống đỉnh cao cho tới những tuyệt tác nội thất, CapitaLand mang niềm tin sâu sắc rằng D1MENSION sẽ là nơi kiến tạo nên một dấu ấn thượng lưu mới.
                                </p>
                                <p class="text-center"><a href="messagefromcapitaland-vn.html">Đọc thêm</a></p>
                            </div>  
                        </div>
                                          
                      
                      </div><!-- end data_b -->
                      
                      <!-- Slider 1 Data 2 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                      <div class="data_a"><img id="imgAscott" src="img/ascott_cgm-vn.png" alt="" width="300" height="508"/></div>
                      <div class="data_b">
                      
                        <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig">
                             
                            <div class="groupheader">
                                <div id="divIntroSlideHeadTxt1_2" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFontViet  clsHeader1 text-uppercase">Thư ngỏ từ</span> 
                                </div>

                                <div id="divIntroSlideHeadTxt2_2">
                                    <span class="clsHeaderBigFont clsHeaderNormalFont clsHeader1 ">The Ascott Limited </span>
                                   
                                </div>
                            </div>
                            <div class="text-center clsPurple2">
                                <p class="text-center">
                                    Đại diện cho The Ascott Limited, chúng tôi rất đỗi tự hào khi được chỉ định trở thành đơn vị quản lý cho D1MENSION - dự án danh giá của CapitaLand, đồng thời cũng là đơn vị trực tiếp vận hành Somerset D1Mension Hồ Chí Minh 
                                    - tháp căn hộ dịch vụ nằm trong tổng thể dự án. Từ chuẩn mực đỉnh cao trong các tổ hợp tiện ích, cho tới sự nhiệt thành của đội ngũ nhân viên, chúng tôi chào đón bạn tới với cuộc sống hoàn hảo tại D1MENSION.
                                </p>
                                <p class="text-center"><a href="messagefromascott-vn.html">Đọc thêm</a></p>
                            </div>
                        
                        </div>
                 
                      
                      </div><!-- end data_b -->
                      
                  </div><!-- MSD SLIDER 1 END -->
                  
               

                
        
            </div>
        </section>
    <style>
    #secIntroSlider {
      min-height: 700px !important;  
    }
    #secIntroSlider #slider1.msd_slider {
      min-height: 599px !important;  
    }
    #secIntroSlider .container-fluid {
      max-width: 1140px !important;
      margin: 0 auto !important;
    }
    #secIntroSlider .msd_slider_colset, #secIntroSlider .msd_slider .msd_slider_col {
      width: 100% !important;
      max-width: 1024px !important;
    }
    #secIntroSlider .msd_slider_col_box img {
      /* width: 550px !important; */
      margin: 0 auto !important;
      display: inline-block;
      text-align: center;
    }
    </style>