  <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2 hidden-xs" data-parallax="scroll" data-image-src="img/locationhero.jpg"> 
                    <div class="clsArtImpression2">
                        <!--<span class="artimp2">Artist's impression  </span>-->
                    </div>
                </div>
                <img id="imgLocKeyVis-mobile" src="img/location-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                <div  id="divPgKeyOuterTxtWrap">
                    <div id="divLocPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >
                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center locHeaderVNAlign" >
                            <div id="divLocPgKeyVisHeadTxt1" >
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet text-left">Kiến tạo</span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderNormalFontViet text-left text-uppercase">Chuẩn Mực Mới</span> 
                            </div>
                            <div id="divLocPgKeyVisHeadTxt2" style="padding-left: 10%;">
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFontViet"> tại&nbsp;</span> 
                                                <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">CHÂU Á</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </section>
        <section id="sec" class="parllaxpadding"> 
            <div class="container-fluid">
				<div class="msd_maincontainer">
                    <!-- header -->
                    <div id="divIntroSlideTxt2" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                            <div class="groupheader">
                                
                                <div id="divIntroSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Sophistication</span> 
                                </div>
                                <div id="divIntroSlideHeadTxt2">
                                	<span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inside</span>
                                    <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">and&nbsp;</span> 
                                    <span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">out</span>
                                </div>
                                
                            </div>
                            
                        </div>  
                    <!-- end header -->
                    <div class="loc_map1"><img src="img/LocationMap4.jpg" alt="location_map1" style="margin-bottom:20px;" /></div>
                    
                <div id="IntroBarInner" class="clsBarInner zoomLocationMap1">
                    <div class=" col-xs-12 text-center clsNormalFont text-uppercase">
                        <a>Nhấn để xem thêm 
                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="loc2_map1_legend">
                    <div class="loc_map1_legend_set">
                        <div class="loc_map1_legend_cols_h1 icon1">ĐỊA ĐIỂM</div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">1</span> Bitexco</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">2</span> Khách sạn Pullman Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">3</span> Khách sạn Park Hyatt Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">4</span> Khách sạn InterContinental Asiana Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">5</span> Khách sạn Le Méridien Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">6</span> Khách sạn The Reverie Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">7</span> Khách sạn Nikko Sài Gòn</p>
                            
                            
                        </div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">8</span> Khách sạn Equatorial TP HCM</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">9</span> Chợ Bến Thành</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">10</span> Nhà thờ Đức bà</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">11</span> Nhà hát thành phố</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">12</span> Chợ Lớn</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">13</span> Nhà khách chính phủ</p>
                            <p class="lgnd_1"><span  class="a1 amenitiesCircle">14</span> Bưu điện thành phố</p>
                            
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="loc_map1_legend_set">
                        <div class="loc_map1_legend_cols_h1 icon4">TRUNG TÂM MUA SẮM VÀ GIẢI TRÍ</div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">15</span> Trung tâm thương mại Vincom</span></p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">16</span> Trung tâm sự kiện Gem</p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">17</span> Chill Skybar</p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">18</span> Riverside Palace</p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">19</span> Trung tâm thương mại Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">20</span> Khu người Hoa</p>
                        </div>
                        
                        <div class="loc_map1_legend_cols">
                            
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">21</span> Now Zone Plaza</p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">22</span> Chợ An Đông</p>
                            <p class="lgnd_1"><span  class="a2 amenitiesCircle">23</span> Công viên 23/9</p>
                             <p class="lgnd_1"><span  class="a2 amenitiesCircle">24</span> Công viên Tao Đàn</p>
                              <p class="lgnd_1"><span  class="a2 amenitiesCircle">25</span> Bảo tàng nghệ thuật</p>
                              <p class="lgnd_1"><span  class="a2 amenitiesCircle">26</span> Sân vân động Thống Nhất</p>
                             
                            
                        </div>
                        
                        
                        <div class="clear"></div>
                    </div>
                    
                    <div class="loc_map1_legend_set">
                        <div class="loc_map1_legend_cols_h1 icon6">TRUNG TÂM HÀNH CHÍNH </div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a3 amenitiesCircle">27</span> Ủy ban nhân dân TP Hồ Chí Minh</p>
                            <p class="lgnd_1"><span  class="a3 amenitiesCircle">28</span> Ngân hàng nhà nước Việt Nam</p>
                            
                        </div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a3 amenitiesCircle">29</span> Sở cảnh sát thành phố</p>
                            <p class="lgnd_1"><span  class="a3 amenitiesCircle">30</span> Cục cảnh sát phòng cháy chữa cháy</p>
                            
                        </div>
                        
                        
                        <div class="clear"></div>
                    </div>
                    <div class="loc_map1_legend_set">
                        <div class="loc_map1_legend_cols_h1 icon2">BỆNH VIỆN</div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a4 amenitiesCircle">31</span> Bệnh viện đa khoa Sài Gòn</p>
                            <p class="lgnd_1"><span  class="a4 amenitiesCircle">32</span> Bệnh viện quốc tế Columbia Asia Saigon</p>
                            
                        </div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a4 amenitiesCircle">33</span> Bệnh viện FV Saigon</p>
                            <p class="lgnd_1"><span  class="a4 amenitiesCircle">34</span> Bệnh Viện Chấn Thương Chỉnh Hình</p>
                            
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="loc_map1_legend_set">
                        <div class="loc_map1_legend_cols_h1 icon5">GIÁO DỤC</div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a5 amenitiesCircle">35</span> Trường quốc tế The American School</p>
                            <p class="lgnd_1"><span  class="a5 amenitiesCircle">36</span> Đại học Khoa học TP HCM</p>
                            
                        </div>
                        
                        <div class="loc_map1_legend_cols">
                            <p class="lgnd_1"><span  class="a5 amenitiesCircle">37</span> Trường Lê Hồng Phong</p>
                             <p class="lgnd_1"><span  class="a5 amenitiesCircle">38</span> Đại học Sài Gòn</p>
                            
                        </div>
                        <div class="clear"></div>
                    </div>                         
                </div>  
                <div style="width:100%; height:50px;"></div>
             

                <div id="divLocImgs">
                    <div class="row">
                        <div class="portfolio-items">
                            <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <a href="" ><img class="img-responsive" src="img/keyplaces/thumbs/Ben-Thanh-Market.jpg" alt=""></a>
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item joomla bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Bitexco-Financial-Tower.jpg" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item bootstrap wordpress col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Cho-Lon-Market.jpg" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>        
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item joomla wordpress apps col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/City-People-Committee.jpg" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>           
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item joomla html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/City-Post-Office.jpg" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>      
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item wordpress html apps col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Columbia-Asia-Hospital.jpg" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>         
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item wordpress html col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Gem-Center.jpg" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->

                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Hotel-Inter-Continental.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Hotel-Park-Hyatt.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Le-Meridien.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Museum-of-Ho-Chi-Minh-City.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Opera-House.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Saigon-Center.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Saigon-Notre-Dame-Basilica.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/State-Bank-of-Vietnam.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Sunwah-Tower.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Tao-Dan-Park.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/The-Reverie.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Thien-Hau-Temple.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                            <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="img/keyplaces/thumbs/Vincom-Center.jpg" alt="">
                                    <div class="overlay ">
                                        <div class="recent-work-inner ">
                                            <h3></h3>
                                            <p></p>
                                            <span class="preview"  ><i class="fa fa-eye"></i> View</span>
                                        </div> 
                                    </div>
                                </div>          
                            </div><!--/.portfolio-item-->
                            
                        </div>
                    </div>
                </div>
                    <!-- /* OSR  End - KEY PLACES */ -->
            </div><!-- END msd_maincontainer -->
            </div>    
        </section>
       <!-- /* OSR  Begin - KEY PLACES */ -->
    <style>
        #divLocImgs {width:97% !important;margin:0 auto;}
        
        .recent-work-wrap .overlay .preview {
            bottom: 0;
            display: inline-block;
            height: 35px;
            line-height: 35px;
            border-radius: 0;
            background: transparent;
            text-align: center;
            color: #fff;
            z-index: 1;
            position: absolute;
            
            width: 100%;
            height: 100%;

            top: 50%;
            left: 0;
            padding: 0;

        }
        
        @media only screen and (max-width : 740px) {#divLocImgs {width:90% !important;}}
    </style>