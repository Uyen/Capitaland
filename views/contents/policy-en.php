
<section class="section-policy">
   <div class="container">
      <div class="policy__block">
         <h3 class="policy__title text-center">Personal Information Protection Policy</h3>

         <div class="policy__content">
            <div class="policy__item">
               <p>1. This Personal Information Protection Policy (“Policy”) outlines how we, RIVER VIEW COMPANY LIMITED, subsidiaries, affiliates and related dependent units (“we”, “us” or “our”) as the context requires, manage the Personal Information we hold. We respect your Personal Information and privacy and are committed to complying with Vietnamese laws on personal information protection. Please read this Policy so that you know and understand the purposes for which we collect, use and disclose your Personal Information. This Policy supplements but does not supersede nor replace any other consents you may have previously provided to us in respect of your Personal Information, and your consents herein are additional to any rights which we may have at law to collect, use or disclose your Personal Information. This Policy does not affect any rights which we may have at law in connection with the collection, use or disclosure of your Personal Information.</p>
            </div>
            <div class="policy__item">
               <div class="title">2. Personal Information</div>
               <div class="content">
                  <p>“Personal Information” means any information or data about you from which you can be identified, including your name, national identification card/passport/other identification number, telephone number, home address, mailing address, email address, medical information, account number, information about individual payment transaction, and any other information relating to you which you have provided us in any form you may have submitted to us, or in other forms of interaction with you, but excluding business contact information or information that you disclose on media by yourself.</p>
                  <p>If your Personal Data has been collected, used or disclosed by when specific requirements or exceptions to the definition of Personal Data are available including sensitive Personal Data under Vietnamese laws, we will comply with those requirements under the applicable Vietnamese laws on personal information protection.</p>
               </div>
            </div>
            <div class="policy__item">
               <div class="title">3. Purpose and scope of the collection of the Personal Information</div>
               <div class="content">
                  <p>When you send the Personal Information to us via this website or email address provided on this website, you are considered to have accepted the collection, use and disclosure of the Personal Information as described herein. We will, subject to applicable law, use and disclose your Personal Information for the following purposes:</p>
                  <p>(a provide you with products or services that you have requested;</p>
                  <p>(b)  help us review, develop, improve, manage the delivery of and – to the extent this requires the use of Personal Information – enhance our products and services, including analysing future customer needs, conducting market research and information analytics;</p>
                  <p>(c)  communicate with you and respond to your queries, requests and complaints;</p>
                  <p>(d)  provide ongoing information about our products and services which may be of interest to you;</p>
                  <p>(e)  handle disputes and conduct and facilitate investigations and proceedings;</p>
                  <p>(f)   protect and enforce our contractual and legal rights and obligations;</p>
                  <p>(g)  prevent, detect and investigate crime, including fraud and money-laundering, and to analyse and manage other commercial risks;</p>
                  <p>(h)  manage our infrastructure and business operations and comply with internal policies and procedures; and</p>
                  <p>(i)    comply with any applicable rules, laws and regulations, codes of practice or guidelines or assist in law enforcement and investigations by relevant authorities.</p>
                  <p>In some cases, the provision and processing of your Personal Information may be a statutory and/or contractual requirement or may be necessary in order to perform any contract you have agreed with us or perform services that you have requested.</p>
                  <p>Your Personal Information will be protected and kept confidential subject to the provisions of any applicable law, your Personal Information may, depending on the products or services concerned, be used, shared, disclosed or transferred to third parties.</p>
               </div>
            </div>
            <div class="policy__item">
               <div class="title">4.    Retain of the Personal Information</div>
               <div class="content">
                  <p>We may retain your Personal Data for as long as it is necessary for the purposes it has been collected, and in most cases, up to 7 years, unless otherwise permitted by applicable law or in order to defend legal claims. Where we no longer require your Personal Data for those purposes, we will cease to retain such Personal Data in accordance with our internal retention policy.</p>
               </div>
            </div>
            <div class="policy__item">
               <div class="title">5.    Cookie Policy</div>
               <div class="content">We use cookies when you use our websites. In many cases, the information we collect using cookies is used on an aggregated basis and in a non-identifiable manner. For example, we may use information collected to better understand website traffic patterns and to improve website experience for our users.</div>
            </div>
            <div class="policy__item">
               <div class="title">6.    Third-Party Sites</div>
               <div class="content">
                  <p>Our website may contain links to other websites operated by third parties, such as our business partners. We are not responsible for the privacy practices of websites operated by third parties that are linked to our website, unless that is a mandatory requirement by applicable Information protection laws and regulations. We encourage you to learn about the privacy policies of such third parties. Once you have left our website, you should check the applicable privacy policy of such third parties at their respective websites to determine how they will handle any information they collect from you.</p>
               </div>
            </div>
            <div class="policy__item">
               <div class="title">7.    Third Party Modules</div>
               <div class="content">Our website uses the Google Maps API from Google Inc. For more information about the privacy practices of Google, please see Google’s Privacy Policy available at http://www.google.com/policies/privacy or such other URL as Google may provide from time to time. The collection, use and disclosure of your Personal Information by Google through such functionality built into our website, shall be subject to Google’s Privacy Policy.</div>
            </div>
            <div class="policy__item">
               <div class="title">8.   Persons or organisations allowed to access the Personal Information</div>
               <div class="content">Only our appointed staff and service providers are allowed to access your Personal Information for the purposes as provided in Section 3.</div>
            </div>
            <div class="policy__item">
               <div class="title">9.    How to contact us</div>
               <div class="content">
                  <p>If you have any questions about this Policy or any queries relating to your Personal Information, or you would like to obtain access and/or make corrections to your Personal Information records, please contact:</p>
                  <p>RIVER VIEW COMPANY LIMITED</p>
                  <p>Contact address: 8th Floor, Vista Tower, 628C Hanoi Highway, An Phu Ward, District 2, Ho Chi Minh City, Vietnam.</p>
               </div>
            </div>
            <div class="policy__item">
               <div class="title">10.  Review of this Policy</div>
               <div class="content">
                  <p>This Policy will be reviewed from time to time by us. We may also from time to time update this Policy to take account of new laws and technology, changes to our operations and practices and the changing business environment. If you are unsure whether you are reading the most current version, please contact us. In the event of any inconsistencies between the English version and other translations of this Policy, the English version shall prevail.</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>