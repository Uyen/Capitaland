<section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding ">
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/hero2.jpg"> 
                     <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div>
                   
                </div>
                
                <img id="imgRegKeyVis-mobile" src="img/register-mobile.jpg" width="1209" height="908" alt="" class="visible-xs">
                
                <div id="divPgKeyOuterTxtWrap">
                
                    <div id="divRegPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap">

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                            <div id="divRegPgKeyVisHeadTxt1">
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont text-left">Setting </span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont text-left">THE </span> 
                            </div>

                            <div id="divRegPgKeyVisHeadTxt2">

                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont  ">Standard</span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFont">IN&nbsp;</span> 
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont">ASIA </span>
                            </div>
                        </div>
                       

                    </div>

                </div>
            </div>    
           

        </section>
        <section id="secRegForm" class="parllaxpadding">
                <div class="container whitebg" id="secRegFormContainer">
                    <div class="row">
                    <form class="well form-horizontal  bv-form" action="registersend.php" method="post" id="contact_form" accept-charset="UTF-8" novalidate="novalidate" style="color: black;"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                            <input type="hidden" name="showform" value="true">
                            
                        <div id="frmWrap">
                            <fieldset>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- OSR - FROM END -->
                                    
                                    <!-- <div class="form-group">
                                        <div class="col-md-12 text-uppercase  clsGold"><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">REGISTRATION</span> &nbsp;&nbsp;&nbsp;<span class="visible-xs"></span><span class="clsMandatoryLbl headerTxt"></span><hr></div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-12 text-uppercase  clsGold" ><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">Đăng kí nhận thông tin.</span> &nbsp;&nbsp;&nbsp;<span class="visible-xs"></span><span class="clsMandatoryLbl headerTxt"></span><hr></div>
                                    </div>

                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12">
                                            Full name
                                        </div>
                                        <div class="col-md-3"> 
                                            <select name="saluation" class="form-control selectpicker-  element-transparent splSelect ">
                                                <option value="MR.">MR.</option>
                                                <option value="MRS.">MRS.</option>
                                                <option value="MS.">MS.</option>
                                                <option value="DR.">DR.</option>
                                                <option value="PROF.">PROF.</option>
                                            </select>
                                            <span class="visible-xs"><br></span>
                                        </div>

                                        <div class="col-md-9 inputGroupContainer">
                                            <input name="full_name" placeholder="" class="form-control  element-transparent clsGold" type="text" data-bv-field="full_name"><i class="form-control-feedback bv-no-label" data-bv-icon-for="full_name" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="stringLength" data-bv-for="full_name" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter a value with valid length</small><small class="help-block" data-bv-validator="notEmpty" data-bv-for="full_name" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in your Full Name</small></div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            Tên đầy đủ
                                        </div>
                                        <div class="col-md-3"> 
                                            <select name="saluation" class="form-control selectpicker-  element-transparent splSelect " >
                                                <option  value="MR.">Ông</option>
                                                <option  value="MRS.">Bà</option>
                                                <option  value="MS.">Anh</option>
                                                <option  value="DR.">Chị</option>
                                            </select>
                                            <span class="visible-xs"><br></span>
                                        </div>

                                        <div class="col-md-9 inputGroupContainer">
                                            <input  name="full_name"  placeholder="" class="form-control  element-transparent clsGold"  type="text">
                                        </div>
                                    </div>

                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12 ">
                                            Contact number
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input name="phonenumber" placeholder="" class="form-control  element-transparent" type="text" data-bv-field="phonenumber"><i class="form-control-feedback bv-no-label" data-bv-icon-for="phonenumber" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="phonenumber" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in your Phone Number</small></div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Số điện thoại
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input  name="phonenumber"  placeholder="" class="form-control  element-transparent"  type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Email
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input  name="email"  placeholder="" class="form-control  element-transparent"  type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Mã vùng/ khu vực
                                        </div>
                                        <div class="col-md-12">
                                            <input  name="countrycode"  placeholder="" class="form-control  element-transparent"  type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Yêu cầu / Chi tiết phản hồi
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="message" rows="5" placeholder="" class="form-control  element-transparent"  type="text"></textarea> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            Captcha
                                        </div>
                                        <div class="col-md-8 col-xs-8">
                                            <input  name="captcha"  placeholder="" class="form-control  element-transparent "  type="text" size="3">
                                        </div>
                                        <div class="col-md-4 offset-md-8 col-xs-4 ">
                                            <img src="captcha.php"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div id="divTermsWrap" style="height: 200px; overflow: auto">
                                                <i>Thông Tin Cá Nhân bạn cung cấp cho CapitaLand Limited (“<b>CapitaLand</b>”) trong mẫu này (“Thông Tin Cá Nhân”) sẽ được xử lý, thu thập, sử dụng và công bố bởi CapitaLand, cũng như, dưới chỉ định của CapitaLand, bởi các bên thuộc tập đoàn và công ty con (“<b>Tập Đoàn</b>”), đối tác và các bên cung cấp dịch vụ được uỷ quyền bởi Tập Đoàn, chiếu theo luật lệ bảo mật thông tin.&nbsp;<br>
                                                <br>
                                                Thông Tin Cá Nhân sẽ được xử lý, thu thập, sử dụng và công bố bởi Tập Đoàn cho mục đích rà soát, phát triển, cải thiện, quản lý sản phẩm và dịch vụ cung cấp nhằm nâng cao chất lượng, phân tích nhu cầu khách hàng, khảo sát thị trường và phân tích dữ liệu, cũng như các mục đích khác thông báo cho bạn theo Chính Sách Bảo Vệ Dữ Liệu Của CapitaLand (“<b>Chính Sách Bảo Vệ Dữ Liệu</b>”) (vào <a href="https://www.capitaland.com/international/en/legal-notices/privacy-policy.html">liên kết này</a>) (“<b>Mục Đích</b>”). <br>
                                                <br>
                                                Nhiều thông tin hơn cung cấp cho bạn, bạn phải đọc kỹ chính sách Thông Tin Cá Nhân và lưu giữ thông tin được đưa ra trong Chính Sách Bảo Vệ Dữ Liệu. Bạn có quyền từ chối cung cấp thông tin vào bất cứ lúc nào bằng việc liên hệ với Các Chuyên Viên Bảo Vệ Dữ Liệu, với chi tiết liên hệ đã đề cập trong Chính Sách Bảo Vệ Dữ Liệu.<br>
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><br />
                                        
                                        <div class="col-md-12 text-uppercase  clsGold" ></div>
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label class="" for="tc">
                                                <input id="tc" type="checkbox" name="tc" value="TC Checked"> 
                                                    Tôi đồng ý nhận các thông tin, tin tức mới từ CapitaLand và tôi đã đọc, cũng như đồng thuận với các điều khoản bảo mật riêng tư.
                                                </label>
                                            </div>
                                            <div class="visible-xs"></div>
                                            <div class="k-invalid-msg" data-for="tc">&nbsp;</div>
                                        </div>
                                        <div class="t_c"></div>
                                    </div>
                                    <div class="form-group">
                                        <div id="divPushAboveSubmit"></div>
                                    </div>
                                   
                                    <div class="form-group text-center">

                                        <div class="col-md-6  col-xs-12  clsSubmitVerySmall- DivBtnSubmit">
                                            <button type="submit" class="btn  btn-custom btnSubmit btnSubmitVN" >CHẤP NHẬN <span class="glyphicon glyphicon-play"></span></button>
                                           
                                        </div>
                                        <div class="col-md-6 col-xs-12 clsSubmitVerySmall- divBtnReset">
                                            
                                            <button type="reset" class="btn btn-custom btnReset btnResetVN" >THỰC HIỆN LẠI <span class="glyphicon glyphicon-refresh"></span></button>
                                        </div>
                                        
                                    </div>
                                   <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12 ">
                                            Email
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input name="email" placeholder="" class="form-control  element-transparent" type="text" data-bv-field="email"><i class="form-control-feedback bv-no-label" data-bv-icon-for="email" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in your Email address</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in a valid Email address</small></div>
                                    </div> -->
                                    
                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12 ">
                                            Country of residence
                                        </div>
                                        <div class="col-md-12">
                                            <input name="countrycode" placeholder="" class="form-control  element-transparent" type="text" data-bv-field="countrycode"><i class="form-control-feedback bv-no-label" data-bv-icon-for="countrycode" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="countrycode" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in Country Code</small></div>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        <div class="col-md-12 ">
                                            Enquiry/Feedback details
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="message" rows="5" placeholder="" class="form-control  element-transparent" type="text"></textarea> 
                                        </div>
                                    </div> -->
                                    <style>
                                           input[type='radio']{
                                                margin:0;
                                            }

                                            input[type='radio'], label{   
                                               display:inline;
                                               vertical-align:top;
                                            }
                                        
                                       
                                        .radio-inline{top:-10px;}
                                        
                                        label {
                                            display: block;
                                            margin-left: 0px;
                                        }
                                        
                                        .clear {clear: both;}
                                        
                                        .form-group a {color:black;}
                                        .form-group a:hover {color:black;}
                                        

                                    </style>

                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12">
                                            Captcha
                                        </div>
                                        <div class="col-md-8 col-xs-8">
                                            <input name="captcha" placeholder="" class="form-control  element-transparent " type="text" size="3" data-bv-field="captcha"><i class="form-control-feedback bv-no-label" data-bv-icon-for="captcha" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="captcha" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter captcha </small></div>
                                        <div class="col-md-4 offset-md-8 col-xs-4 ">
                                            <img src="captcha.php">
                                        </div>
                                    </div> -->

                                    <!-- <div class="form-group">
                                        <div class="col-md-12">
                                            <div id="divTermsWrap" style="height: 150px; overflow: auto">
                                                <i>Your Personal Data which you provide CapitaLand Limited (“<b>CapitaLand</b>”) in this form (“<b>Personal Data</b>”) will be processed, collected, used and disclosed by CapitaLand, as well as, under the instructions of CapitaLand, by its related corporations and affiliates (the “<b>Group</b>”), and authorised agents and authorised service providers of the Group, in compliance with applicable data protection laws and regulations.<br>
                                                <br>
                                                Your Personal Data will be processed, collected, used and disclosed by the Group for the purposes of enabling the Group to review, develop, improve, manage the delivery of, and enhance the products and services of the Group, including analysing future customer needs, conducting market research and data analytics, as well as the other purposes notified to you in the CapitaLand Data Protection Policy (the “<b>DPP</b>”) (accessible <a href="/international/en/legal-notices/privacy-policy.html" target="_blank">here</a>) (the “<b>Purposes</b>”).<br>
                                                <br>
                                                More information on your rights, the transfer of your Personal Data and our data retention policies is set out in the DPP which you must carefully read.&nbsp;Your consent can be withdrawn at any time by contacting our Data Protection Officers, at the contact details set out in the DPP.</i>
                                            </div>
                                        </div>
                                    </div> -->
                                    
                                    <!-- <div class="form-group has-feedback"><br>
                                        
                                        <div class="col-md-12 text-uppercase  clsGold"></div>
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label class="" for="tc">
                                                <input id="tc" type="checkbox" name="tc" value="TC Checked" data-bv-field="tc"><i class="form-control-feedback" data-bv-icon-for="tc" style="display: none;"></i> 
                                                    I hereby agree to receive future communication and newsletters from CapitaLand and I have read and agreed with the Terms and Privacy Policy.
                                                </label>
                                            </div>
                                            <div class="visible-xs"></div>
                                            <div class="k-invalid-msg" data-for="tc">&nbsp;</div>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="tc" data-bv-result="NOT_VALIDATED" style="display: none;">Please check to accept Terms &amp; Conditions </small></div>
                                        <div class="t_c"></div>
                                    </div> -->
                                    
                                    <!-- <div class="form-group">
                                        <div id="divPushAboveSubmit"></div>
                                    </div>
                                   
                                    <div class="form-group text-center">

                                        <div class="col-md-6  col-xs-12  clsSubmitVerySmall- DivBtnSubmit">
                                            <button type="submit" class="btn  btn-custom btnSubmit btnSubmitVN">SUBMIT <span class="glyphicon glyphicon-play"></span></button>
                                           
                                        </div>
                                        <div class="col-md-6 col-xs-12 clsSubmitVerySmall- divBtnReset">
                                            
                                            <button type="reset" class="btn btn-custom btnReset btnResetVN">RESET <span class="glyphicon glyphicon-refresh"></span></button>
                                        </div>
                                        
                                    </div> -->


                                    
                                    
                                    
                                    
                                </div> <!-- OSR - FROM END -->
                                
                            </fieldset>
                        </div>
                        </form>
                         
                    </div>
                    
                     
                    
                    
                </div>
                
                
            </section>




            <form class="well form-horizontal  bv-form" action="registersend.php" method="post" id="contact_form" accept-charset="UTF-8" novalidate="novalidate" style="color: black;"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                            <input type="hidden" name="showform" value="true">
                            
                        <div id="frmWrap">
                            <fieldset>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- OSR - FROM END -->
                                    
                                    <!-- <div class="form-group">
                                        <div class="col-md-12 text-uppercase  clsGold"><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">REGISTRATION</span> &nbsp;&nbsp;&nbsp;<span class="visible-xs"></span><span class="clsMandatoryLbl headerTxt"></span><hr></div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-12 text-uppercase  clsGold" ><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">Đăng kí nhận thông tin.</span> &nbsp;&nbsp;&nbsp;<span class="visible-xs"></span><span class="clsMandatoryLbl headerTxt"></span><hr></div>
                                    </div>

                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12">
                                            Full name
                                        </div>
                                        <div class="col-md-3"> 
                                            <select name="saluation" class="form-control selectpicker-  element-transparent splSelect ">
                                                <option value="MR.">MR.</option>
                                                <option value="MRS.">MRS.</option>
                                                <option value="MS.">MS.</option>
                                                <option value="DR.">DR.</option>
                                                <option value="PROF.">PROF.</option>
                                            </select>
                                            <span class="visible-xs"><br></span>
                                        </div>

                                        <div class="col-md-9 inputGroupContainer">
                                            <input name="full_name" placeholder="" class="form-control  element-transparent clsGold" type="text" data-bv-field="full_name"><i class="form-control-feedback bv-no-label" data-bv-icon-for="full_name" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="stringLength" data-bv-for="full_name" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter a value with valid length</small><small class="help-block" data-bv-validator="notEmpty" data-bv-for="full_name" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in your Full Name</small></div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            Tên đầy đủ
                                        </div>
                                        <div class="col-md-3"> 
                                            <select name="saluation" class="form-control selectpicker-  element-transparent splSelect " >
                                                <option  value="MR.">Ông</option>
                                                <option  value="MRS.">Bà</option>
                                                <option  value="MS.">Anh</option>
                                                <option  value="DR.">Chị</option>
                                            </select>
                                            <span class="visible-xs"><br></span>
                                        </div>

                                        <div class="col-md-9 inputGroupContainer">
                                            <input  name="full_name"  placeholder="" class="form-control  element-transparent clsGold"  type="text">
                                        </div>
                                    </div>

                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12 ">
                                            Contact number
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input name="phonenumber" placeholder="" class="form-control  element-transparent" type="text" data-bv-field="phonenumber"><i class="form-control-feedback bv-no-label" data-bv-icon-for="phonenumber" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="phonenumber" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in your Phone Number</small></div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Số điện thoại
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input  name="phonenumber"  placeholder="" class="form-control  element-transparent"  type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Email
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input  name="email"  placeholder="" class="form-control  element-transparent"  type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Mã vùng/ khu vực
                                        </div>
                                        <div class="col-md-12">
                                            <input  name="countrycode"  placeholder="" class="form-control  element-transparent"  type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            Yêu cầu / Chi tiết phản hồi
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="message" rows="5" placeholder="" class="form-control  element-transparent"  type="text"></textarea> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            Captcha
                                        </div>
                                        <div class="col-md-8 col-xs-8">
                                            <input  name="captcha"  placeholder="" class="form-control  element-transparent "  type="text" size="3">
                                        </div>
                                        <div class="col-md-4 offset-md-8 col-xs-4 ">
                                            <img src="captcha.php"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div id="divTermsWrap" style="height: 200px; overflow: auto">
                                                <i>Thông Tin Cá Nhân bạn cung cấp cho CapitaLand Limited (“<b>CapitaLand</b>”) trong mẫu này (“Thông Tin Cá Nhân”) sẽ được xử lý, thu thập, sử dụng và công bố bởi CapitaLand, cũng như, dưới chỉ định của CapitaLand, bởi các bên thuộc tập đoàn và công ty con (“<b>Tập Đoàn</b>”), đối tác và các bên cung cấp dịch vụ được uỷ quyền bởi Tập Đoàn, chiếu theo luật lệ bảo mật thông tin.&nbsp;<br>
                                                <br>
                                                Thông Tin Cá Nhân sẽ được xử lý, thu thập, sử dụng và công bố bởi Tập Đoàn cho mục đích rà soát, phát triển, cải thiện, quản lý sản phẩm và dịch vụ cung cấp nhằm nâng cao chất lượng, phân tích nhu cầu khách hàng, khảo sát thị trường và phân tích dữ liệu, cũng như các mục đích khác thông báo cho bạn theo Chính Sách Bảo Vệ Dữ Liệu Của CapitaLand (“<b>Chính Sách Bảo Vệ Dữ Liệu</b>”) (vào <a href="https://www.capitaland.com/international/en/legal-notices/privacy-policy.html">liên kết này</a>) (“<b>Mục Đích</b>”). <br>
                                                <br>
                                                Nhiều thông tin hơn cung cấp cho bạn, bạn phải đọc kỹ chính sách Thông Tin Cá Nhân và lưu giữ thông tin được đưa ra trong Chính Sách Bảo Vệ Dữ Liệu. Bạn có quyền từ chối cung cấp thông tin vào bất cứ lúc nào bằng việc liên hệ với Các Chuyên Viên Bảo Vệ Dữ Liệu, với chi tiết liên hệ đã đề cập trong Chính Sách Bảo Vệ Dữ Liệu.<br>
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><br />
                                        
                                        <div class="col-md-12 text-uppercase  clsGold" ></div>
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label class="" for="tc">
                                                <input id="tc" type="checkbox" name="tc" value="TC Checked"> 
                                                    Tôi đồng ý nhận các thông tin, tin tức mới từ CapitaLand và tôi đã đọc, cũng như đồng thuận với các điều khoản bảo mật riêng tư.
                                                </label>
                                            </div>
                                            <div class="visible-xs"></div>
                                            <div class="k-invalid-msg" data-for="tc">&nbsp;</div>
                                        </div>
                                        <div class="t_c"></div>
                                    </div>
                                    <div class="form-group">
                                        <div id="divPushAboveSubmit"></div>
                                    </div>
                                   
                                    <div class="form-group text-center">

                                        <div class="col-md-6  col-xs-12  clsSubmitVerySmall- DivBtnSubmit">
                                            <button type="submit" class="btn  btn-custom btnSubmit btnSubmitVN" >CHẤP NHẬN <span class="glyphicon glyphicon-play"></span></button>
                                           
                                        </div>
                                        <div class="col-md-6 col-xs-12 clsSubmitVerySmall- divBtnReset">
                                            
                                            <button type="reset" class="btn btn-custom btnReset btnResetVN" >THỰC HIỆN LẠI <span class="glyphicon glyphicon-refresh"></span></button>
                                        </div>
                                        
                                    </div>
                                   <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12 ">
                                            Email
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <input name="email" placeholder="" class="form-control  element-transparent" type="text" data-bv-field="email"><i class="form-control-feedback bv-no-label" data-bv-icon-for="email" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in your Email address</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in a valid Email address</small></div>
                                    </div> -->
                                    
                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12 ">
                                            Country of residence
                                        </div>
                                        <div class="col-md-12">
                                            <input name="countrycode" placeholder="" class="form-control  element-transparent" type="text" data-bv-field="countrycode"><i class="form-control-feedback bv-no-label" data-bv-icon-for="countrycode" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="countrycode" data-bv-result="NOT_VALIDATED" style="display: none;">Please key-in Country Code</small></div>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        <div class="col-md-12 ">
                                            Enquiry/Feedback details
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="message" rows="5" placeholder="" class="form-control  element-transparent" type="text"></textarea> 
                                        </div>
                                    </div> -->
                                    <style>
                                           input[type='radio']{
                                                margin:0;
                                            }

                                            input[type='radio'], label{   
                                               display:inline;
                                               vertical-align:top;
                                            }
                                        
                                       
                                        .radio-inline{top:-10px;}
                                        
                                        label {
                                            display: block;
                                            margin-left: 0px;
                                        }
                                        
                                        .clear {clear: both;}
                                        
                                        .form-group a {color:black;}
                                        .form-group a:hover {color:black;}
                                        

                                    </style>

                                    <!-- <div class="form-group has-feedback">
                                        <div class="col-md-12">
                                            Captcha
                                        </div>
                                        <div class="col-md-8 col-xs-8">
                                            <input name="captcha" placeholder="" class="form-control  element-transparent " type="text" size="3" data-bv-field="captcha"><i class="form-control-feedback bv-no-label" data-bv-icon-for="captcha" style="display: none;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="captcha" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter captcha </small></div>
                                        <div class="col-md-4 offset-md-8 col-xs-4 ">
                                            <img src="captcha.php">
                                        </div>
                                    </div> -->

                                    <!-- <div class="form-group">
                                        <div class="col-md-12">
                                            <div id="divTermsWrap" style="height: 150px; overflow: auto">
                                                <i>Your Personal Data which you provide CapitaLand Limited (“<b>CapitaLand</b>”) in this form (“<b>Personal Data</b>”) will be processed, collected, used and disclosed by CapitaLand, as well as, under the instructions of CapitaLand, by its related corporations and affiliates (the “<b>Group</b>”), and authorised agents and authorised service providers of the Group, in compliance with applicable data protection laws and regulations.<br>
                                                <br>
                                                Your Personal Data will be processed, collected, used and disclosed by the Group for the purposes of enabling the Group to review, develop, improve, manage the delivery of, and enhance the products and services of the Group, including analysing future customer needs, conducting market research and data analytics, as well as the other purposes notified to you in the CapitaLand Data Protection Policy (the “<b>DPP</b>”) (accessible <a href="/international/en/legal-notices/privacy-policy.html" target="_blank">here</a>) (the “<b>Purposes</b>”).<br>
                                                <br>
                                                More information on your rights, the transfer of your Personal Data and our data retention policies is set out in the DPP which you must carefully read.&nbsp;Your consent can be withdrawn at any time by contacting our Data Protection Officers, at the contact details set out in the DPP.</i>
                                            </div>
                                        </div>
                                    </div> -->
                                    
                                    <!-- <div class="form-group has-feedback"><br>
                                        
                                        <div class="col-md-12 text-uppercase  clsGold"></div>
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label class="" for="tc">
                                                <input id="tc" type="checkbox" name="tc" value="TC Checked" data-bv-field="tc"><i class="form-control-feedback" data-bv-icon-for="tc" style="display: none;"></i> 
                                                    I hereby agree to receive future communication and newsletters from CapitaLand and I have read and agreed with the Terms and Privacy Policy.
                                                </label>
                                            </div>
                                            <div class="visible-xs"></div>
                                            <div class="k-invalid-msg" data-for="tc">&nbsp;</div>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="tc" data-bv-result="NOT_VALIDATED" style="display: none;">Please check to accept Terms &amp; Conditions </small></div>
                                        <div class="t_c"></div>
                                    </div> -->
                                    
                                    <!-- <div class="form-group">
                                        <div id="divPushAboveSubmit"></div>
                                    </div>
                                   
                                    <div class="form-group text-center">

                                        <div class="col-md-6  col-xs-12  clsSubmitVerySmall- DivBtnSubmit">
                                            <button type="submit" class="btn  btn-custom btnSubmit btnSubmitVN">SUBMIT <span class="glyphicon glyphicon-play"></span></button>
                                           
                                        </div>
                                        <div class="col-md-6 col-xs-12 clsSubmitVerySmall- divBtnReset">
                                            
                                            <button type="reset" class="btn btn-custom btnReset btnResetVN">RESET <span class="glyphicon glyphicon-refresh"></span></button>
                                        </div>
                                        
                                    </div> -->


                                    
                                    
                                    
                                    
                                </div> <!-- OSR - FROM END -->
                                
                            </fieldset>
                        </div>
                        </form>