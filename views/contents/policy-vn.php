<section class="section-policy">
   <div class="container">
      <div class="policy__block">
         <h3 class="policy__title text-center">Chính Sách Bảo Vệ Thông Tin Cá Nhân</h3>

         <div class="policy__content">
            <div class="policy__item">
               <p>1. Chính Sách Bảo Vệ Thông Tin Cá Nhân (“Chính Sách”) này quy định cách quản lý Thông Tin Cá Nhân do chúng tôi, CÔNG TY TNHH CẢNH SÔNG , các công ty con, công ty liên kết, đơn vị phụ thuộc liên quan (“chúng tôi” hoặc “của chúng tôi”) tuỳ thuộc vào ngữ cảnh) nắm giữ. Chúng tôi tôn trọng Thông Tin Cá Nhân và quyền riêng tư của khách hàng và cam kết tuân thủ các quy định của pháp luật Việt Nam về bảo vệ thông tin cá nhân. Khách hàng vui lòng đọc Chính Sách này để biết và hiểu mục đích thu thập, sử dụng và tiết lộ Thông Tin Cá Nhân. Chính Sách này bổ sung nhưng không thay thế bất kỳ sự đồng ý nào trước đây mà khách hàng có thể đã cung cấp trước cho chúng tôi liên quan đến Thông Tin Cá Nhân của khách hàng, và sự đồng ý của khách hàng là bổ sung cho các quyền mà chúng tôi được quyền thu thập, sử dụng hoặc tiết lộ Thông Tin Cá Nhân của khách hàng. Chính Sách này không tác động đến bất kỳ quyền nào mà chúng tôi có được theo quy định pháp luật liên quan đến việc thu thập, sử dụng hoặc tiết lộ Thông Tin Cá Nhân của khách hàng.</p>
            </div>
            <div class="policy__item">
               <div class="title">2. Thông Tin Cá Nhân</div>
               <div class="content">
                  <p>“Thông Tin Cá Nhân” là bất kỳ thông tin hoặc dữ liệu xác định danh tính của khách hàng, bao gồm: tên, tuổi, số chứng minh nhân dân/hộ chiếu/căn cước công dân, số điện thoại, địa chỉ nhà riêng, địa chỉ bưu phẩm, địa chỉ thư điện tử, thông tin y tế, số tài khoản, thông tin về các giao dịch thanh toán cá nhân của khách hàng và bất kỳ thông tin khác liên quan đến khách hàng được cung cấp cho chúng tôi dưới bất kỳ hình thức nào hoặc các hình thức tương tác khác giữa khách hàng và chúng tôi, nhưng không bao gồm thông tin liên hệ công việc và thông tin mà khách hàng đã tự công bố trên các phương tiện truyền thông.</p>
                  <p>Nếu Thông Tin Cá Nhân đã được chúng tôi thu thập, sử dụng hoặc công bố tại nơi mà có các yêu cầu cụ thể hoặc trường hợp ngoại trừ cho định nghĩa của Thông Tin Cá Nhân, bao gồm Thông Tin Cá Nhân nhạy cảm, theo quy định của pháp luật Việt Nam, chúng tôi sẽ tuân thủ theo yêu cầu đó theo các quy định của pháp luật Việt Nam về bảo vệ thông tin cá nhân.</p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">3. Mục đích và phạm vi thu thập Thông Tin Cá Nhân</div>
               <div class="content">
                  <p> Khi khách hàng gửi Thông Tin Cá Nhân thông qua trang web này hoặc đến địa chỉ thư điện tử được cung cấp tại trang web này, khách hàng được xem là đã đồng ý và chấp thuận đối với việc thu thập, sử dụng và tiết lộ tất cả các Thông Tin Cá Nhân đó theo cách được mô tả trong Chính Sách này. Chúng tôi sẽ, tuỳ thuộc vào pháp luật hiện hành, sử dụng và tiết lộ Thông Tin Cá Nhân của khách hàng cho các mục đích sau đây:</p>
                  <p>(a)  cung cấp sản phẩm hoặc dịch vụ khách hàng mong muốn;</p>
                  <p>(b)  giúp chúng tôi rà soát, phát triển, cải thiện, quản lý việc cung cấp và – đến mức độ yêu cầu sử dụng Thông Tin Cá Nhân – nâng cao chất lượng sản phẩm và dịch vụ của chúng tôi, bao gồm phân tích nhu cầu khách hàng trong tương lai, thực hiện nghiên cứu thị trường và phân tích dữ liệu;</p>
                  <p>(c)  giao tiếp với khách hàng và phản hồi thắc mắc, yêu cầu và khiếu nại của khách hàng;</p>
                  <p>(d)  cung cấp thông tin về sản phẩm và dịch vụ của chúng tôi mà khách hàng có thể quan tâm;</p>
                  <p>(e)  giải quyết tranh chấp, thực hiện và tạo thuận lợi cho các cuộc điều tra và tố tụng;</p>
                  <p>(f)   bảo vệ và thi hành quyền và nghĩa vụ theo pháp luật và theo hợp đồng của chúng tôi;</p>
                  <p>(g)  ngăn ngừa, phát hiện và điều tra tội phạm, bao gồm gian lận và rửa tiền, phân tích và quản lý rủi ro thương mại khác;</p>
                  <p>(h)  quản lý cơ sở hạ tầng và hoạt động kinh doanh của chúng tôi và tuân thủ chính sách và quy trình nội bộ; và</p>
                  <p>(i)    tuân thủ nguyên tắc, pháp luật, quy định được áp dụng hoặc tập quán hoặc hướng dẫn thi hành hoặc hỗ trợ việc thực thi quy định pháp luật và hoạt động điều tra của cơ quan có thẩm quyền liên quan.</p>
                  <p>Trong một số trường hợp, việc cung cấp và xử lý Thông Tin Cá Nhân của khách hàng có thể theo quy định của pháp luật và/ hoặc theo hợp đồng, hoặc cần thiết để thực hiện hợp đồng mà khách hàng đã đồng ý với chúng tôi hoặc cung cấp dịch vụ theo yêu cầu của khách hàng.</p>
                  <p>Thông Tin Cá Nhân của khách hàng sẽ được bảo vệ và bảo mật tuỳ thuộc vào quy định pháp luật, Thông Tin Cá Nhân của khách hàng có thể, tùy thuộc vào các sản phẩm hoặc dịch vụ liên quan, được sử dụng, chia sẻ, tiết lộ hoặc chuyển giao cho các bên thứ ba. </p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">4. Lưu trữ Thông Tin Cá Nhân</div>
               <div class="content">
                  <p> Chúng tôi có thể lưu trữ Thông Tin Cá Nhân của khách hàng trong thời gian cần thiết cho các mục đích thu thập và trong hầu hết các trường hợp, tối đa 7 năm, trừ khi được pháp luật quy định khác hoặc để bảo vệ trước các khiếu nại pháp lý. Khi chúng tôi không còn nhu cầu cho Thông Tin Cá Nhân của khách hàng cho các mục đích nêu trên, chúng tôi sẽ ngừng lưu trữ Thông Tin Cá Nhân đó theo chính sách lưu trữ nội bộ của chúng tôi.</p>
                  <p></p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">5. Chính sách cookie</div>
               <div class="content">
                  <p> Chúng tôi sử dụng cookie khi khách hàng sử dụng trang web của chúng tôi. Trong nhiều trường hợp, thông tin chúng tôi thu thập bằng cookie được sử dụng trên cơ sở tổng hợp và theo cách không thể nhận dạng. Ví dụ: chúng tôi có thể sử dụng thông tin được thu thập để hiểu rõ hơn lượng truy cập trang web và để cải thiện trải nghiệm trang web cho người dùng của chúng tôi.</p>
                  <p></p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">6. Trang của bên thứ ba</div>
               <div class="content">
                  <p>Trang web của chúng tôi có thể chứa các liên kết dẫn đến các trang web khác được điều hành bởi các bên thứ ba, ví dụ như các đối tác kinh doanh của chúng tôi. Chúng tôi không chịu trách nhiệm đối với các thông lệ về quyền riêng tư của các trang web được điều hành bởi các bên thứ ba được liên kết với trang web của chúng tôi, trừ khi đó là yêu cầu bắt buộc theo luật và quy định bảo vệ dữ liệu hiện hành. Chúng tôi khuyến khích khách hàng tìm hiểu về các chính sách riêng tư của các bên thứ ba như vậy. Khi khách hàng đã rời khỏi trang web của chúng tôi, khách hàng nên kiểm tra chính sách riêng tư của các bên thứ ba đó tại các trang web tương ứng của họ để xác định cách họ sẽ xử lý bất kỳ thông tin thu thập từ khách hàng. </p>
                  <p></p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">7. Mô-đun của bên thứ ba</div>
               <div class="content">
                  <p>Trang web của chúng tôi sử dụng API Google Maps từ Công ty Google. Để biết thêm thông tin về các quy tắc thực hành quyền riêng tư của Google, vui lòng xem Chính Sách Riêng Tư của Google có sẵn tại http://www.google.com/polaho/privacy hoặc URL khác như Google có thể cung cấp theo thời gian. Việc Google thu thập, sử dụng và công bố Thông Tin Cá Nhân của khách hàng thông qua chức năng như vậy được tích hợp trong trang web của chúng tôi, phải tuân theo Chính Sách Riêng Tư của Google. </p>
                  <p></p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">8. Những người hoặc tổ chức có thể được tiếp cận với Thông Tin Cá Nhân</div>
               <div class="content">
                  <p> Chỉ có các nhân viên của chúng tôi được chỉ định và các công ty cung cấp dịch vụ cho chúng tôi được phép truy cập Thông Tin Cá Nhân của khách hàng và chỉ nhằm phục vụ các mục đích được nêu ra ở mục 3.</p>
                  <p></p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">9. Liên hệ với chúng tôi</div>
               <div class="content">
                  <p> Nếu khách hàng có bất kỳ câu hỏi nào về Chính Sách này hoặc có thắc mắc liên quan đến Thông Tin Cá Nhân của khách hàng hoặc khách hàng muốn có quyền truy cập và / hoặc chỉnh sửa hồ sơ Thông Tin Cá Nhân của mình, vui lòng liên hệ:</p>
                  <p> CÔNG TY TNHH CẢNH SÔNG</p>
                  <p>Địa chỉ: Lầu 8, Tòa nhà Vista, số 628C Xa lộ Hà Nội, Phường An Phú, Quận 2, Thành phố Hồ Chí Minh, Việt Nam.</p>
               </div>
            </div>
             <div class="policy__item">
               <div class="title">10. Rà soát Chính Sách</div>
               <div class="content">
                  <p>Chính Sách này sẽ được chúng tôi rà soát theo từng thời điểm. Chúng tôi cũng có thể cập nhật Chính Sách này theo từng thời điểm trên cơ sở xem xét các luật và công nghệ mới, thay đổi hoạt động và thực tiễn và môi trường kinh doanh thay đổi. Nếu khách hàng không chắc chắn liệu khách hàng có đang đọc phiên bản mới nhất hay không, vui lòng liên hệ với chúng tôi. Trong trường hợp có bất kỳ mâu thuẫn nào giữa bản tiếng Anh và các bản dịch khác của Chính Sách này, phiên bản tiếng Anh sẽ được ưu tiên áp dụng. </p>
                  <p></p>
               </div>
            </div>

         </div>
      </div>
   </div>
</section>