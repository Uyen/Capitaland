 <div class="wrapper media-page">
        
         <!-- Section - Media -->
         <section class="section section-auto media-page" id="media" data-section-name="media">
            <article class="d-flex flex-wrap align-items-center">
               <div class="d-flex col-12 p-4 justify-content-center">
                  <div class="article-inner col-12 col-lg-10 text-left p-lg-5 d-flex flex-column justify-content-center">
                     <h4 class="headline">
                        <span class="ml-0">
                        <small>Video lifestyle</small>
                        </span>
                     </h4>
                     <div class="media-wrapper">
                        <ul class="d-flex flex-wrap pt-4 align-items-stretch">
                          
                           <li class="item">
                              <!-- de-la-sol-video-emergence-cbd-lite -->
                              <a data-toggle="modal" data-target="#de-la-sol-video-emergence-cbd-lite">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_002.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>26 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>De La Sol by CapitaLand: The Emergence of the CBD Lite</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- de-la-sol-video-feel-the-pulse -->
                              <a data-toggle="modal" data-target="#de-la-sol-video-feel-the-pulse">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_004.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>22 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>Feel The Pulse Of The CBD Lite. All In One Place</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- event-project-introduction -->
                              <a data-toggle="modal" data-target="#event-project-introduction">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_006.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>20 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>De La Sol Project Introduction</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- sales-gallery-grand-opening -->
                              <a data-toggle="modal" data-target="#sales-gallery-grand-opening">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_005.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>12 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>The Grand Opening of Sales Gallery & Show Suites</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- show-suites-vip-preview -->
                              <a data-toggle="modal" data-target="#show-suites-vip-preview">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail_007.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>9 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>Show Suites VIP Preview</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li class="item">
                              <!-- signing-ceremony-4-may -->
                              <a data-toggle="modal" data-target="#signing-ceremony-4-may">
                                 <div class="media-thumb"><img class="img-fluid" src="img/media/thumbnail.jpg"></div>
                                 <div class="media-info">
                                    <div class="media-date"><small>4 May 2018</small></div>
                                    <div class="media-caption">
                                       <p>Signing Ceremony of Strategic Partnership with Exclusive Distribution Agencies</p>
                                    </div>
                                    <div class="media-arrow d-flex justify-content-end">
                                       <div class="lg-next lg-icon"></div>
                                    </div>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </article>
         </section>
      </div>
      <!-- Media Modals [Auto-generated by Jekyll]-->
      <!-- Modal for "Signing Ceremony of Strategic Partnership with Exclusive Distribution Agencies" -->
      <div class="modal modal-media fade" id="signing-ceremony-4-may" tabindex="-1" role="dialog" aria-labelledby="signing-ceremony-4-may" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="signing-ceremony-4-may-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media04.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#signing-ceremony-4-may-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#signing-ceremony-4-may-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>4 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "Show Suites VIP Preview" -->
      <div class="modal modal-media fade" id="show-suites-vip-preview" tabindex="-1" role="dialog" aria-labelledby="show-suites-vip-preview" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="show-suites-vip-preview-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/SRXmhlvreVo?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#show-suites-vip-preview-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#show-suites-vip-preview-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>9 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "The Grand Opening of Sales Gallery & Show Suites" -->
      <div class="modal modal-media fade" id="sales-gallery-grand-opening" tabindex="-1" role="dialog" aria-labelledby="sales-gallery-grand-opening" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="sales-gallery-grand-opening-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/09.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/AWhF3EDkxaM?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#sales-gallery-grand-opening-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#sales-gallery-grand-opening-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>12 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "De La Sol Project Introduction" -->
      <div class="modal modal-media fade" id="event-project-introduction" tabindex="-1" role="dialog" aria-labelledby="event-project-introduction" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="event-project-introduction-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <div class="carousel-item active">
                           <img src="img/media/01.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/02.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/03.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/04.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/05.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/06.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/07.jpg" class="d-block w-100">
                        </div>
                        <div class="carousel-item ">
                           <img src="img/media/08.jpg" class="d-block w-100">
                        </div>
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch ">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/EOID3Ir4NWE?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                     <!-- Carousel Controls -->
                     <a class="carousel-control-prev" href="#event-project-introduction-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#event-project-introduction-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>20 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "Feel The Pulse Of The CBD Lite. All In One Place" -->
      <div class="modal modal-media fade" id="de-la-sol-video-feel-the-pulse" tabindex="-1" role="dialog" aria-labelledby="de-la-sol-video-feel-the-pulse" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-feel-the-pulse-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch active">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/4wHTRsZUDks?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>22 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal for "De La Sol by CapitaLand: The Emergence of the CBD Lite" -->
      <div class="modal modal-media fade" id="de-la-sol-video-emergence-cbd-lite" tabindex="-1" role="dialog" aria-labelledby="de-la-sol-video-emergence-cbd-lite" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <!-- Close Button -->
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
               <!-- Carousel -->
               <div class="media-thumb">
                  <div id="de-la-sol-video-emergence-cbd-lite-carousel" class="carousel slide" data-interval="false">
                     <!-- Carousel Slides -->
                     <div class="carousel-inner">
                        <!-- Images -->
                        <!-- Videos -->
                        <div class="carousel-item align-self-stretch active">
                           <iframe class="w-100" width="100%" height="519.8" src="https://www.youtube.com/embed/AJ5TOfAHBlk?rel=0&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Caption -->
               <div class="media-info">
                  <div class="media-date"><small>26 May 2018</small></div>
                  <div class="media-arrow d-flex justify-content-end">
                     <div class="lg-next lg-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Duplicate the following snippet to add new media modals. -->
      <!--
         <div class="modal modal-media fade" id="[ID HERE]" tabindex="-1" role="dialog" aria-labelledby="[ID HERE]" aria-hidden="true">
           <div class="modal-dialog modal-lg">
             <div class="modal-content">
         
               <!-- Close Button - -> (remove this line or remove space in '- ->')
               <div class="crossBtn white" data-dismiss="modal" aria-label="Close"><i></i></div>
         
               <!-- Carousel - -> (remove this line or remove space in '- ->')
               <div class="media-thumb">
                 <div id="[ID HERE]-carousel" class="carousel slide" data-interval="false">
         
                   <!-- Carousel Slides - -> (remove this line or remove space in '- ->')
                   <div class="carousel-inner">
         
                     <!-- Images [Duplicate as many as needed] - -> (remove this line or remove space in '- ->')
                     <div class="carousel-item active">
                       <img src="img/media/01.jpg" class="d-block w-100">
                     </div>
                     <div class="carousel-item ">
                       <img src="img/media/02.jpg" class="d-block w-100">
                     </div>
                     <div class="carousel-item ">
                       <img src="img/media/03.jpg" class="d-block w-100">
                     </div>
         
                     <!-- Videos - -> (remove this line or remove space in '- ->')
                     <div class="carousel-item align-self-stretch ">
                       <iframe
                         class="w-100"
                         width="100%"
                         height="519.8"
                         src="https://www.youtube.com/embed/[YOUTUBE ID HERE]?rel=0&amp;showinfo=0"
                         frameborder="0"
                         allow="autoplay; encrypted-media"
                         allowfullscreen>
                       </iframe>
                     </div>
         
                   </div>
         
                   <!-- Carousel Controls - -> (remove this line or remove space in '- ->')
                   <a class="carousel-control-prev" href="#show-suites-vip-preview-carousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                   </a>
                   <a class="carousel-control-next" href="#show-suites-vip-preview-carousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                   </a>
         
                 </div>
               </div>
         
               <!-- Caption - -> (remove this line or remove space in '- ->')
               <div class="media-info">
                 <div class="media-date"><small>[ DATE HERE ]</small></div>
                 <div class="media-arrow d-flex justify-content-end"><div class="lg-next lg-icon"></div></div>
               </div>
         
             </div>
           </div>
         </div>
         -->