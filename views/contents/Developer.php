   <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/developerhero.jpg"> 

                    <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div>
                </div>
                
                <img id="imgDevKeyVis-mobile" src="img/developer-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                <div  id="divPgKeyOuterTxtWrap">
                
                    <div id="divDevPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                            
                            <div id="divDevParllaxTxt1" class="clsHeader1">
                                   
                                    <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">Mở ra</span>
                                     <span class="clsHeaderBigFont clsHeader1 clsHeaderNormalFontViet">những </span> 
                                    <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet"> giới hạn</span> 
                                
                                </div>

                                <div id="divDevParllaxTxt2" class="divDevPGTxt2Viet">
                                  
                                    <span class="clsHeaderBigFont clsHeader1   clsHeaderNormalFontViet">CHƯA TỪNG</span> 
                                    <span class="clsHeaderBigFont_vn clsHeader1 clsHeaderItalicFontViet">được chạm tới</span>
                                    
                                    
                                </div>
                            
                            
                        </div>
                        

                    </div>

                </div>
            </div>    
           

        </section>

        <section class="developer-slider parllaxpadding" >
            <div class="container " id="deverloperPgContainer" style="background-color:white;padding:0">
                
                <style>

                    #deverloperPgContainer{width: 85%;}
          .msd_slider{max-width:750px; width:100%;}
          .msd_slider_col_box{padding:50px 60px!important;padding-bottom: 40px !important;}
          .msd_slider_col_box img{}
          .msd_slider_colset{}
          .msd_slider_col{width:100%!important;}
                    
                   
                    
                </style>

                
                <div style="display:block; width:100%; height:50px;"></div>
                
                
                
                <div class="mod_location_slider" id="mod_location_slider1">
                    
                    <div class="mod_location_slider_box">
                
                    
                        <div class="mod_location_slider_item active">
                            <div class="thumb">
                                <img src="img/developer11.png" alt="location_location_pic1"/>
                                
                                <div id="" class="clsBarInner zoomMap_facilities1" style="display:none;">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc">&nbsp;</div>
                        </div><!-- END mod_location_slider_item -->
                        
                        
                        <div class="mod_location_slider_item">
                            <div class="thumb">
                            
                                <img src="img/developer2.png" alt="location_location_pic1"/>
                                
                                <div id="" class="clsBarInner zoomMap_facilities2" style="display:none;">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc">&nbsp;</div>
                        </div><!-- END mod_location_slider_item -->
                        
                        <div class="mod_location_slider_item">
                            <div class="thumb">
                            
                                <img src="img/developer3.png" alt="location_location_pic1"/>
                                
                                <div id="" class="clsBarInner zoomMap_facilities3" style="display:none;">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc">&nbsp;</div>
                        </div><!-- END mod_location_slider_item -->
                        
                        
                        <div class="mod_location_slider_item">
                            <div class="thumb">
                            
                                <img src="img/developer4.png" alt="location_location_pic1"/>
                                
                                <div id="" class="clsBarInner zoomMap_facilities4" style="display:none;">
                                    <div class=" col-xs-12 text-center clsNormalFont">
                                        <a>CLICK TO VIEW
                                        <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>  
                                  
                            </div>
                            
                            <div class="desc">&nbsp;</div>
                        </div><!-- END mod_location_slider_item -->
                        
                        
                        <div class="controlx">
                          <div class="controlx_box">
                            <div class="controlx_btn"></div>
                            <div class="controlx_btn right"></div>
                            <div class="clear"></div>
                          </div>
                        </div>
                
                
                    </div><!-- END mod_location_slider_box-->
                        
                <!-- Controls -->
                    </div><!-- END mod_location_slider-->

                
                
                
                
                
                
              <div id="secDeveloperPgPara" class="clsPurple2">
                  <p>
                   Được niêm yết và đặt trụ sở chính tại Singapore, CapitaLand là một trong những tập đoàn Bất động sản lớn nhất Châu Á. Tại đây, tập đoàn không ngừng tận dùng nguồn lực dồi dào, tối ưu chiến lược quản lý vốn, kết hợp cùng sự am hiểu thị 
                      trường và khả năng vận hành để phát triển nên các dự án Bất động sản cao cấp xuất sắc. Tập đoàn cũng đa dạng danh mục phát triển bất động sản của mình với rất nhiều công trình tổ hợp, trung tâm mua sắm, căn hộ dịch vụ, văn phòng và các dự án căn hộ.

                  
                  </p>
                  <p>
                  Việt Nam là một trong số các thị trường trọng điểm của CapitaLand tại châu Á. Tại Việt Nam, thị trường Bất động sản được thúc đẩy bởi nguồn vốn trong nước dồi dào, tốc độ đô thị hóa nhanh chóng, cùng dân số trẻ đang ngày càng phát triển. 
                      CapitaLand cam kết trở thành một nhà phát triển bất động sản bền vững tại Việt Nam. Tập đoàn CapitaLand hiện diện tại 6 thành phố lớn – Hồ Chí Minh, Hà Nội, Hải Phòng, Đà Nẵng, Bình Dương và Nha Trang với các dự án căn hộ nhà ở và căn hộ dịch vụ.

                  
                  </p>
                  <p>
                  
                  Với các dự án căn hộ, CapitaLand sở hữu danh mục lên tới gần 9,100 các căn hộ cao cấp qua 9 dự án tại TP Hồ Chí Minh và Hà Nội. Với các dự án căn hộ dịch vụ, 
                      The Ascott Limited– tập đoàn quản lý trực thuộc CapitaLand, đồng thời sở hữu danh mục hơn 4,000 căn hộ dịch vụ tại 19 dự án trên khắp 7 thành phố lớn, trở thành tập đoàn sở hữu – vận hành căn hộ dịch vụ lớn nhất tại Việt Nam.
                  </p>
                  <!--
                    <p>
                    CapitaLand is one of Asia’s largest real estate companies headquartered and listed in Singapore. The company leverages its significant asset base, design and development capabilities, active capital management strategies, 
                   extensive market network and operational capabilities to develop high-quality real estate products and services. Its diversified global real estate portfolio includes integrated developments, shopping malls, serviced residences, offices and homes.
                        </p>

                   <p>
                   Vietnam is one of CapitaLand’s key markets in Asia. The real estate market in Vietnam is supported by the country’s strong economic growth, rapid urbanization and a young and growing population. CapitaLand is committed to being 
                   a long-term real estate developer in the country. CapitaLand Group's current presence in Vietnam is in the six major cities – Ho Chi Minh City, Hanoi, Hai Phong, Da Nang, Binh Duong, and Nha Trang in the residential and serviced residences sectors.
                    </p>
                  
                  -->
                </div>              
                
                
                
                
                
            </div>
          
            
            
        </section>

        