



               <section id="divKeyVisual" class="clsKeyvisual">
                  <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                     <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2 hidden-xs" data-parallax="scroll" data-image-src="img/residenceshero.jpg">
                        <!-- <div class="clsArtImpression2">
                           <span class="artimp3">Artist's impression  </span>
                        </div> -->
                     </div>
                     <img id="imgResKeyVis-mobile" src="img/residences-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                     <div  id="divPgKeyOuterTxtWrap">
                        <div id="divResPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap">
                           <div id="divTemplateKeyVisParllaxTxtHeaderWrap" >
                              <div id="divResSlideHeadTxt1" class="clsHeader1">
                                    <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Sophistication</span> 
                            </div>

                                <div id="divResSlideHeadTxt2" >
                                  <span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inside</span>
                                    <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">and&nbsp;</span> 
                                    <span class="clsHeaderBigFont clsHeaderItalicFont clsHeader1">out</span>
                                </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <style>
                  #divBroc{position: relative;top: -45px;}
               </style>
               <section id="secRes2"  class="parllaxpadding">
                  <div class="container-fluid">
                     <div class="msd_maincontainer">
                      <!--   <div id="divBroc"  class="text-center">
                           <div class="clsBarInner ">
                              <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                 <a href="D1MENSION_Brochure.pdf" target="_blank">Tải tài liệu giới thiệu dự án  
                                 <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span> 
                                 </a>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div> -->
                        <div class="loc_map1"><img src="img/img_location/English14-15.jpg" alt="location_map1"/></div>
                        <div id="IntroBarInner" class="clsBarInner zoomMap1">
                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                              <a>CLICK TO VIEW
                              <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                           </div>
                           <div class="clear"></div>
                        </div>
                       <div class="loc_map1_legend" id="divResCommLegend">
                        
                        <!-- <div class="loc_map1_legend_set">
                            <div class="loc_map1_legend_cols_h1">COMMERCIAL AMENITIES<br><span>GROUND FLOOR</span></div>
                            
                            <div class="loc_map1_legend_cols">
                                <p class="lgnd_1"><span class="type1"></span> Entrance Plaza</p>
                                <p class="lgnd_2"><span></span>Ground Floor Parking</p>
                                <p class="lgnd_3"><span></span>Gourmet Lounge</p>
                            </div>
                            
                            <div class="loc_map1_legend_cols">
                               <p class="lgnd_4"><span></span>Gourmet Lounge</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="loc_map1_legend_set">
                            <div class="loc_map1_legend_cols_h1">RESIDENTIAL TOWER<br><span>GROUND FLOOR</span></div>
                            
                            <div class="loc_map1_legend_cols">
                                <p class="lgnd_5"><span class="type2"></span>Residential Porte Cochere (Drop-off)</p>
                                <p class="lgnd_6"><span class="type2"></span>Residential Vehicle Reception</p>
                                <p class="lgnd_7"><span class="type2"></span>The Playtime Park (Children's Playground)</p>
                                 <p class="lgnd_8"><span class="type2"></span>The Sanctuary (Residential Lounge)</p>
                                 <p class="lgnd_9"><span class="type2"></span>The Mailroom</p>
                            </div>
                            
                            <div class="loc_map1_legend_cols">
                                <p class="lgnd_10"><span class="type2"></span>The Library (Reading Lounge)</p>
                                <p class="lgnd_11"><span class="type2"></span>The Commons (Co-Working Space on Level 1)</p>
                                <p class="lgnd_12"><span class="type2"></span>Garden Walkway</p>
                                <p class="lgnd_13"><span class="type2"></span>Foliage Pavilions</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="loc_map1_legend_set">
                            <div class="loc_map1_legend_cols_h1">RESIDENTIAL TOWER SKY TERRACE<br><span>LEVEL 15 AND LEVEL 16</span></div>
                            <div class="loc_map1_legend_cols">
                                <p class="lgnd_14"><span class="type3"></span>Sky Gym (Level 15)</p>
                                <p class="lgnd_15"><span class="type3"></span>Sky Changing Rooms with Sauna &amp; Steam Bath (Level 15)</p>
                                <p class="lgnd_16"><span class="type3"></span>Sky Party House</p>
                                <p class="lgnd_17"><span class="type3"></span>Sky Wine &amp; Cigar Lounge</p>
                                <p class="lgnd_18"><span class="type3"></span>Sky Poolside Bar</p>
                                <p class="lgnd_19"><span class="type3"></span>Sky Jacuzzi</p>
                            </div>
                            
                            <div class="loc_map1_legend_cols">
                                <p class="lgnd_20"><span class="type3"></span>Sky Pool Deck</p>
                                <p class="lgnd_21"><span class="type3"></span>Sky Infinity Pool with Glass Bottom</p>
                                <p class="lgnd_22"><span class="type3"></span>Sky Rain Curtain</p>
                                <p class="lgnd_23"><span class="type3"></span>Sky Green Wall</p>
                            </div>
                            <div class="clear"></div>                   
                        </div> -->
                        
                        
                    
                    </div>
                        <!--   
                           <div id="" class="clsHeader1 loclabel" style="text-align:center;">
                               <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Tiện ích</span> 
                           </div>
                           
                           
                                                  <div class="mod_location_slider" id="mod_location_slider1">
                           
                           <div class="mod_location_slider_box">
                                           
                                                   
                               <div class="mod_location_slider_item active">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_001.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities1">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem 
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Lối vào chính</span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Được phủ lên một màu xanh tươi non tự nhiên, lối vào của dự án tạo cho cư dân cùng khách ghé thăm cảm giác thư thái từ những bước chân đầu tiên. Để sau đó là ấn tượng choáng ngợp bởi điểm nhấn kiến trúc độc đáo, thể hiện thông qua mái vòm, 
                                       những đường cong mềm mại cùng các cột trụ lớn cứng cáp. </p> 
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                               
                               <div class="mod_location_slider_item">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_002.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities2">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Tổ hợp bể bơi và sân tiệc</span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Trang hoàng bằng những biểu tượng chạm khắc ánh kim, tổ hợp bể bơi và sân tiệc tại tầng trệt của D1mension trở thành không gian hoàn hảo cho những cuộc vui. Mang hình bóng của những hồ bơi La Mã xưa kia, nơi đây được tô điểm với khuôn mẫu kiến trúc cổ điển và tinh xảo.</p>
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                               <div class="mod_location_slider_item">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_003.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities3">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem 
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1"> Sảnh Vòm Đón Khách</span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Sảnh vòm vốn luôn là một điểm đặc trưng của các tòa tháp, lâu đài, dinh thự lớn từ xa xưa, đóng vai trò như lối ra vào chính của công trình. Điểm nhấn này được tái hiện tại D1MENSION, đem tới trải nghiệm xa hoa bậc nhất cho cư dân.</p> 
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                               
                               <div class="mod_location_slider_item">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_004.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities4">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Phòng tập thể dục trên không</span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Nằm trên tầng 15 của tòa tháp và sở hữu tầm nhìn toàn cảnh thành phố tráng lệ, phòng tập trên không được trang bị hàng loạt máy tập tối tân. Khi được tích hợp kèm thêm phòng thay đồ và xông hơi, 
                                       nơi đây trở thành một tổ hợp rèn luyện đẳng cấp, xa hoa cho cư dân của D1mension.</p> 
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                               
                               <div class="mod_location_slider_item">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_005.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities5">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Hồ bơi sàn kính trên không </span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Sảng khoái cảm nhận làn nước mát với khung cảnh hoa lệ của cả thành phố thu gọn trong tầm mắt, đó là trải nghiệm tuyệt vời mà hồ bơi sàn kính trên không mang lại. Nằm trên tầng 16, đây sẽ là tiện ích độc đáo tạo nên sự hứng khởi và thư giãn tuyệt vời cho cư dân mỗi ngày.</p> 
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                               <div class="mod_location_slider_item">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_006.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities6">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Sảnh tiệc trên không</span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Là một khu vực rộng lớn xa hoa gồm sảnh thư giãn, phòng dùng bữa, quầy rượu và cigar. Với thiết kế sang trọng và tinh tế, đây là nơi cư dân cùng gia đình có thể tổ chức những sự kiện quan trọng hoặc đơn thuần tận hưởng khoảnh khắc riêng mình.</p>
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                               
                               <div class="mod_location_slider_item">
                                   <div class="thumb">
                                   
                                       <img src="img/img_location/Facilities_007.jpg" alt="location_location_pic1"/>
                                       
                                       <div id="" class="clsBarInner zoomMap_facilities7">
                                           <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                               <a>Click để xem
                                               <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                           </div>
                                           <div class="clear"></div>
                                       </div>  
                                         
                                   </div>
                                   
                                   <div class="desc">
                                   
                                   header
                                     <div id="" class="clsPurple  " data-wow-duration="400ms" data-wow-delay="500ms" data-animation="animated fadeInRightBig" >
                                             
                                             <div class="groupheader">
                                                 <div id="">
                                                     <span class="clsHeaderSmallFont clsHeaderNormalFont clsHeader1">Công viên giải trí</span> 
                                                 </div>
                                             </div>
                                             
                                         </div>  
                                     end header
                                   
                                   <p>Rạng rỡ trong sắc xanh cỏ cây, khu vực công viên giải trí sẽ là nơi các em nhỏ vui chơi thỏa thích. Khám phá thế giới tự nhiên thu nhỏ hay đơn thuần kết thân với bè bạn, nơi đây thực sự lý tưởng cho các em phát triển thể chất và tâm hồn.</p> 
                                   
                                   </div>
                               </div>END mod_location_slider_item
                               
                                           
                                           
                                                   </div>END mod_location_slider_box
                               
                                           Controls
                                           <style>
                                           .controlx_btn img{width:70%!important; height:auto;}
                                           .controlx_btn{width:5%; text-align:center;}
                                           .controlx_btn:before{content:normal!important;}
                                           </style>
                             <div class="controlx">
                                 <div class="controlx_box">
                                 <div class="controlx_btn"><img src="msd_assets/plugins/slideshow/img/slider_arrowl.png" alt=""/></div>
                                 <div class="controlx_btn"><img src="msd_assets/plugins/slideshow/img/slider_arrowr.png" alt=""/></div>
                                 <div class="clear"></div>
                               </div>
                             </div>
                               
                                                 </div>END mod_location_slider
                            -->
                        <!-- <div id="" class="clsHeader1 loclabel" style="text-align:center;">
                           <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Nội thất</span> 
                           </div>
                           -->
                        <style>
                           /* jssor slider bullet navigator skin 05 css */
                           /*
                           .jssorb05 div           (normal)
                           .jssorb05 div:hover     (normal mouseover)
                           .jssorb05 .av           (active)
                           .jssorb05 .av:hover     (active mouseover)
                           .jssorb05 .dn           (mousedown)
                           */
                           .jssorb05 {
                           position: absolute;
                           display:none!important;
                           }
                           .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                           position: absolute;
                           /* size of bullet elment */
                           width: 16px;
                           height: 16px;
                           background: url('msd_assets/plugins/slideshow/img/b05.png') no-repeat;
                           overflow: hidden;
                           cursor: pointer;
                           }
                           .jssorb05 div { background-position: -7px -7px; }
                           .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
                           .jssorb05 .av { background-position: -67px -7px; }
                           .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
                           /* jssor slider arrow navigator skin 12 css */
                           /*
                           .jssora12l                  (normal)
                           .jssora12r                  (normal)
                           .jssora12l:hover            (normal mouseover)
                           .jssora12r:hover            (normal mouseover)
                           .jssora12l.jssora12ldn      (mousedown)
                           .jssora12r.jssora12rdn      (mousedown)
                           */
                           .jssora12l, .jssora12r {
                           display: block;
                           position: absolute;
                           /* size of arrow element */
                           width: 30px;
                           height: 46px;
                           cursor: pointer;
                           /*background: url('msd_assets/plugins/slideshow/img/a12.png') no-repeat;
                           overflow: hidden;*/
                           color:#fff!important;
                           font-size:24px!important;
                           text-align:center;
                           }
                           .jssora12l img, .jssora12r img{width:70%; height:auto;}
                           /*
                           .jssora12l:before {
                           content: '\2329';
                           }
                           .jssora12r:before {
                           content: '\232A';
                           }
                           */
                           .jssora12l { background-position: -16px -37px; }
                           .jssora12r { background-position: -75px -37px; }
                           .jssora12l:hover { background-position: -136px -37px; }
                           .jssora12r:hover { background-position: -195px -37px; }
                           .jssora12l.jssora12ldn { background-position: -256px -37px; }
                           .jssora12r.jssora12rdn { background-position: -315px -37px; }
                        </style>
                        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:600px;height:300px;overflow:hidden;display:none;">
                           <!-- Loading Screen -->
                           <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                              <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                              <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                           </div>
                           <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:600px;height:300px;overflow:hidden;">
                             <!--  <div data-p="112.50">
                                <img data-u="image" src="img/img_location/Interior_001.jpg" alt="" />hihi
                             </div>
                             <div data-p="112.50">
                                <img data-u="image" src="img/img_location/Interior_002.jpg" alt="" />
                             </div>
                             <div data-p="112.50">
                                <img data-u="image" src="img/img_location/Interior_003.jpg" alt="" />
                             </div>
                             <div data-p="112.50">
                                <img data-u="image" src="img/img_location/Interior_004.jpg" alt="" />
                             </div> -->
                           </div>
                           <!-- Bullet Navigator -->
                           <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                              <!-- bullet navigator item prototype -->
                              <div data-u="prototype" style="width:16px;height:16px;"></div>
                           </div>
                           <!-- Arrow Navigator -->
                           <span data-u="arrowleft" class="jssora12l" style="top:0px;left:0px;width:30px;height:46px;" data-autocenter="2"><img src="msd_assets/plugins/slideshow/img/slider_arrowl.png" alt=""/></span>
                           <span data-u="arrowright" class="jssora12r" style="top:0px;right:0px;width:30px;height:46px;" data-autocenter="2"><img src="msd_assets/plugins/slideshow/img/slider_arrowr.png" alt=""/></span>
                        </div>
                        <!-- #endregion Jssor Slider End -->
                        <div id="" class="clsHeader1 loclabel" style="text-align:center; display:none!important;">
                           <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Level Plan</span> 
                        </div>
                        <div class="mod_location_slider" id="mod_location_slider2" style="display:none!important;">
                           <div class="mod_location_slider_box">
                              <div class="mod_location_slider_item active">
                                 <div class="thumb">
                                    <img src="img/img_location/Levelplan_001.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_levelplan1">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc">
                                    <div class="legend_levelplan">
                                       <p>LEGEND</p>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;2 BEDROOMS<br/>
                                          <span style="background-color:#8e929d;"></span>&nbsp;&nbsp;3 BEDROOMS
                                       </div>
                                       <div class="legend_levelplan_col1">
                                          <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/Levelplan_002.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_levelplan2">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc">
                                    <div class="legend_levelplan">
                                       <p>LEGEND</p>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;2 BEDROOMS<br/>
                                          <span style="background-color:#8e929d;"></span>&nbsp;&nbsp;3 BEDROOMS
                                       </div>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#9eb2ad;"></span>&nbsp;&nbsp;4 BEDROOMS<br/>
                                          <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/Levelplan_003.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_levelplan3">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc">
                                    <div class="legend_levelplan">
                                       <p>LEGEND</p>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;PENTHOUSE<br/>
                                          <span style="background-color:#8e929d;"></span>&nbsp;&nbsp;SKY CHANGING ROOMS<br class="nonex"/><span class="nonex"></span><i class="nonex">&nbsp;</i>&nbsp;WITH SAUNA &amp; STEAM BATH<br/>
                                          <span style="background-color:#696e80;"></span>&nbsp;&nbsp;SKY GYM
                                       </div>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#607b74;"></span>&nbsp;&nbsp;LANDSCAPE<br/>
                                          <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/Levelplan_004.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_levelplan4">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc">
                                    <div class="legend_levelplan">
                                       <p>LEGEND</p>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#7b674d;"></span>&nbsp;&nbsp;PENTHOUSE<br/>
                                          <span style="background-color:#676d7a;"></span>&nbsp;&nbsp;SKY PARTY HOUSE<br/>
                                          <span style="background-color:#767987;"></span>&nbsp;&nbsp;SKY INFINITY POOL<br class="nonex"/><span class="nonex"></span><i class="nonex">&nbsp;</i>&nbsp;WITH GLASS BOTTOM<br/>
                                       </div>
                                       <div class="legend_levelplan_col1">
                                          <span style="background-color:#7d6650;"></span>&nbsp;&nbsp;SKY POOL DECK<br/>
                                          <span style="background-color:#627e76;"></span>&nbsp;&nbsp;LANDSCAPE<br/>
                                          <span class="white"></span>&nbsp;&nbsp;LIFT CORE / CORRIDOR / M&amp;E
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                           </div>
                           <!-- END mod_location_slider_box-->
                           <!-- Controls -->
                           <div class="controlx">
                              <div class="controlx_box">
                                 <div class="controlx_btn"></div>
                                 <div class="controlx_btn"></div>
                                 <div class="clear"></div>
                              </div>
                           </div>
                        </div>
                        <!-- END mod_location_slider-->
                        <div class="disclaimerx" style="display:none!important;"><strong>DISCLAIMER:</strong>  THE PLAN IS SUBJECT TO CHANGE AS MAY BE APPROVED BY THE RELEVANT AUTHORITIES. |  THE PLAN IS NOT DRAWN TO SCALE. DIMENSIONS ARE SHOWN IN MILLIMETERS. | THE AREA(S) QUOTED IS AN APPROXIMATE MEASUREMENT AND SUBJECT TO FINAL SURVEY BY REGISTERED SURVEYOR.</div>
                        <div id="" class="clsHeader1 loclabel" style="text-align:center; display:none!important;">
                           <span class="clsHeaderBigFont  clsHeaderItalicFont  clsHeader1">Unit Plan</span> 
                        </div>
                        <div class="mod_location_slider" id="mod_location_slider3" style="display:none!important;">
                           <div class="mod_location_slider_box">
                              <div class="mod_location_slider_item active">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_001.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit1">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_002.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit2">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_003.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit3">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_004.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit4">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>NHẤN VÀO ĐỂ XEM 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_005.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit5">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_006.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit6">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_007.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit7">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_008.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit8">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                              <div class="mod_location_slider_item">
                                 <div class="thumb">
                                    <img src="img/img_location/UnitPlan_009.jpg" alt="location_location_pic1"/>
                                    <div id="" class="clsBarInner zoomMap_unit9">
                                       <div class=" col-xs-12 text-center clsNormalFontViet text-uppercase">
                                          <a>Click để xem 
                                          <span class="glyphicon glyphicon-menu-right   wow shake" style="float:right;"  data-wow-duration="2000ms" data-wow-delay="200ms"></span></a>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 <div class="desc" style="display:none;">
                                    No Content
                                 </div>
                              </div>
                              <!-- END mod_location_slider_item -->
                           </div>
                           <!-- END mod_location_slider_box-->
                           <!-- Controls -->
                           <div class="controlx">
                              <div class="controlx_box">
                                 <div class="controlx_btn"></div>
                                 <div class="controlx_btn"></div>
                                 <div class="clear"></div>
                              </div>
                           </div>
                        </div>
                        <!-- END mod_location_slider-->
                        <div class="disclaimerx" style="display:none!important;"><strong>DISCLAIMER:</strong>  THE PLAN IS SUBJECT TO CHANGE AS MAY BE APPROVED BY THE RELEVANT AUTHORITIES. | THE PLAN IS NOT DRAWN TO SCALE. DIMENSIONS ARE SHOWN IN MILLIMETERS. | THE AREA(S) QUOTED IS AN APROXIMATE MEASUREMENT AND SUBJECT TO FINAL SURVEY BY REGISTERED SURVEYOR. | FURNITURE ARE SHOWN FOR ILLUSTRATION PURPOSES ONLY AND ARE NOT NECESSARILY PROVIDED UPON HANDOVER.</div>
                        <div style="width:100%; height:20px;"></div>
                     </div>
                     <!-- END msd_maincontainer -->
                  </div>
                  <!--click de xem -->
            
               </section>
              

