<?php 
$current_lang = !empty($current_lang) ? $current_lang : EN_LANG;

$menus = array();
//$listmenus = array();
$menu_test = array(
    // Design concept - menu  
    array(
        'data' => array(
            EN_LANG => array(
                'name' => 'Location',
                'url' => 'location',
                'has_childs' => 1
            ),
            VI_LANG => array(
                'name' => 'Vị trí',
                'url' => 'vi-tri',
                'has_childs' => 1
            ),
        ), //end parent property
        'childs' => array(
        ),//end childs property
    ),//end parent item
    // Location - menu
    array(
        'data' => array(
            EN_LANG => array(
                'name' => 'Facilities',
                'url' => 'facilities',
                'has_childs' => 0
            ),
            VI_LANG => array(
                'name' => 'Tiện ích',
                'url' => 'tien-ich',
                'has_childs' => 0
            ),
        ),
        'childs' => array(   ),
    ),

    // Unit layout - menu
    array(
        'data' => array(
            EN_LANG => array(
                'name' => 'Floor Plan',
                'url' => 'floor-plan',
                'has_childs' => 1
            ),
            VI_LANG => array(
                'name' => 'Mặt bằng tầng ',
                'url' => 'mat-bang-tang',
                'has_childs' => 1
            ),
        ),
        'childs' => array(     ),
    ),
   array(
            'data' => array(
                EN_LANG => array(
                    'name' => 'Gallery',
                    'url' => 'gallery',
                    'has_childs' => 0
                ),
                VI_LANG => array(
                    'name' => 'Hình ảnh dự án',
                    'url' => 'hinh-anh-du-an',
                    'has_childs' => 0
                ),
            ),
            'childs' => array(),
        ),
        // Facilities Plan
    array(
        'data' => array(
            EN_LANG => array(
                'name' => 'Media',
                'url' => 'media',
                'has_childs' => 0
            ),
            VI_LANG => array(
                'name' => 'Thông tin truyền thông',
                'url' => 'thong-tin-truyen-thong',
                'has_childs' => 0
            ),
        ),
        'childs' => array(),
    ),
    // Unit plan
    array(
        'data' => array(
            EN_LANG => array(
                'name' => 'Promotion',
                'url' => 'promotion',
                'has_childs' => 0
            ),
            VI_LANG => array(
                'name' => 'Chính sách khuyến mãi',
                'url' => 'chinh-sach-khuyen-mai',
                'has_childs' => 0
            ),
        ),
        'childs' => array(),
    ),
    // Media - menu
    array(
        'data' => array(
            EN_LANG => array(
                'name' => 'Developer',
                'url' => 'developer',
                'has_childs' => 1
            ),
            VI_LANG => array(
                'name' => 'Chủ đầu tư',
                'url' => 'chu-dau-tu',
                'has_childs' => 1
            ),
        ),
        'childs' => array(),
    ),
      array(
            'data' => array(
                EN_LANG => array(
                    'name' => 'Register',
                    'url' => 'register',
                    'has_childs' => 0
                ),
                VI_LANG => array(
                    'name' => 'Đăng ký',
                    'url' => 'dang-ky',
                    'has_childs' => 0
                ),
            ),
            'childs' => array(),
        ),
        // E-brochure
        array(
            'data' => array(
                EN_LANG => array(
                    'name' => 'Contact',
                    'url' => 'contact',
                    'has_childs' => 0
                ),
                VI_LANG => array(
                    'name' => 'Thông tin liên lạc',
                    'url' => 'thong-tin-lien-lac',
                    'has_childs' => 0
                ),
            ),
            'childs' => array(),
        ),
        // array(
        //     'data' => array(
        //         EN_LANG => array(
        //             'name' => 'Personal Information Protection Policy',
        //             'url' => 'protection-policy',
        //             'has_childs' => 0
        //         ),
        //         VI_LANG => array(
        //             'name' => 'Chính Sách Bảo Vệ Thông Tin Cá Nhân',
        //             'url' => 'chinh-sach',
        //             'has_childs' => 0
        //         ),
        //     ),
        //     'childs' => array(),
        // ),
);
$menus [EN_LANG] = $menu_test;
 ?>