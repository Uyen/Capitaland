<?php
     $lag = $_GET['lag'] ;
         if($lag == VI_LANG){
                 ?>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <p style="text-align: justify;">
                            Chúng tôi đã đặc biệt thận trọng trong việc chuẩn bị nội dung tài liệu này cũng như các bản mẫu. Tuy nhiên vui lòng lưu ý rằng bản thân tập đoàn, cũng như các đại lý bán hàng, sẽ không chịu trách nhiệm cho những thông
                            tin chưa chính xác trong tài liệu này. Tuy bản thân chúng tôi thực sự tin tưởng rằng những nội dung dưới đây là hoàn toàn chính xác, nhưng tài liệu này sẽ không mang tính đại diện hay trở thành một tuyên ngôn riêng. Tất
                            cả thông tin, những thông số đều có thể sẽ được thay đổi bởi chủ đầu tư hoặc bởi các nhà chức trách có thẩm quyền, bởi vậy mà đây không thể trở thành một phần của các hợp đồng hay đề nghị. Các bản vẽ, thiết kế hay hình
                            ảnh đồ họa sẽ không đại diện chính xác cho điều kiện xây dựng thực tế. Các khu vực sàn được đo đạc kĩ lưỡng, có căn cứ từ các bản báo cáo. Trong khi đó, các hình ảnh đồ họa có thể không phản ánh hoàn toàn điều kiện thực
                            tế.
                        </p>

                        <!--  <h3 style="color:white" >RIVER VIEW COMPANY LIMITED</h3>
                                        <p>A wholly-owned subsidiary of CapitaLand Limited
                                               </p>
                                        <p>Office Address: 8th Floor, Vista Tower, 628C Hanoi Highway, An Phu Ward,

                                            District 2, Ho Chi Minh City, Vietnam </p>
                                        <p>Project Address: 608 Vo Van Kiet, Cau Kho Ward,

                                            District 1, Ho Chi Minh City, Vietnam
                                            </p>
                                        <p>1800 599 986
                                        </p> -->
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <p><span class="copyright">© Copyright 2019 CAPITALAND. All Rights Reserved.</span></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="ftBuild" src="<?=get_resource_url('img/ft-building.png')?>" height="101" width="290" alt="" />
                    </div>
                </div>
            </div>
        </footer>

        <div class="footer__bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-7 col-lg-8 col-xs-12">
                                <div class="footer__text">
                                    <p>Giấy chứng nhận đăng ký doanh nghiệp số 0313296323 do Sở Kế hoạch và Đầu tư Thành phố Hồ Chí Minh cấp đăng ký lần đầu ngày 09/6/2015</p>
                                    <p>Website thuộc sở hữu của CÔNG TY TNHH CẢNH SÔNG</p>
                                    <p>Số 608 Võ Văn Kiệt, Phường Cầu Kho, Quận 1, thành phố Hồ Chí Minh, Việt Nam.</p>
                                    <p><a class="link" href="<?=PATH_URL . 'chinh-sach?lag=' . VI_LANG ?>" title="Chính Sách Bảo Vệ Thông Tin Cá Nhân">Chính Sách Bảo Vệ Thông Tin Cá Nhân</a></p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 col-xs-12 mt-md-0 mt-3 text-center">
                                <!-- Bo cong thuong -->
                                <p class="poly-c">
                                    <a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=59596" target="_blank">
                                        <img class="img-fluid" id="logo_bct" src="img/large/cong_thuong.png" alt="formul8 logo" />
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 mt-md-0 mt-3">
                        <div class="footer__logo">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <img class="logo px-1" src="img/footer_capitaland.png" alt="capitaland logo" />
                                            <img class="logo px-1" src="img/footer_ascott.png" alt="capitaland logo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hotline">
            <a href="tel:1800599986" title=""><img src="<?=get_resource_url('img/hotline.png')?>" alt="" /></a>
        </div>
        <?php }else{ ?>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <p style="text-align: justify;">
                            We have used reasonable care in preparing this website and in constructing the model(s) and showflats. Please note, however, that neither our agents nor we will be held responsible for any inaccuracy in the contents of
                            this website. Whilst we believe the contents of this website to be correct and accurate and correct at the time of publication, they are not to be regarded as statements or representations of fact. All information,
                            specifications and plans herein contained may be subjected to change from time to time by us and/or the competent authorities as may be required and do not form part of an offer or contract. Renderings, depictions and
                            illustrations are artistic impressions. Photographs do not necessarily represent as-built standard specifications. Floor areas are approximate measurements and are subject to final survey. Likewise the model(s) and
                            showflats are artistic impressions only and should not be considered as representation of fact.
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <p><span class="copyright">© Copyright 2019 CAPITALAND. All Rights Reserved.</span></p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="ftBuild" class="mt-md-0 mt-3" src="img/ft-building.png" alt="" />
                    </div>
                </div>
            </div>
        </footer>

        <div class="footer__bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-7 col-lg-8 col-xs-12">
                                <div class="footer__text">
                                    <p>Enterprise Registration Certificate No. 0313296323 firstly issued on 09 June 2015 by the Department of Planning and Investment of Ho Chi Minh City</p>
                                    <p>Website owner: RIVER VIEW COMPANY LIMITED</p>
                                    <p>608 Vo Van Kiet, Cau Kho Ward, District 1, Ho Chi Minh City, Vietnam</p>
                                    <p><a class="link" href="<?=PATH_URL . 'policy?lag=' . EN_LANG ?>" title="Personal Information Protection Policy">Personal Information Protection Policy</a></p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 col-xs-12 mt-md-0 mt-3 text-center">
                                <!-- Bo cong thuong -->
                                <p class="poly-c">
                                    <a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=59596" target="_blank">
                                        <img class="img-fluid" id="logo_bct" src="img/large/cong_thuong.png" alt="formul8 logo" />
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 mt-md-0 mt-3">
                        <div class="footer__logo">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <img class="logo px-1" src="img/footer_capitaland.png" alt="capitaland logo" />
                                            <img class="logo px-1" src="img/footer_ascott.png" alt="capitaland logo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hotline">
            <a href="tel:1800599986" title=""><img src="<?=get_resource_url('img/hotline.png')?>" alt="" /></a>
        </div>

        <?php } ?>
        <div class="blackbox_shade"></div>
        <div class="blackbox">
            <div class="blackbox_control"><span>CLOSE - X</span></div>
            <div class="blackbox_container">hihi</div>
        </div>
        <div class="brightbox"></div>

        <?php
                // if($view == 'introduction-en' || $view == 'introduction-vn'){
                //     if($lag == VI_LANG){
                ?>
        <!-- POPUP VN -->
        <!--  <div class="modal fade modal-js-vn show_popup_vn" id="myModal-js-vn">
                        <div class="modal-dialog w800 fix_800">
                            <div class="modal-content fixmodal">
                                <div class="modal-body">
                                <button id="close_pupop" type="button" class="close" data-dismiss="modal">×</button>
                                    <div class="img_tks">
                                        <img src="img/vietnamese_3.png" alt="">
                                    </div>
                                    <div class="text-center"></div>
                                </div>
                            </div>
                        </div>
                    </div> -->
        <? //}else{ ?>
        <!-- <div class="modal fade modal-js-english show_popup_en" id="myModal-js-english">
                        <div class="modal-dialog w800 fix_800">
                            <div class="modal-content fixmodal">
                                <div class="modal-body">
                                <button id="close_pupop" type="button" class="close" data-dismiss="modal">×</button>
                                    <div class="img_tks">
                                        <img src="img/English_1.png" alt="">
                                    </div>
                                    <div class="text-center"></div>
                                </div>
                            </div>
                        </div>
                    </div> -->
        <? 
            //} } ?>
        <?php
        if($view == 'floor-plan-en' || $view == 'floor-plan-vn'){
                ?>

        <script src="<?=get_resource_url('js/app1.js')?>" charset="utf-8"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>

        <?php
                }
                ?>

        <?php
                if(($view == 'media-en' || $view == 'media-vn') ||  ($view == 'promotion-en' || $view == 'promotion-vn')){
                ?>
        <script src="<?=get_resource_url('js/app1.js')?>" charset="utf-8"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>

        <!-- MSD JS INCLUDES -->
        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/jquery.transit.min.js')?>"></script>
        <!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="<?=get_resource_url('msd_assets/js/msd_functions.js')?>"></script>

        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/msdSlider.js')?>"></script>

        <script src="<?=get_resource_url('msd_assets/js/msd_functions_location.js')?>"></script>

        <!-- MSD JS INCLUDES -->
        <?php
                }
                ?>

        <!--  <script type="text/javascript">_satellite.pageBottom();</script> -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?=get_resource_url('js/min/toucheffects-min.js')?>"></script>

        <script src="<?=get_resource_url('js/flickity.pkgd.min.js')?>"></script>
        <script src="<?=get_resource_url('js/jquery.fancybox.pack.js')?>"></script>

        <script src="<?=get_resource_url('js/custom.js')?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?=get_resource_url('js/retina.js')?>"></script>
        <script src="<?=get_resource_url('js/waypoints.min.js')?>"></script>
        <script src="<?=get_resource_url('js/bootstrap.min.js')?>"></script>
        <script src="<?=get_resource_url('js/parallax.js')?>"></script>
        <script src="<?=get_resource_url('js/wow.min.js')?>"></script>
        <script src="<?=get_resource_url('js/scripts.js')?>"></script>
        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/jquery.transit.min.js')?>"></script>
        <!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="<?=get_resource_url('msd_assets/js/msd_functions.js')?>"></script>
        <?php
                if($view == 'developer-en' || $view == 'developer-vn'){
                ?>
        <!-- MSD JS INCLUDES -->
        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/jquery.transit.min.js')?>"></script>
        <!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="<?=get_resource_url('msd_assets/js/msd_functions.js')?>"></script>

        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/msdSlider.js')?>"></script>

        <script src="<?=get_resource_url('msd_assets/js/msd_functions_location.js')?>"></script>

        <!-- MSD JS INCLUDES -->
        <?php
                }
                ?>
        <!-- $view in router -->
        <?php
                if(($view == 'gallery-en' || $view == 'gallery-vn')){
                ?>
        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/jquery.transit.min.js')?>"></script>
        <!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="<?=get_resource_url('msd_assets/js/msd_functions.js')?>"></script>
        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/msdSlider.js')?>"></script>
        <script src="<?=get_resource_url('msd_assets/js/msd_functions_location.js')?>"></script>

        <?php 
                    }
                ?>
        <?php
                if(($view == 'location-en' || $view == 'location-vn') || ($view == 'facilities-en' || $view == 'facilities-vn') ){
                ?>
        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/jquery.transit.min.js')?>"></script>
        <!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="<?=get_resource_url('msd_assets/js/msd_functions.js')?>"></script>

        <script src="<?=get_resource_url('msd_assets/plugins/msd_slider/js/msdSlider.js')?>"></script>

        <script src="<?=get_resource_url('msd_assets/js/msd_functions_location.js')?>"></script>

        <?php 
                    }
                ?>

 

    </body>
</html>