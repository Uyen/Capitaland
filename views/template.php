<?php 
function render_view($view) {
    require_once(__DIR__ . "/header.php");
    include_once(__DIR__ . "/contents/" . $view . '.php');
    
    require_once(__DIR__ . "/footer.php");
}

function get_resource_url ($path = null) {
    $url = trim(rtrim(PATH_URL, '/\\') . '/' . ltrim($path, '/\\'));
    $path_full = BASEFOLDER . $path;
    if (function_exists('filemtime')) {
        if (file_exists($path_full)) {
            $url .= '?r=' . filemtime($path_full);
        }
    }
    return $url;
}

function join_path($first_path, $second_path) {
    return trim(rtrim($first_path, '/\\') . '/' . ltrim($second_path, '/\\'));
}

function get_url_form_define($define) {
    if (defined($define)) {
        return join_path(PATH_URL, constant($define));
    }
    else if ($define == '#') {
         return '#';
     } 
    else {
        return join_path(PATH_URL, '#define-not-found');

    }
}

function get_url_form_path($path) {
    return join_path(PATH_URL, $path);
}


?>