<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>D1MENSION - CapitaLand's Next Project in Vietnam</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="F8" />
		<!-- Bootstrap -->
		<script src="js/modernizr.custom.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.fancybox.css" rel="stylesheet">
		<link href="css/flickity.css" rel="stylesheet" >
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
        
		<link href="css/styles.css" rel="stylesheet">
		<link href="css/innerpages.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon-.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png">

        <link rel="icon" type="image/png" href="favicon.ico" />
       <link href="msd_assets/css/msd_styles.css" rel="stylesheet">
        <link href="css/regform.css" rel="stylesheet"> 
        <!-- MSD CSS INCLUDES -->
        <link rel="stylesheet" href="msd_assets/plugins/msd_slider/css/style.css">
	</head>
	<body>
		<!--[if lt IE 7]>
		<p class="">You are using an <strong>outdated</strong> browser. Please update browser to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
        
        
		<header>
            
            <div class="main-navbar " >
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="index.html"><img id="imgBrandLogo" src="img/D1Logo2.png" alt=""></a>
                        <span class="breadcrumbx">Page name goes here</span>
                        
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="msd-navbar">
                        <div id="my-nav" class="navbar-collapse collapse text-center">
                            <ul class="nav nav-pills nav-stacked text-uppercase">
                                

                                <li><a href="introduction.html" >Giới thiệu</a></li>
                                <li><a href="location.html" >Vị trí </a></li>
                                <li><a href="residences.html" >Chi tiết dự án</a></li>
                                <li><a href="developer.html" >Chủ đầu tư</a></li>
                                <li id="mnuReg" class="clsGoldbg active" ><a href="register-vn.php">Đăng kí thông tin  </a></li>

                            </ul>
                            
                            
                           


                        </div>
                    </div>
                    <div id="divLangToggle" > <a href="register.php">EN</a> / <a href="register-vn.php"> VN</a> </div>
                    
                </nav>
            </div>
           
          
            
            <div id="main-navbar-cover"></div>
            
            <div class="texture-overlay"></div>
            
			
		</header>
        
        <section id="divKeyVisual" class="clsKeyvisual">
            <div id="keyVisParllaxWrap" class="container-fluid nopadding " >
                
                <div class="parallax-window parallax-windowIndividual parallax-left-shape parallax-left-shape2  hidden-xs" data-parallax="scroll" data-image-src="img/hero2.jpg"> 
                     <div class="clsArtImpression2">
                        <span class="artimp1">Artist's impression  </span>
                    </div>
                   
                </div>
                
                <img id="imgRegKeyVis-mobile" src="img/register-mobile.jpg" width="1209" height="908" alt=""  class="visible-xs">
                
                <div  id="divPgKeyOuterTxtWrap">
                
                    <div id="divRegPgKeyTxtWrap" class="clsKeyVisParllaxTxtWrap" >

                        <div id="divTemplateKeyVisParllaxTxtHeaderWrap" class="text-center ">

                            <div id="divRegPgKeyVisHeadTxt1" >
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFont text-left">Kiến tạo</span> 
                            </div>

                            <div id="divRegPgKeyVisHeadTxt2-">

                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet  "><i>Chuẩn Mực Mới</i></span> 
                                <span class="clsHeaderSmallFont clsHeader1 clsHeaderItalicFontViet">tại</span> 
                                <span class="clsHeaderBigFont clsHeader1 clsHeaderItalicFontViet">CHÂU Á</span>
                            </div>
                            
                            
                            
                            
                            
                        </div>
                       

                    </div>

                </div>
            </div>    
           

        </section>

        <style>
            
        
        </style>
        <!-- OSR - Begin registration Page -->
        
        
        <?php
                $varCode =     $_POST["password"];
                if ($varCode != "CAPITALANDVIP--") {$showform="true";}
                if ($showform) {    
                
            ?>
            
            <section  id="secRegForm" class="parllaxpadding">
                <div class="container whitebg" id="secRegFormContainer">
                    <div class="row">
                        <form class="well form-horizontal " action="registersend.php" method="post"  id="contact_form"  accept-charset="UTF-8" >
                            <input type="hidden" name="showform" value="<?php echo $showform ?>">
                            
                        <div id="frmWrap">
                            <fieldset>
                                <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" > <!-- OSR - FROM END -->
                                    
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-md-12 text-uppercase  clsGold" ><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">Đăng kí nhận thông tin.</span> &nbsp;&nbsp;&nbsp;<span class="visible-xs"></span><span class="clsMandatoryLbl headerTxt"></span><hr></div>
                                        
                                        
                                        
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-md-2 "> 
                                            
                                            
                                            <select name="saluation" class="form-control selectpicker-  element-transparent splSelect " >

                                                
                                                    
                                                    <option  value="MR.">Ông</option>
                                                    <option  value="MRS.">Bà</option>
                                                    <option  value="MS.">Anh</option>
                                                    <option  value="DR.">Chị</option>
                                                    
                                                

                                            </select>
                                            <span class="visible-xs"><br></span>
     
                                        </div>

                                        <div class="col-md-10 inputGroupContainer">
                                            <input  name="full_name"  placeholder="Tên đầy đủ" class="form-control  element-transparent clsGold"  type="text">
                                        </div>
                                        
                                    </div>

                                    
                                    
                                   <div class="form-group">
                                        <div class="col-md-12 inputGroupContainer">
                                            <input  name="email"  placeholder="Email" class="form-control  element-transparent"  type="text">
                                        </div>

                                    </div>

                                    
                                    <div class="form-group">
                                        <div class="col-md-6 ">
                                            <input  name="countrycode"  placeholder="Mã vùng/ khu vực" class="form-control  element-transparent"  type="text">
                                            <span class="visible-xs"><br></span>
                                        </div>

                                        <div class="col-md-6 inputGroupContainer">
                                            <input  name="phonenumber"  placeholder="Số điện thoại" class="form-control  element-transparent"  type="text">
                                        </div>

                                    </div>
                                    
                                    
                                    
                                    <div class="form-group"> <br />
                                      
                                        <div class="col-md-12 text-uppercase  clsGold" ><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">Cách thức liên lạc mong muốn</span> </span><hr></div>
                                        
                                        <div class="col-md-12 text-uppercase ">
                                          <label class="checkbox-inline">
                                            <input type="checkbox" name="prefcontactmethod[]" value="Call"><div class="radoSpace"> Gọi điện</div>
                                          </label><br ><div class="clsFamily"></div>
                                          <label class="checkbox-inline">
                                            <input type="checkbox" name="prefcontactmethod[]" value="Email"> <div class="radoSpace">Gửi Email</div>
                                          </label><br ><div class="clsSalesStaff"></div>
                                          
                                          
                                        </div>
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="form-group"><br /><span class="visible-xs"><br /></span>
                                        <div class="col-md-12 text-uppercase  clsGold" ><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">Thời gian liên lạc mong muốn</span> </span><hr></div>
                                        
                                        <div class="col-md-12 ">
                                            
                                            
                                            <div class="form-group">
                                               
                                                
                                                <div class="col-md-12">
                                                  <label class="checkbox-inline">
                                                    <input type="checkbox" name="prefcontacttime[]" value="10AM - 12PM"> <div class="radoSpace">10 giờ - 12 giờ sáng</div>
                                                  </label><br >
                                                  <label class="checkbox-inline">
                                                    <input type="checkbox" name="prefcontacttime[]" value="12PM - 3PM"> <div class="radoSpace">12 giờ - 3 giờ chiều</div>
                                                  </label><br >
                                                  <label class="checkbox-inline">
                                                    <input type="checkbox" name="prefcontacttime[]" value="3PM - 6PM" > <div class="radoSpace">3 giờ - 6 giờ chiều</div>
                                                  </label><br >
                                                  <label class="checkbox-inline">
                                                    <input type="checkbox" name="prefcontacttime[]" value="6PM - 8PM"> <div class="radoSpace">6 giờ - 8 giờ chiều</div>
                                                  </label><br >
                                                    
                                                  
                                                </div>
                                              </div>

       
                                           
                                            </div>
                                        
                                            
                                            
                                        </div>

                                    
                                    

                                    
                                    <style>
                                           input[type='radio']{
                                                margin:0;
                                            }

                                            input[type='radio'], label{   
                                               display:inline;
                                               vertical-align:top;
                                            }
                                        
                                       
                                        .radio-inline{top:-10px;}
                                        
                                        label {
                                            display: block;
                                            margin-left: 0px;
                                        }
                                        
                                        .clear {clear: both;}
                                        
                                        .form-group a {color:black;}
                                        .form-group a:hover {color:black;}
                                        

                                    </style>

                                    
                                   
                                    
                                    <div class="form-group">
                                        <div class="col-md-2 col-xs-6">
                                            <input  name="captcha"  placeholder="" class="form-control  element-transparent "  type="text" size="3">
                                        </div>
                                        <div class="col-md-2 offset-md-8 col-xs-6 ">
                                            <img src="captcha.php"/>
                                        </div>

                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group"><br />
                                        
                                        <div class="col-md-12 text-uppercase  clsGold" ><span class="headerTxt clsHeaderNormalFont clsRegistraionTitle">Điều khoản và điều kiện</span> </span><hr></div>
                            
                                        
                                        <div class="col-md-12">
                                            <div class="radio ">
                                                <label class="radio-inline " >
                                                    <input type="radio" name="tc" value="TC Checked"> 
                                                    <div class="radoSpace">
                                                        
                                                            Tôi đồng ý nhận các thông tin, tin tức mới từ CapitaLand và tôi đã đọc, cũng như đồng thuận với các điều khoản bảo mật riêng tư.
                                                        
                                                           
                                                    </div>
                                                    
                                                   
                                                </label>
                                            </div>
                                            <div class="visible-xs"><br /><br /><br /></div>
                                            <div class="k-invalid-msg" data-for="tc">&nbsp;</div>
                                        </div>
                                        <div class="t_c"></div>
                                    </div>
                                    
                                    
                                   
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                        <div id="divPushAboveSubmit"></div>
                                    </div>
                                   
                                    <div class="form-group text-center">

                                        <div class="col-md-6  col-xs-12  clsSubmitVerySmall- DivBtnSubmit">
                                            <button type="submit" class="btn  btn-custom btnSubmit" >CHẤP NHẬN <span class="glyphicon glyphicon-play"></span></button>
                                           
                                        </div>
                                        <div class="col-md-6 col-xs-12 clsSubmitVerySmall- divBtnReset">
                                            
                                            <button type="reset" class="btn btn-custom btnReset" >THỰC HIỆN LẠI <span class="glyphicon glyphicon-refresh"></span></button>
                                        </div>
                                        
                                    </div>


                                    
                                    
                                    
                                    
                                </div> <!-- OSR - FROM END -->
                                
                            </fieldset>
                        </div>
                        </form>
                         
                    </div>
                    
                     
                    
                    
                </div>
                
                
            </section>
            <?php
                    
                }
             else
                 
            {
            ?>
                
            <section  id="secRegForm" >
                <div class="container" id="secRegFormContainer_none" >
                    <div class="row" style="background-color:black;">
                        <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" > 
                            
                            <div id="divWrongPass" class="text-center clsRed">
                            
                                Incorrect password. Please try again.
                                
                                <br />  <br /> 
                                
                                
                               
                                
                                <a href="register-pre.php" class="btn btn-custom btnBack" role="button"><span class="glyphicon glyphicon-play icon-flipped"></span>&nbsp; GO BACK</a>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php
             }

            ?>
        
        
        
        
        <!-- OSR - End registraion Pge -->
         
            
		<footer>
			<div class="container-fluid">
                
				<div class="row  left-margin">
					<div class="col-md-8">
						<p>
                        
                            Chúng tôi đã đặc biệt thận trọng trong việc chuẩn bị nội dung tài liệu này cũng như các bản mẫu. Tuy nhiên vui lòng lưu ý rằng bản thân tập đoàn, cũng như các đại lý bán hàng, sẽ không chịu trách nhiệm cho những thông tin chưa chính xác trong tài liệu này. Tuy bản thân chúng tôi thực sự tin tưởng rằng những nội dung dưới đây là hoàn toàn chính xác, nhưng tài liệu này sẽ không mang tính đại diện hay trở thành một tuyên ngôn riêng. Tất cả thông tin, những thông số đều có thể sẽ được thay đổi bởi chủ đầu tư hoặc bởi các nhà chức trách có thẩm quyền, bởi vậy mà đây không thể trở thành một phần của các hợp đồng hay đề nghị. Các bản vẽ, thiết kế hay hình ảnh đồ họa sẽ không đại diện chính xác cho điều kiện xây dựng thực tế. Các khu vực sàn được đo đạc kĩ lưỡng, có căn cứ từ các bản báo cáo. Trong khi đó, các hình ảnh đồ họa có thể không phản ánh hoàn toàn điều kiện thực tế.
                            
                        
                        </p>
                        
                       
					</div>
					<div class="col-md-4">
						
					</div>
                   
                    
				</div>
                
                <div class="row  left-margin">
					<div class="col-md-8">
                        <p><span  class="copyright">© Copyright 2016 CAPITALAND. All Rights Reserved.</span></p>
					</div>
					<div class="col-md-4 text-center">
						<img id="ftBuild" src="img/ft-building.png" height="101" width="290" alt="">
					</div>

				</div>
                
                
                <div class="row  left-margin hidden-sm hidden-xs" id="ftrRowTwo">
                    <div class="col-md-8 col-xs-12" id="divF8logoCol" >
                      
                        <img id="f8logo" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55" />
					</div>
                    <div class="col-md-2  col-xs-12 " id="divDevbyCol" >
                        
						<img id="caplandlogo" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109" />
					</div>
                    
					<div class="col-md-2 cos-xs-12" id="divMangedbycol" >
                        
						<img id="ascottlogo" src="img/footer_ascott-vn.png" alt="capitaland logo" width="102" height="121" />
					</div>
                    
                </div>
                
                <div class="row  text-center visible-sm visible-xs" id="ftrRowTwoMobile">
                    
                    <div class="col-md-4  col-xs-12 ">
                        
						<img id="caplandlogoM" src="img/footer_capitaland-vn.png" alt="capitaland logo" width="157" height="109" /> <br /><br /><br />
					</div>
                    
					<div class="col-md-4 cos-xs-12">
                        
						<img id="ascottlogoM" src="img/footer_ascott-vn.png.png" alt="capitaland logo" width="102" height="121" /><br /><br /><br />
					</div>
                    <div class="col-md-4 col-xs-12">
                      
                        <img id="f8logoM" src="img/footer_formul8-vn.png" alt="formul8 logo" width="140" height="55" /><br />
					</div>
                </div>
                
                
			</div>
		</footer>
        
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/min/toucheffects-min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        
       
        
        
		<script src="js/flickity.pkgd.min.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
        
        <script src="js/parallax.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/scripts.js"></script>
        <script type="text/javascript" src="js/bootstrapValidator.min.js"> </script>
        <script src="js/register.js"></script>
        
        
        
        <!-- MSD JS INCLUDES -->
        <script src="msd_assets/plugins/msd_slider/js/jquery.transit.min.js"></script><!-- This Transit is authored and maintained by Rico Sta. Cruz -->
        <script src="msd_assets/plugins/msd_slider/js/msdSlider.js"></script>
        <script src="msd_assets/js/msd_functions.js"></script>


        <div class="brightbox"></div>

           
	</body>
</html>
