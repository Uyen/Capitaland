<?php

	
	//****Begin example for opening & closing resultset using mysql_query connection****//
	
		//$sqlTest = "";
		//$sqlTest = $sqlTest."SELECT  * FROM tblAgencies ";
		
		//echo $sqlTest;
		//$resultTest = mysql_query($sqlTest);
		//$testCount = 0;
		//$testCountTotRows = mysql_affected_rows();
		//echo $testCountTotRows;
		//mysql_free_result($resultTest);
	
	//****End example for opening & closing resultset using mysql_query connection****//
	
	//****Begin example for opening & closing resultset using PDO connection****//
		//$sql = "Select * from tblagencies where status=1";
		//$result = $pdo->query($sql);
		//foreach ($result as $row) {
		//	echo $row["licenseno"] ." - ". $row["agencyname"] . "<br />";
		//}
	
	//****End example for opening & closing resultset using PDO connection****//
	
		
		
   //-Generates and returns specified length key    
    function generateKey($x) {
      $key = "";
      $i = 0;
      while ($i < $x) {
        $module = rand(1,3);

        if($module == 1) {  //generate 0-9
          $key = $key.chr(rand(48,57)); 
        }

        if($module == 2){  //generate a-z
          $key = $key.chr(rand(97,122));  
        }

        if($module == 3){  //generate A-Z
          $key = $key.chr(rand(65,90)); 
        }

        $i++;
      }
      return($key);
    }

    function make_uid($type,$id){
      switch($type){
        case "forum":      $uid_a=1; break;
        case "realestate": $uid_a=2; break;
        case "jobs":       $uid_a=3; break;
        case "auto":       $uid_a=4; break;
        case "events":     $uid_a=5; break;
        case "classifieds":$uid_a=6; break;
        case "directory":  $uid_a=7; break;
        case "garage_sale":$uid_a=8; break;
        case "coupon":     $uid_a=9; break;
      }
      
      $a=strlen($id);
      if($a<5) {
        $x=5-$a;
        $uid_b=$id;
        for($y=0;$y<$x;$y++) $uid_b='0'.$uid_b;  
      }
      return($uid_a.$uid_b);
    }

    function getPageId(){
      $pagePath=$_SERVER['PHP_SELF'];
      if (strpos($pagePath,"/")>=0) {
        return strrev(substr(strrev($pagePath),4,strpos(strrev($pagePath),"/")-4));
      }
    }

    function get_date_format($db_date){
		if(strtotime($db_date) !== false) {
		  $stringArray = explode("-", $db_date);
		  $date = mktime(0,0,0,$stringArray[1],$stringArray[2],$stringArray[0]);
		  $formated_date = date("jS M Y", $date);
		 }
		else
		{
			$formated_date = "";
		}
      return($formated_date);
    }

    function get_long_date_format($db_date){
      $datetimeArray = explode(" ", $db_date);
      $time = $datetimeArray[1];
      $stringArray = explode("-", $datetimeArray[0]);
      $date = mktime(0,0,0,$stringArray[1],$stringArray[2],$stringArray[0]);
      $formated_date = date("F d, Y", $date);       
      $long_date = $formated_date." ".$time;
      $long_date = substr($long_date,0,strlen($long_date)-3);
      return($long_date);
    }
    
    function get_time_format($db_time){
      return(date("g:i A",strtotime($db_time)));
    }
    
    function get_video_thumb($youtube_url){
      //youtube url example: http://www.youtube.com/watch?v=wWeWjllm--M
      //thumb url example: http://i.ytimg.com/vi/wWeWjllm--M/default.jpg
      
      return('http://i.ytimg.com/vi/'.substr($youtube_url,31).'/default.jpg');
    }

    function get_video_movie($youtube_url){
      //youtube url example: http://www.youtube.com/watch?v=wWeWjllm--M
      //thumb movie example: http://www.youtube.com/v/wWeWjllm--M&hl=en&fs=1
      
      return('http://www.youtube.com/v/'.substr($youtube_url,31).'&hl=en&fs=1');
    }

    function resize_image($doc_root,$file_name){
      $filename = $doc_root.'/'.$file_name;
      $new_w = 100;
      $new_h = 75;
      $m_new_w = 596;
      $m_new_h = 780;

      $image_stats = getimagesize($file_name);
      $imagewidth = $image_stats[0];
      $imageheight = $image_stats[1];
      $img_type = $image_stats[2];

      $ratio = $imagewidth/$imageheight;

      if($imagewidth >= $imageheight) {
        $new_h=$new_w/$ratio;
        $m_new_h=$m_new_w/$ratio;
      }

      if($imageheight > $imagewidth) {
        $new_w=$new_h*$ratio;
        $m_new_w=$m_new_h*$ratio;
      }

      //resize
      //$src_img      = imagecreatefromjpeg($file_name);
      $src_img = @ImageCreateFromJPEG ($file_name) or // Read JPEG Image
      $src_img = @ImageCreateFromPNG ($file_name) or // or PNG Image
      $src_img = @ImageCreateFromGIF ($file_name) or // or GIF Image
      $src_img = false; // If image is not JPEG, PNG, or GIF
      $modydst_img  = imagecreatetruecolor($m_new_w,$m_new_h);
      imageCopyResampled($modydst_img,$src_img,0,0,0,0,$m_new_w,$m_new_h,$imagewidth ,$imageheight);
      imagejpeg($modydst_img, "$doc_root/$file_name",100);     
    }

    function create_thumb($doc_root,$file_name, $new_file_name){
      $filename = $doc_root.'/'.$file_name;
      $new_w = 100;
      $new_h = 75;
      $m_new_w = 550;
      $m_new_h = 475;

      $image_stats = getimagesize($file_name);
      $imagewidth = $image_stats[0];
      $imageheight = $image_stats[1];
      $img_type = $image_stats[2];

      $ratio = $imagewidth/$imageheight;

      if($imagewidth >= $imageheight) {
        $new_h=$new_w/$ratio;
        $m_new_h=$m_new_w/$ratio;
      }

      if($imageheight > $imagewidth) {
        $new_w=$new_h*$ratio;
        $m_new_w=$m_new_h*$ratio;
      }

      //thumbnail  
      //$src_img      = imagecreatefromjpeg($file_name);
      $src_img = @ImageCreateFromJPEG ($file_name) or // Read JPEG Image
      $src_img = @ImageCreateFromPNG ($file_name) or // or PNG Image
      $src_img = @ImageCreateFromGIF ($file_name) or // or GIF Image
      $src_img = false; // If image is not JPEG, PNG, or GIF      
      $thumbdst_img = imagecreatetruecolor($new_w,$new_h);
      imageCopyResampled($thumbdst_img,$src_img,0,0,0,0,$new_w,$new_h,$imagewidth ,$imageheight);

      if ($new_file_name != "") {
        imagejpeg($thumbdst_img, "$doc_root/$new_file_name", 100);

      } else {
        imagejpeg($thumbdst_img, "$doc_root/$file_name".'_thumb',100);
      }
    }
	

    function beginsWith($myString, $toFind) {
        return (substr($myString, 0, strlen($toFind)) === $toFind);
    }

    function endsWith($myString, $toFind) {
        return (substr($myString, strlen($myString) - strlen($toFind)) === $toFind);
    }

    function HTML($s) {
        if (get_magic_quotes_gpc()) {
            $s = stripslashes($s);            
        }

        //return htmlentities($s, ENT_QUOTES);
        return htmlspecialchars($s, ENT_QUOTES);
    }

    function DB($s) {
        if (get_magic_quotes_gpc()) {
            $s = stripslashes($s);            
        }

      return mysql_escape_string($s);
    }

    function resize_image2($file_path, $new_file_path, $new_width = 0, $new_height = 0, $disallow_upscale = false, $no_ext = false) {
      // calculate new size
	  // usage : resize_image2($uploadfile,$uploadfile,160,0,true,true); 	
      $image_stats = getimagesize($file_path);
      $src_width = $image_stats[0];
      $src_height = $image_stats[1];
      $src_type = $image_stats[2];

      $myNewWidth = 0;
      $myNewHeight = 0;

      if ($disallow_upscale && ($src_width < $new_width)) {
        $myNewWidth = $src_width;

      } else {
        $myNewWidth = $new_width;
      }

      $ratioX = ($src_width * 1.0) / $myNewWidth;

      if ($new_height == 0) {
        $myNewHeight = round($src_height / $ratioX);

      } else {
        $myNewHeight = $new_height;
      }

      if ($disallow_upscale && ($src_height < $myNewHeight)) {
        $myNewHeight = $src_width;
      }

      ini_set('max_execution_time', 300);
      ini_set('memory_limit', '128M');

      // read and resize image
      $src_img = @ImageCreateFromJPEG ($file_path) or // Read JPEG Image
      $src_img = @ImageCreateFromPNG ($file_path) or // or PNG Image
      $src_img = @ImageCreateFromGIF ($file_path) or // or GIF Image
      $src_img = null; // If image is not JPEG, PNG, or GIF

      if ($src_img == null) {
        echo "image not jpeg, png, gif";
      }

      $mod_img = imagecreatetruecolor($myNewWidth, $myNewHeight);
      imageCopyResampled($mod_img, $src_img, 0, 0, 0, 0, $myNewWidth, $myNewHeight, $src_width ,$src_height);

      // check image library support
      $jpegSupport = function_exists('imagejpeg');
      $pngSupport = function_exists('imagepng');
      $gifSupport = function_exists('imagegif');

      if (!($jpegSupport || $pngSupport || $gifSupport)) {
        echo "please check image library support";
      }

      $ref_info = $resize_info = pathinfo($file_path);
      if ($new_file_path != "") {
        $ref_info = $new_info = pathinfo($new_file_path);
      }

      $foundType = false;
      $result_file = null;
      if ($src_type == IMAGETYPE_GIF && $gifSupport) {
        $result_file = $ref_info['dirname'] . "/" . $ref_info['filename'] . ($no_ext? "" : ".gif");
        imagegif($mod_img, $result_file);
        $foundType = true;

      } else if ($src_type == IMAGETYPE_PNG && $pngSupport) {
        $result_file = $ref_info['dirname'] . "/" . $ref_info['filename'] . ($no_ext? "" : ".png");
        imagepng($mod_img, $result_file);
        $foundType = true;

      } else { // force jpeg support
        $result_file = $ref_info['dirname'] . "/" . $ref_info['filename'] . ($no_ext? "" : ".jpeg");
        imagejpeg($mod_img, $result_file);
        $foundType = true;
      }

      imagedestroy($mod_img);
      return $result_file;
    }

    function return_bytes($val) {
      // adapted from http://www.php.net/manual/en/function.ini-get.php
      $val = trim($val);
      $last = strtolower($val[strlen($val) - 1]);
      switch($last) {
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
      }

      return $val;
    }

    function getAppUploadLimit() {
      $upload_max_filesize = getUploadMaxFileSize();
      return return_bytes($upload_max_filesize) > return_bytes("2M")? "2M": $upload_max_filesize;
    }

    function getProfilePictureLimit() {
      return getAppUploadLimit();
    }

    function getUploadMaxFileSize() {
      $upload_max_filesize = ini_get("upload_max_filesize");
      $post_max_size = ini_get("post_max_size");

      return return_bytes($post_max_size) > return_bytes($upload_max_filesize)? $upload_max_filesize: $post_max_size;
    }

    function getIp() {
      return $_SERVER['REMOTE_ADDR'];
    }
	
	function curPageURL() {
	 $pageURL = 'http';
	 if ( isset( $_SERVER["HTTPS"] ) && strtolower( $_SERVER["HTTPS"] ) == "on" ) {
		$pageURL .= "s";
		}

	 //if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
	  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
	}


	function curPageName() {
	 return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
	
	function is_date( $str )
	{
	  $stamp = strtotime( $str );
	 
	  if (!is_numeric($stamp))
	  {
		 return FALSE;
	  }
	  $month = date( 'm', $stamp );
	  $day   = date( 'd', $stamp );
	  $year  = date( 'Y', $stamp );
	 
	  if (checkdate($month, $day, $year))
	  {
		 return TRUE;
	  }
	 
	  return FALSE;
	}

	function multi2dSortAsc(&$arr, $key){
	  $sort_col = array();
	  foreach ($arr as $sub) $sort_col[] = $sub[$key];
	  array_multisort($sort_col,SORT_NUMERIC,SORT_DESC, $arr );
	}
	
	function changeLastcolColour($string){
	  

		$last = strrpos($string,"<td>");
		$total = strlen($string) - 1;
		$stop = $last - $total; 
		$string = substr_replace($string, '<td><font color=red>', $last, 4);  
		return $string;
	}	
	function array_duplicates($array)
	{
		if(!is_array($array))
		return false;
		$duplicates = array();
		$unique = array_unique($array);
		if(count($array) > count($unique))
		for($i = 0; $i < count($array); $i++)
		if(!array_key_exists($i, $unique))
		$duplicates[] = $array[$i];
		return $duplicates;
	}
	function print_array($aArray) {
		sort($aArray);
		print "<b>";
		foreach($aArray as $value) {
		print "$value,";
		}
		print "</b>";

	
	}
	
	function get_array_to_string($aArray) {
		sort($aArray);
		$outStr = "";
		foreach($aArray as $value) {
		$outStr = $outStr."$value,";
		}
		$outStr = rtrim($outStr, ",");
		return $outStr;
	
	}
	
	
	function extract_repeatedVals_from_comma_string_with_count($cString) 
	{
	
		$common_numbers = explode(",",$cString);
		//print_r ($common_numbers);
		$shop = array( array( "arCol1" => "aNo", 
							  "aCount" => 0, 	
							  
							)
					   
					 );
		$new_array = array();
		foreach ($common_numbers as $key => $value) {
			if(isset($new_array[$value]))
				$new_array[$value] += 1;
			else
				$new_array[$value] = 1;
		}				

		foreach ($new_array as $fruit => $n) {
			 if($n > 1)
			 {
			 
				 array_push($shop, array("arCol1" => $fruit,"aCount" => $n)); 
				 
				//echo $fruit;
			   
				//echo "($n)";


			}
			
		}
		
		multi2dSortAsc($shop, "aCount");
		//echo "<pre>";
		//print_r($shop);
		//echo "</pre>";
		
		
		$rowCount = 1;	
		for ($k=0;$k<=sizeof($shop)-1;$k++) {
			if ($shop[$k]["aCount"] >0 )
			{
			$strCommonNos = $strCommonNos . $shop[$k]["arCol1"] . "(". $shop[$k]["aCount"] . "), " ;
			}
		}
		
		return $strCommonNos;
	

	}
	
	function multiple_space_replace($string)
	{
	// using preg_replace to replace space, tabs and newline in a given string
	$str = preg_replace('/\s+/', ' ', $string);  
	return $str;
	}		

	function getRealIpAddr() {
		return (empty($_SERVER['HTTP_CLIENT_IP'])?(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?
		$_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR']):$_SERVER['HTTP_CLIENT_IP']);
	}
																																		
	//The following function can be used to escape singe and double quotes in a string with out the risk of escaping quotes twice.This function  escapes a quote, only if it hasn't already been escaped.

	function safe_string_escape($str)
	{
	   $len=strlen($str);
		$escapeCount=0;
		$targetString='';
		for($offset=0;$offset<$len;$offset++) {
			switch($c=$str{$offset}) {
				case "'":
				// Escapes this quote only if its not preceded by an unescaped backslash
						if($escapeCount % 2 == 0) $targetString.="\\";
						$escapeCount=0;
						$targetString.=$c;
						break;
				case '"':
				// Escapes this quote only if its not preceded by an unescaped backslash
						if($escapeCount % 2 == 0) $targetString.="\\";
						$escapeCount=0;
						$targetString.=$c;
						break;
				case '\\':
						$escapeCount++;
						$targetString.=$c;
						break;
				default:
						$escapeCount=0;
						$targetString.=$c;
			}
		}
		return $targetString;
	}
	//echo safe_string_escape("asda'sda\'dsad\"sadasd'");

	

//***Begin - returns insert query, by elimination sql injection for single quotes - updated by PVR 2010 Oct 7 -- src=http://www.zedwood.com/article/82/preventing-sql-injection-in-php-insert-and-update***/

		

//we need to call a quote function to sanitize every input coming from the user. 		

function quote($value)

{

    if (get_magic_quotes_gpc())

        $value = stripslashes($value);

    if (is_numeric($value))

        return "'$value'";

    return "'".mysql_real_escape_string($value)."'";

}

//function for Insert 

//Usage:

		//$record = array();

		//$record['username'] = $_POST['username'];

		//$record['password'] = $_POST['password'];

		//$query = get_quoted_insert('users',$record);

		//echo $query;

function get_quoted_insert($table, $assoc_arr)

{

    foreach($assoc_arr as $k=>$v)

        $assoc_arr[$k] = quote($v);



    $insertstr="INSERT INTO `".$table."`";

    $insertstr.=" (`". implode("`,`", array_keys($assoc_arr)) ."`) VALUES" ;

    $insertstr.=" (". implode(",", array_values($assoc_arr)) .");" ;

    return $insertstr;

}



//function for update 
//Usage:

//$record = array();
//$record['username'] = $_POST['username'];
//$record['password'] = $_POST['password'];
//$query = get_quoted_update('tableName',$record,'id', '5');

function get_quoted_update($table, $update_arr, $row_identifier_id, $id)

{

    $update_str="update  `".$table."` set ";

    foreach($update_arr as $k=>$v)

       $update_str.= "`".$k."`=".quote($v).",";

    $update_str = substr($update_str, 0, -1)." ";

    $update_str.="where ".$row_identifier_id ."=".quote($id);

    return $update_str;

}



//Usage:

		//$record = array();

		//$record['username'] = $_POST['username'];

		//$record['password'] = $_POST['password'];

		//$query = get_quoted_update('users',$record, '5');

		//echo $query;



//***End - returns insert query, by elimination sql injection for single quotes - updated by PVR 2010 Oct 7 -- src=http://www.zedwood.com/article/82/preventing-sql-injection-in-php-insert-and-update***/



//***Begin funciton to get value of single field - PVR - 2010 OCT 13***

function get_single_field_value($table, $field, $whereCond)

		

		{

			$hordetailsql="select  `".$field."` from `".$table."` ".$whereCond;

			//echo $hordetailsql . "<br>";



			$hordetailresult=mysql_query($hordetailsql); 

			$horodetailrow = mysql_fetch_array($hordetailresult, MYSQL_BOTH);

			$varHoroDetails =  $horodetailrow[0];

			unset($hordetailsql); 

			unset($hordetailresult);

			unset($horodetailrow);

			return $varHoroDetails;

		    

		}

//***End funciton to get value of single field - PVR - 2010 OCT 13***


function right($value, $count){

    return substr($value, ($count*-1));

}

 

function left($string, $count){

    return substr($string, 0, $count);

}

function removeZeroFromNumber($sNumber)
{
	$sNumber  = str_replace(" ","",$sNumber);  // removing spaces in number
	

	if (left($sNumber,1)==0)
	{
		$sNumber = substr($sNumber,1,strlen($sNumber)-1); //removing 0 before first number
	}
	$sNumber  = str_replace(",0",",",$sNumber);  //removing 0 before number
	
	return $sNumber;
}

function isEmail($email)
{
	if (preg_match("/^(\w+((-\w+)|(\w.\w+))*)\@(\w+((\.|-)\w+)*\.\w+$)/",$email))
	{
		return true;
	}
	else 
	{
		return false;
	}
}


function get_ip_address() {
 foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
     if (array_key_exists($key, $_SERVER) === true) {
         foreach (explode(',', $_SERVER[$key]) as $ip) {
            if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
            return $ip;
          }
        }
     }
  }
}

//****Inseret visitor stat***

//****Begin inserting  visitor details***/
function insert_visitor_stat($db,$agtid,$lid,$remarks) {
											
	$varIP =  get_ip_address();
	
	$varReferrer =  isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

	
	$varBrowser =  $_SERVER['HTTP_USER_AGENT']; 

	
	
	$record = array();
	
	$record['statdate'] = date('c');;
	
	//$record['agtid'] = intval($varAgtID);
	
	//$record['listid'] = intval($varListId);
	
	$record['agtid'] = intval($agtid);
	
	$record['listid'] = intval($lid);
	
	
	$record['ipaddress'] = $varIP;
	
	$record['referrer'] = $varReferrer;
	
	$record['url'] = curPageURL();

	$record['browser'] = $varBrowser;
	
	$record['remarks'] = $remarks;
	
	

	$sql = get_quoted_insert('tblStatistics',$record);

	//echo $sql;
	$q = $db->prepare($sql);
	$q->execute();		
	
}				  
//***end inserting visitors details

function GET($key, $default = null) {
    return isset($_GET[$key]) ? $_GET[$key] : $default;
}

// this function always returns a valid date
function dateconvert($date = NULL, $date_type = 'sql',$original_date_type='dd/mm/yyyy',$explodechar='/') {
    //$date =  preg_replace('/\D/','/',$date);
	$date =  preg_replace('/\D/','/',getYMDDate($date, $original_date_type,$explodechar));
	
    $timestamp = strtotime($date);
	
	switch($date_type) {
        default: case 'sql' : return date('Y-m-d', $timestamp); break; // prints YYYY-MM-DD
        case 'en_EN' : return date('d-m-Y', $timestamp); break; // prints DD-MM-YYYY
		case 'en_EN2' : return date('m-d-Y', $timestamp); break; // prints MM-DD-YYYY
    }
	
}


// this function always returns a valid date
function getYMDDate($date = NULL, $original_date_type='dd/mm/yyyy',$explodechar='/') {
	switch($original_date_type) {
        default: case 'dd'.$explodechar.'mm'.$explodechar.'yyyy' : 
			list($d,$m,$y) = explode($explodechar, $date);
			//return "1";
			break;
        case 'mm'.$explodechar.'dd'.$explodechar.'yyyy' : 
			list($m,$d,$y) = explode($explodechar, $date);
			//return "2";
			break;
		case 'yyyy'.$explodechar.'mm'.$explodechar.'dd' : 
			list($d,$m,$y) = explode($explodechar, $date);
			//return "3";
			break;
		
    }
	
	$strDate = $y."-".$m."-".$d;
	return date("Y-m-d", strtotime($strDate));
	//return $original_date_type;
}

?>
