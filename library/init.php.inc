<?php require_once("db_connect.php.inc"); ?>
<?php require_once("functions.php.inc"); ?>

<?php
  $query  = "SELECT * FROM system_settings";
  $result = mysql_query($query);
  if(mysql_affected_rows()>0){
	$mainFolder    				= mysql_result($result,0,"mainfoldername");	
	$docRoot       				= $_SERVER['DOCUMENT_ROOT'].$mainFolder;
	$ServerHost 				= $_SERVER['HTTP_HOST']."/".$mainFolder;
    $websiteUrl  				= mysql_result($result,0,"website_url");
    $websiteName   				= mysql_result($result,0,"website_name");
    $adminEmailId  				= mysql_result($result,0,"admin_email");
	$welcomeMsg  				= mysql_result($result,0,"welcome_msg");
    $city_name     				= mysql_result($result,0,"city_name");
    $template_path 				= mysql_result($result,0,"template_path");
	$templateRoot  				= $docRoot.$template_path;
    $templateUrl   				= $websiteUrl.$template_path;
	
	$mhChar     				= mysql_result($result,0,"mhchar");
	$shChar     				= mysql_result($result,0,"shchar");
  }

  $query  = "SELECT * FROM languages";
  $result = mysql_query($query);
  if(mysql_affected_rows()>0){
	$selected_language			= mysql_result($result,0,"language");
 	$language_path_rel			= "/languages".mysql_result($result,0,"language_filepath");
	$language_path_abs			= $docRoot.$language_path_rel;
	$language_path_url			= $websiteUrl.$language_path_rel;
  }
  

  //-Common variables [Global]
  $websiteNameLinked = "<a href='".$websiteUrl."'>".$websiteName."</a>";
  $errorEmailId = $adminEmailId;
  $postmasterEmailId = $adminEmailId;
?>
<?php 
$check = mysql_query ("SELECT * FROM languages LIMIT 0,1");
 /* >>limit<< is just to make it faster in case the db is huge */
if ($check){				//checking if the language table is present and if language file should be loaded
$languagefile = $language_path_abs."/main.lang";
$testpath = $docRoot."/languages"."/English"."/default.lang";
include($language_path_abs."/main.lang");
}
else {		// This means that language tables are not imported. Eng. file is inserted until fixed.
$status_msg = "This error is probably caused by missing tables in the database.
			  Make sure that you have uploaded/updated the database file correctly. Contact 
			  support if you are unsure how to do this.<br>
			  The English languagefile has been loaded and cannot be changed until the database is fixed.";
//include($docRoot."/languages"."/English"."/default.lang");
}												    
 ?>